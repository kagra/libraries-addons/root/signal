// @(#)root/kfr:$Id$

#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class THistogram+;

#pragma link C++ class TKFRLegacy *Complex+;
#pragma link C++ class TKFRLegacy *ComplexReal+;
#pragma link C++ class TKFRLegacy *RealComplex+;

#endif
