# Version used during development
cmake_minimum_required(VERSION 3.5)
project(RSignal/KFR)

# Include custom CMake modules and utilities
list(PREPEND CMAKE_MODULE_PATH 
    ${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/Inc/
    ${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/Req/
    ${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/Modules/
)

    #
    # Load compiler
    include(LoadStandardCompilerC)
    include(LoadStandardCompilerCXX)

    #
    # Library dependencies
    include(LoadROOT)
#
# Prepare library including ROOT    
set(LIBRARY "${LIBRARY}_KFR")

FILE_SOURCES(SOURCES "src")
add_library(${LIBRARY} SHARED ${SOURCES})
target_include_directories(${LIBRARY} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

target_link_libraries(${LIBRARY} ${ROOT_LIBRARIES})
target_link_package(${LIBRARY} KFR REQUIRED)

#
# Prepare ROOT Dictionary
set(DICTIONARY ${LIBRARY}.Dict)
if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/include/LinkDef.h)
    message(FATAL_ERROR "ROOT LinkDef is missing `LinkDef.h`.")
endif()

FILE_HEADERS_RECURSE(HEADERS "include")
list(REMOVE_ITEM HEADERS LinkDef.h)
ROOT_GENERATE_DICTIONARY(${DICTIONARY} ${HEADERS} LINKDEF ${CMAKE_CURRENT_SOURCE_DIR}/include/LinkDef.h MODULE ${LIBRARY})

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include DESTINATION . PATTERN "*.in" EXCLUDE)
install(TARGETS ${LIBRARY} LIBRARY DESTINATION lib ARCHIVE DESTINATION lib RUNTIME DESTINATION bin)
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARY}.rootmap DESTINATION lib)   # Install the rootmap file
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARY}_rdict.pcm DESTINATION lib) # Install the rdict.pcm file
