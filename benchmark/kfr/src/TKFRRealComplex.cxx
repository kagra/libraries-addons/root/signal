// @(#)root/kfr:$Id$
// Author: Marco Meyer   30/12/2023

/*************************************************************************
 * Copyright (C) 1995-2023, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

////////////////////////////////////////////////////////////////////////////////
///
/// \class TKFRLegacy *RealComplex
///
/// One of the interface classes to the KFR package, can be used directly
/// or via the TVirtualFFT class. Only the basic interface of KFR is implemented.
///
/// Computes a real input/complex output discrete Fourier transform in 1 or more
/// dimensions. However, only out-of-place transforms are now supported for transforms
/// in more than 1 dimension. For detailed information about the computed transforms,
/// please refer to the KFR manual
///
/// How to use it:
/// 1. Create an instance of TKFRLegacy *RealComplex - this will allocate input and output
///    arrays (unless an in-place transform is specified)
/// 2. Run the Init() function with the desired flags and settings (see function
///    comments for possible kind parameters)
/// 3. Set the data (via SetPoints()or SetPoint() functions)
/// 4. Run the Transform() function
/// 5. Get the output (via GetPoints() or GetPoint() functions)
/// 6. Repeat steps 3)-5) as needed
/// For a transform of the same size, but with different flags,
/// rerun the Init() function and continue with steps 3)-5)
///
/// NOTE:
///       1. running Init() function will overwrite the input array! Don't set any data
///          before running the Init() function
///       2. KFR computes unnormalized transform, so doing a transform followed by
///          its inverse will lead to the original array scaled by the transform size
///
///
/////////////////////////////////////////////////////////////////////////////////

#include "TKFRLegacy *RealComplex.h"
#include "TComplex.h"
#include "Riostream.h"

#include "kfr/dft/fft.hpp"
#include "kfr/version.hpp"
namespace kfr
{
const char* library_version_dft();
} // namespace kfr

ClassImp(TKFRLegacy *RealComplex);
using complex = kfr::complex<double>;

////////////////////////////////////////////////////////////////////////////////
///default

TKFRLegacy *RealComplex::TKFRLegacy *RealComplex()
{
   fIn   = 0;
   fOut  = 0;
   fTemp = 0;
   fPlan = 0;
   fN    = 0;
   fNdim = 0;
   fTotalSize = 0;
}

////////////////////////////////////////////////////////////////////////////////
///For 1d transforms
///Allocates memory for the input array, and, if inPlace = kFALSE, for the output array

TKFRLegacy *RealComplex::TKFRLegacy *RealComplex(Int_t n, Bool_t inPlace)
{
   if (!inPlace) {
      fIn = new std::vector<double>(n, 0);
      fOut = new std::vector<complex>((n/2+1), 0);
   } else {
      fIn = new std::vector<double>((2*(n/2+1)), 0);
      fOut = 0;
   }

   fN = new Int_t[1];
   fN[0] = n;
   fTotalSize = n;
   fNdim = 1;
   fPlan = 0;
   fTemp = 0;
}

////////////////////////////////////////////////////////////////////////////////
///For ndim-dimensional transforms
///Second argument contains sizes of the transform in each dimension

TKFRLegacy *RealComplex::TKFRLegacy *RealComplex(Int_t ndim, Int_t *n, Bool_t inPlace)
{
   if (ndim>1 && inPlace==kTRUE){
      Error("TKFRLegacy *RealComplex", "multidimensional in-place r2c transforms are not implemented yet");
      return;
   }
   fNdim = ndim;
   fTotalSize = 1;
   fN = new Int_t[fNdim];
   for (Int_t i=0; i<fNdim; i++){
      fN[i] = n[i];
      fTotalSize*=n[i];
   }
   Int_t sizeout = Int_t(Double_t(fTotalSize)*(n[ndim-1]/2+1)/n[ndim-1]);
   if (!inPlace) {
      fIn = new std::vector<double>(fTotalSize, 0);
      fOut = new std::vector<complex>(sizeout, 0);
   } else {
      fIn = new std::vector<double>((2*sizeout), 0);
      fOut = 0;
   }

   fPlan = 0;
   fTemp = 0;
}

////////////////////////////////////////////////////////////////////////////////
///Destroys the data arrays and the plan. However, some plan information stays around
///until the root session is over, and is reused if other plans of the same size are
///created

TKFRLegacy *RealComplex::~TKFRLegacy *RealComplex()
{
   delete reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan);
   fPlan = 0;
   
   delete ((std::vector<double>*) fIn);
   fIn = 0;
   
   if(fOut) 
      delete ((std::vector<complex>*) fOut);
   fOut = 0;
   if (fTemp)
      delete ((std::vector<kfr::u8>*) fTemp);
   fTemp = 0;

   if (fN)
      delete [] fN;
   fN = 0;
}

////////////////////////////////////////////////////////////////////////////////
///Creates the fftw-plan
///
///NOTE:  input and output arrays are overwritten during initialisation,
///       so don't set any points, before running this function!!!!!
///
///Arguments sign and kind are dummy and not need to be specified
///Possible flag_options:
///
/// - "ES" (from "estimate") - no time in preparing the transform, but probably sub-optimal
///   performance
/// - "M" (from "measure") - some time spend in finding the optimal way to do the transform
/// - "P" (from "patient") - more time spend in finding the optimal way to do the transform
/// - "EX" (from "exhaustive") - the most optimal way is found
///
///This option should be chosen depending on how many transforms of the same size and
///type are going to be done. Planning is only done once, for the first transform of this
///size and type.

void TKFRLegacy *RealComplex::Init(Option_t *flags,Int_t /*sign*/, const Int_t* /*kind*/)
{
   fFlags = flags;

   if (fPlan) delete reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan);
   fPlan = new kfr::dft_plan_real<double>(fTotalSize);
   if(fFlags.Contains("D")) reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan)->dump();

   if (fTemp) delete ((std::vector<kfr::u8>*) fTemp);
   fTemp = new std::vector<kfr::u8>(reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan)->temp_size, 0);
}

////////////////////////////////////////////////////////////////////////////////
///Computes the transform, specified in Init() function

void TKFRLegacy *RealComplex::Transform()
{
   std::vector<double> * _fIn   = reinterpret_cast<std::vector<double>*>(fIn);
   std::vector<complex>* _fOut  = reinterpret_cast<std::vector<complex>*>(fOut ? fOut : fIn);
   std::vector<kfr::u8>* _fTemp = reinterpret_cast<std::vector<kfr::u8>*>(fTemp);

   if (fPlan) {
      reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan)->execute(&(*_fOut)[0], &(*_fIn)[0], &(*_fTemp)[0]);
   } else {
      Error("Transform", "transform not initialised");
      return;
   }
}

////////////////////////////////////////////////////////////////////////////////
///Fills the array data with the computed transform.
///Only (roughly) a half of the transform is copied (exactly the output of KFR),
///the rest being Hermitian symmetric with the first half

void TKFRLegacy *RealComplex::GetPoints(Double_t *data, Bool_t fromInput) const
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);
   std::vector<complex>* _fOut  = reinterpret_cast<std::vector<complex>*>(fOut ? fOut : fIn);

   if (fromInput){
      for (Int_t i=0; i<fTotalSize; i++)
         data[i] = (*_fIn)[i];
   } else {
      Int_t realN = 2*Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);
      if (fOut){
         for (Int_t i=0; i<realN; i+=2){
            data[i] = (*_fOut)[i/2].real();
            data[i+1] = (*_fOut)[i/2].imag();
         }
      }
      else {
         for (Int_t i=0; i<realN; i++)
            data[i] = (*_fIn)[i];
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
///Returns the real part of the point #ipoint from the output or the point #ipoint
///from the input

Double_t TKFRLegacy *RealComplex::GetPointReal(Int_t ipoint, Bool_t fromInput) const
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);
   std::vector<complex>* _fOut  = reinterpret_cast<std::vector<complex>*>(fOut ? fOut : fIn);

   if (fOut && !fromInput){
      Warning("GetPointReal", "Output is complex. Only real part returned");
      return (*_fOut)[ipoint].real();
   }
   else
      return (*_fIn)[ipoint];
}

////////////////////////////////////////////////////////////////////////////////
///Returns the real part of the point #ipoint from the output or the point #ipoint
///from the input

Double_t TKFRLegacy *RealComplex::GetPointReal(const Int_t *ipoint, Bool_t fromInput) const
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);
   std::vector<complex>* _fOut  = reinterpret_cast<std::vector<complex>*>(fOut ? fOut : fIn);

   Int_t ireal = ipoint[0];
   for (Int_t i=0; i<fNdim-1; i++)
      ireal=fN[i+1]*ireal + ipoint[i+1];

    if (fOut && !fromInput){
      Warning("GetPointReal", "Output is complex. Only real part returned");
      return (*_fOut)[ireal].real();
   }
   else
      return (*_fIn)[ireal];
}


////////////////////////////////////////////////////////////////////////////////
///Returns the point #ipoint.
///For 1d, if ipoint > fN/2+1 (the point is in the Hermitian symmetric part), it is still
///returned. For >1d, only the first (roughly)half of points can be returned
///For 2d, see function GetPointComplex(Int_t *ipoint,...)

void TKFRLegacy *RealComplex::GetPointComplex(Int_t ipoint, Double_t &re, Double_t &im, Bool_t fromInput) const
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);
   std::vector<complex>* _fOut = reinterpret_cast<std::vector<complex>*>(fOut ? fOut : fIn);

   if (fromInput){
      re = (*_fIn)[ipoint];
   } else {
      if (fNdim==1){
         if (fOut){
            if (ipoint<fN[0]/2+1){
               re = (*_fOut)[ipoint].real();
               im = -(*_fOut)[ipoint].imag();
            } else {
               re =  (*_fOut)[fN[0]-ipoint].real();
               im = (*_fOut)[fN[0]-ipoint].imag();
            }
         } else {
            if (ipoint<fN[0]/2+1){
               re = (*_fIn)[2*ipoint];
               im = (*_fIn)[2*ipoint+1];
            } else {
               re = (*_fIn)[2*(fN[0]-ipoint)];
               im = (*_fIn)[2*(fN[0]-ipoint)+1];
            }
         }
      }
      else {
         Int_t realN = 2*Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);
         if (ipoint>realN/2){
            Error("GetPointComplex", "Illegal index value");
            return;
         }
         if (fOut){
            re = (*_fOut)[ipoint].real();
            im = (*_fOut)[ipoint].imag();
         } else {
            re = (*_fIn)[2*ipoint];
            im = (*_fIn)[2*ipoint+1];
         }
      }
   }
}
////////////////////////////////////////////////////////////////////////////////
///For multidimensional transforms. Returns the point #ipoint.
///In case of transforms of more than 2 dimensions,
///only points from the first (roughly)half are returned, the rest being Hermitian symmetric

void TKFRLegacy *RealComplex::GetPointComplex(const Int_t *ipoint, Double_t &re, Double_t &im, Bool_t fromInput) const
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);
   std::vector<complex>* _fOut  = reinterpret_cast<std::vector<complex>*>(fOut ? fOut : fIn);

   Int_t ireal = ipoint[0];
   for (Int_t i=0; i<fNdim-2; i++)
      ireal=fN[i+1]*ireal + ipoint[i+1];
   //special treatment of the last dimension
   ireal = (fN[fNdim-1]/2+1)*ireal + ipoint[fNdim-1];

   if (fromInput){
      re = (*_fIn)[ireal];
      return;
   }
   if (fNdim==1){
      if (fOut){
         if (ipoint[0] <fN[0]/2+1){
            re = (*_fOut)[ipoint[0]].real();
            im = -(*_fOut)[ipoint[0]].imag();
         } else {
            re = (*_fOut)[fN[0]-ipoint[0]].real();
            im = (*_fOut)[fN[0]-ipoint[0]].imag();
         }
      } else {
         if (ireal <fN[0]/2+1){
            re = (*_fIn)[2*ipoint[0]];
            im = (*_fIn)[2*ipoint[0]+1];
         } else {
            re = (*_fIn)[2*(fN[0]-ipoint[0])];
            im = (*_fIn)[2*(fN[0]-ipoint[0])+1];
         }
      }
   }
   else if (fNdim==2){
      if (fOut){
         if (ipoint[1]<fN[1]/2+1){
            re = (*_fOut)[ipoint[1]+(fN[1]/2+1)*ipoint[0]].real();
            im = -(*_fOut)[ipoint[1]+(fN[1]/2+1)*ipoint[0]].imag();
         } else {
            if (ipoint[0]==0){
               re = (*_fOut)[fN[1]-ipoint[1]].real();
               im = (*_fOut)[fN[1]-ipoint[1]].imag();
            } else {
               re = (*_fOut)[(fN[1]-ipoint[1])+(fN[1]/2+1)*(fN[0]-ipoint[0])].real();
               im = (*_fOut)[(fN[1]-ipoint[1])+(fN[1]/2+1)*(fN[0]-ipoint[0])].imag();
            }
         }
      } else {
         if (ipoint[1]<fN[1]/2+1){
            re = (*_fIn)[2*(ipoint[1]+(fN[1]/2+1)*ipoint[0])];
            im = -(*_fIn)[2*(ipoint[1]+(fN[1]/2+1)*ipoint[0])+1];
         } else {
            if (ipoint[0]==0){
               re = (*_fIn)[2*(fN[1]-ipoint[1])];
               im = (*_fIn)[2*(fN[1]-ipoint[1])+1];
            } else {
               re = (*_fIn)[2*((fN[1]-ipoint[1])+(fN[1]/2+1)*(fN[0]-ipoint[0]))];
               im = (*_fIn)[2*((fN[1]-ipoint[1])+(fN[1]/2+1)*(fN[0]-ipoint[0]))+1];
            }
         }
      }
   }
   else {

      if (fOut){
         re = (*_fOut)[ireal].real();
         im = (*_fOut)[ireal].imag();
      } else {
         re = (*_fIn)[2*ireal];
         im = (*_fIn)[2*ireal+1];
      }
   }
}


////////////////////////////////////////////////////////////////////////////////
///Returns the input array// One of the interface classes to the KFR package, can be used directly
/// or via the TVirtualFFT class. Only the basic interface of KFR is implemented.

Double_t* TKFRLegacy *RealComplex::GetPointsReal(Bool_t fromInput) const
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);

   if (!fromInput){
      Error("GetPointsReal", "Output array is complex");
      return 0;
   }
   return &(*_fIn)[0];
}

////////////////////////////////////////////////////////////////////////////////
///Fills the argument arrays with the real and imaginary parts of the computed transform.
///Only (roughly) a half of the transform is copied, the rest being Hermitian
///symmetric with the first half

void TKFRLegacy *RealComplex::GetPointsComplex(Double_t *re, Double_t *im, Bool_t fromInput) const
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);
   std::vector<complex>* _fOut  = reinterpret_cast<std::vector<complex>*>(fOut ? fOut : fIn);

   Int_t nreal;
   if (fOut && !fromInput){
      nreal = Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);
      for (Int_t i=0; i<nreal; i++){
         re[i] = (*_fOut)[i].real();
         im[i] = (*_fOut)[i].imag();
      }
   } else if (fromInput) {
      for (Int_t i=0; i<fTotalSize; i++){
         re[i] = (*_fIn)[i];
         im[i] = 0;
      }
   }
   else {
      nreal = 2*Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);
      for (Int_t i=0; i<nreal; i+=2){
         re[i/2] = (*_fIn)[i];
         im[i/2] = (*_fIn)[i+1];
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
///Fills the argument arrays with the real and imaginary parts of the computed transform.
///Only (roughly) a half of the transform is copied, the rest being Hermitian
///symmetric with the first half

void TKFRLegacy *RealComplex::GetPointsComplex(Double_t *data, Bool_t fromInput) const
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);
   std::vector<complex>* _fOut  = reinterpret_cast<std::vector<complex>*>(fOut ? fOut : fIn);

   Int_t nreal;

   if (fOut && !fromInput){
      nreal = Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);

      for (Int_t i=0; i<nreal; i+=2){
         data[i] = (*_fOut)[i/2].real();
         data[i+1] = (*_fOut)[i/2].imag();
      }
   } else if (fromInput){
      for (Int_t i=0; i<fTotalSize; i+=2){
         data[i] = (*_fIn)[i/2];
         data[i+1] = 0;
      }
   }
   else {

      nreal = 2*Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);
      for (Int_t i=0; i<nreal; i++)
         data[i] = (*_fIn)[i];
   }
}

////////////////////////////////////////////////////////////////////////////////
///Set the point #ipoint

void TKFRLegacy *RealComplex::SetPoint(Int_t ipoint, Double_t re, Double_t /*im*/)
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);

   (*_fIn)[ipoint] = re;
}

////////////////////////////////////////////////////////////////////////////////
///For multidimensional transforms. Set the point #ipoint

void TKFRLegacy *RealComplex::SetPoint(const Int_t *ipoint, Double_t re, Double_t /*im*/)
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);

   Int_t ireal = ipoint[0];
   for (Int_t i=0; i<fNdim-1; i++)
      ireal=fN[i+1]*ireal + ipoint[i+1];
   (*_fIn)[ireal]=re;
}

////////////////////////////////////////////////////////////////////////////////
///Set all input points

void TKFRLegacy *RealComplex::SetPoints(const Double_t *data)
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);

   for (Int_t i=0; i<fTotalSize; i++){
      (*_fIn)[i]=data[i];
   }
}

////////////////////////////////////////////////////////////////////////////////
///Sets the point #ipoint (only the real part of the argument is taken)

void TKFRLegacy *RealComplex::SetPointComplex(Int_t ipoint, TComplex &c)
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);

   (*_fIn)[ipoint]=c.Re();
}

////////////////////////////////////////////////////////////////////////////////
///Set all points. Only the real array is used

void TKFRLegacy *RealComplex::SetPointsComplex(const Double_t *re, const Double_t* /*im*/)
{
   std::vector<double>* _fIn   = reinterpret_cast<std::vector<double>*>(fIn);

   for (Int_t i=0; i<fTotalSize; i++)
      (*_fIn)[i] = re[i];
}

////////////////////////////////////////////////////////////////////////////////
///allowed options:
///"ES"
///"M"
///"P"
///"EX"

UInt_t TKFRLegacy *RealComplex::MapFlag(Option_t *flag)
{
   throw std::invalid_argument("Not implemented in KFR");

   // TString opt = flag;
   // opt.ToUpper();
   // if (opt.Contains("ES"))
   //    return FFTW_ESTIMATE;
   // if (opt.Contains("M"))
   //    return FFTW_MEASURE;
   // if (opt.Contains("P"))
   //    return FFTW_PATIENT;
   // if (opt.Contains("EX"))
   //    return FFTW_EXHAUSTIVE;
   // return FFTW_ESTIMATE;
}

TString TKFRLegacy *RealComplex::Version()
{
   return TString(kfr::library_version_dft());
}
