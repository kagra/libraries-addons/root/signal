// @(#)root/kfr:$Id$
// Author: Marco Meyer   30/12/2023

/*************************************************************************
 * Copyright (C) 1995-2023, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

////////////////////////////////////////////////////////////////////////////////
/// \class TKFRLegacy *Complex
///
/// One of the interface classes to the KFR package, can be used directly
/// or via the TVirtualFFT class. Only the basic interface of KFR is implemented.
/// Computes complex input/output discrete Fourier transforms (DFT)
/// in one or more dimensions. For the detailed information on the computed
/// transforms please refer to the KFR manual, chapter "What KFR really computes".
///
/// How to use it:
///
/// 1. Create an instance of TKFRLegacy *Complex - this will allocate input and output
///    arrays (unless an in-place transform is specified)
/// 2. Run the Init() function with the desired flags and settings
/// 3. Set the data (via SetPoints(), SetPoint() or SetPointComplex() functions)
/// 4. Run the Transform() function
/// 5. Get the output (via GetPoints(), GetPoint() or GetPointComplex() functions)
/// 6. Repeat steps 3)-5) as needed
///
/// For a transform of the same size, but with different flags or sign, rerun the Init()
/// function and continue with steps 3)-5)
///
/// NOTE:
///       1. running Init() function will overwrite the input array! Don't set any data
///          before running the Init() function
///       2. KFR computes unnormalized transform, so doing a transform followed by
///          its inverse will lead to the original array scaled by the transform size
///
////////////////////////////////////////////////////////////////////////////////

#include "TKFRLegacy *Complex.h"
#include "TComplex.h"
#include "Riostream.h"

#include "kfr/dft/fft.hpp"
#include "kfr/version.hpp"
namespace kfr
{
const char* library_version_dft();
} // namespace kfr

ClassImp(TKFRLegacy *Complex);
using complex = kfr::complex<double>;

////////////////////////////////////////////////////////////////////////////////
///default

TKFRLegacy *Complex::TKFRLegacy *Complex()
{
   fIn   = 0;
   fOut  = 0;
   fTemp = 0;
   fPlan = 0;
   fN    = 0;
   fNdim = 0;
   fTotalSize = 0;
   fSign = 1;
}


// kfr::dft_plan_ptr<T> dft = kfr::dft_cache::instance().get(kfr::ctype_t<T>(), input.size());
//               std::vector<complex<T>> output(input.size(), std::numeric_limits<T>::quiet_NaN());
//               std::vector<kfr::u8> temp(dft->temp_size);

//               dft->execute(&output[0], &input[0], &temp[0]);
//                kfr::dft_cache::instance().clear();
////////////////////////////////////////////////////////////////////////////////
///For 1d transforms
///Allocates memory for the input array, and, if inPlace = kFALSE, for the output array

TKFRLegacy *Complex::TKFRLegacy *Complex(Int_t n, Bool_t inPlace)
{
   fIn = new std::vector<complex>(n, 0);
   fOut = 0;
   if (!inPlace)
      fOut = new std::vector<complex>(n, 0);
   fTemp = 0;

   fN    = new Int_t[1];
   fN[0] = n;
   fTotalSize = n;
   fNdim = 1;
   fSign = 1;
   fPlan = 0;
}

////////////////////////////////////////////////////////////////////////////////
///For multidim. transforms
///Allocates memory for the input array, and, if inPlace = kFALSE, for the output array

TKFRLegacy *Complex::TKFRLegacy *Complex(Int_t ndim, Int_t *n, Bool_t inPlace)
{
   if (ndim > 1)
      throw std::invalid_argument("KFR5 is only implementing 1D FFT.");

   fNdim = ndim;
   fTotalSize = 1;
   fN = new Int_t[fNdim];
   for (Int_t i=0; i<fNdim; i++){
      fN[i] = n[i];
      fTotalSize*=n[i];
   }

   fIn = new std::vector<complex>(fTotalSize, 0);
   fOut = 0;
   if (!inPlace)
      fOut = new std::vector<complex>(fTotalSize, 0);

   fTemp = 0;
   fSign = 1;
   fPlan = 0;
}

////////////////////////////////////////////////////////////////////////////////
///Destroys the data arrays and the plan. However, some plan information stays around
///until the root session is over, and is reused if other plans of the same size are
///created

TKFRLegacy *Complex::~TKFRLegacy *Complex()
{
   delete reinterpret_cast<kfr::dft_plan<double>*>(fPlan);
   fPlan = 0;
   
   delete ((std::vector<complex>*) fIn);
   if(fOut) 
      delete ((std::vector<complex>*) fOut);
   if (fTemp)
      delete ((std::vector<kfr::u8>*) fTemp);
   if (fN)
      delete [] fN;
}

////////////////////////////////////////////////////////////////////////////////
///Creates the fftw-plan
///
///NOTE:  input and output arrays are overwritten during initialisation,
///       so don't set any points, before running this function!!!!!
///
///2nd parameter: +1
///
///Argument kind is dummy and doesn't need to be specified
///
///Possible flag_options:
/// - "ES" (from "estimate") - no time in preparing the transform, but probably sub-optimal
///   performance
/// - "M" (from "measure") - some time spend in finding the optimal way to do the transform
/// - "P" (from "patient") - more time spend in finding the optimal way to do the transform
/// - "EX" (from "exhaustive") - the most optimal way is found
///This option should be chosen depending on how many transforms of the same size and
///type are going to be done. Planning is only done once, for the first transform of this
///size and type.

void TKFRLegacy *Complex::Init( Option_t *flags, Int_t sign,const Int_t* /*kind*/)
{
   fSign = sign;
   fFlags = flags;

   if (fPlan) delete reinterpret_cast<kfr::dft_plan<double>*>(fPlan);
   fPlan = new kfr::dft_plan<double>(fTotalSize);
   if(fFlags.Contains("D")) reinterpret_cast<kfr::dft_plan<double>*>(fPlan)->dump();

   if (fTemp) delete ((std::vector<kfr::u8>*) fTemp);
   fTemp = new std::vector<kfr::u8>(reinterpret_cast<kfr::dft_plan<double>*>(fPlan)->temp_size, 0);
}

////////////////////////////////////////////////////////////////////////////////
///Computes the transform, specified in Init() function

void TKFRLegacy *Complex::Transform()
{
   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<complex>* _fOut  = fOut ? reinterpret_cast<std::vector<complex>*>(fOut) : _fIn;
   
   std::vector<kfr::u8>* _fTemp = reinterpret_cast<std::vector<kfr::u8>*>(fTemp);

   if (fPlan) {
      reinterpret_cast<kfr::dft_plan<double>*>(fPlan)->execute(&(*_fOut)[0], &(*_fIn)[0], &(*_fTemp)[0]);
   } else {
      Error("Transform", "transform not initialised");
      return;
   }
}

////////////////////////////////////////////////////////////////////////////////
///Copies the output(or input) into the argument array

void TKFRLegacy *Complex::GetPoints(Double_t *data, Bool_t fromInput) const
{
   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<complex>* _fOut  = fOut ? reinterpret_cast<std::vector<complex>*>(fOut) : _fIn;
   
   if (!fromInput){
      
      for (Int_t i=0; i<2*fTotalSize; i+=2){
         data[i]   = (*_fOut)[i/2].real();
         data[i+1] = (*_fOut)[i/2].imag();
      }

   } else {
      
      for (Int_t i=0; i<2*fTotalSize; i+=2){
         data[i]   = (*_fIn)[i/2].real();
         data[i+1] = (*_fIn)[i/2].imag();
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
///returns real and imaginary parts of the point #ipoint

void TKFRLegacy *Complex::GetPointComplex(Int_t ipoint, Double_t &re, Double_t &im, Bool_t fromInput) const
{
   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<complex>* _fOut  = fOut ? reinterpret_cast<std::vector<complex>*>(fOut) : _fIn;
   
   if (fOut && !fromInput){
      re = (*_fOut)[ipoint].real();
      im = (*_fOut)[ipoint].imag();
   } else {
      re = (*_fIn)[ipoint].real();
      im = (*_fIn)[ipoint].imag();
   }
}

////////////////////////////////////////////////////////////////////////////////
///For multidimensional transforms. Returns real and imaginary parts of the point #ipoint

void TKFRLegacy *Complex::GetPointComplex(const Int_t *ipoint, Double_t &re, Double_t &im, Bool_t fromInput) const
{
   if (fNdim > 1)
      throw std::invalid_argument("KFR5 is only implementing 1D FFT.");

   Int_t ireal = ipoint[0];
   for (Int_t i=0; i<fNdim-1; i++)
      ireal=fN[i+1]*ireal + ipoint[i+1];

   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<complex>* _fOut  = fOut ? reinterpret_cast<std::vector<complex>*>(fOut) : _fIn;
   
   if (fOut && !fromInput){
      re = (*_fOut)[ireal].real();
      im = (*_fOut)[ireal].imag();
   } else {
      re = (*_fIn)[ireal].real();
      im = (*_fIn)[ireal].imag();
   }
}

////////////////////////////////////////////////////////////////////////////////
///Copies real and imaginary parts of the output (input) into the argument arrays

void TKFRLegacy *Complex::GetPointsComplex(Double_t *re, Double_t *im, Bool_t fromInput) const
{
   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<complex>* _fOut  = fOut ? reinterpret_cast<std::vector<complex>*>(fOut) : _fIn;
      
   if (!fromInput){
      
      for (Int_t i=0, N=(*_fOut).size(); i<N; i++){
         re[i] = (*_fOut)[i/2].real();
         im[i] = (*_fOut)[i/2].imag();
      }

   } else {
      
      for (Int_t i=0, N=(*_fIn).size(); i<N; i++){
         re[i] = (*_fIn)[i/2].real();
         im[i] = (*_fIn)[i/2].imag();
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
///Copies the output(input) into the argument array

void TKFRLegacy *Complex::GetPointsComplex(Double_t *data, Bool_t fromInput) const
{
   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<complex>* _fOut  = fOut ? reinterpret_cast<std::vector<complex>*>(fOut) : _fIn;

   if (!fromInput){

      for (Int_t i=0; i<fTotalSize; i+=2){
         data[i]   = (*_fOut)[i/2].real();
         data[i+1] = (*_fOut)[i/2].imag();
      }

   } else {

      for (Int_t i=0; i<fTotalSize; i+=2){
         data[i]   = (*_fIn)[i/2].real();
         data[i+1] = (*_fIn)[i/2].imag();
      }
   }
}

////////////////////////////////////////////////////////////////////////////////
///sets real and imaginary parts of point # ipoint

void TKFRLegacy *Complex::SetPoint(Int_t ipoint, Double_t re, Double_t im)
{
   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   (*_fIn)[ipoint] = complex(re, im);
}

////////////////////////////////////////////////////////////////////////////////
///For multidim. transforms. Sets real and imaginary parts of point # ipoint

void TKFRLegacy *Complex::SetPoint(const Int_t *ipoint, Double_t re, Double_t im)
{
   if (fNdim > 1)
      throw std::invalid_argument("KFR5 is only implementing 1D FFT.");

   Int_t ireal = ipoint[0];
   for (Int_t i=0; i<fNdim-1; i++)
      ireal=fN[i+1]*ireal + ipoint[i+1];

   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   (*_fIn)[ireal] = complex(re, im);
}

////////////////////////////////////////////////////////////////////////////////

void TKFRLegacy *Complex::SetPointComplex(Int_t ipoint, TComplex &c)
{
   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   (*_fIn)[ipoint] = complex(c.Re(), c.Im());
}

////////////////////////////////////////////////////////////////////////////////
///set all points. the values are copied. points should be ordered as follows:
///[re_0, im_0, re_1, im_1, ..., re_n, im_n)

void TKFRLegacy *Complex::SetPoints(const Double_t *data)
{
   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);

   for (Int_t i=0; i<2*fTotalSize-1; i+=2){
      (*_fIn)[i/2] = complex(data[i], data[i+1]);
   }
}

////////////////////////////////////////////////////////////////////////////////
///set all points. the values are copied

void TKFRLegacy *Complex::SetPointsComplex(const Double_t *re_data, const Double_t *im_data)
{
   if (!fIn){
      Error("SetPointsComplex", "Size is not set yet");
      return;
   }
   
   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   for (Int_t i=0; i<fTotalSize; i++){
      (*_fIn)[i] = complex(re_data[i], im_data[i+1]);
   }
}

////////////////////////////////////////////////////////////////////////////////
///allowed options:
/// - "ES" - FFTW_ESTIMATE
/// - "M" - FFTW_MEASURE
/// - "P" - FFTW_PATIENT
/// - "EX" - FFTW_EXHAUSTIVE

UInt_t TKFRLegacy *Complex::MapFlag(Option_t *flag)
{
   throw std::invalid_argument("Not implemented in KFR");

   // TString opt = flag;
   // opt.ToUpper();
   // if (opt.Contains("ES"))
   //    return FFTW_ESTIMATE;
   // if (opt.Contains("M"))
   //    return FFTW_MEASURE;
   // if (opt.Contains("P"))
   //    return FFTW_PATIENT;
   // if (opt.Contains("EX"))
   //    return FFTW_EXHAUSTIVE;
   // return FFTW_ESTIMATE;
}

TString TKFRLegacy *Complex::Version()
{
   return TString(kfr::library_version_dft());
}
