#include "StressTest.h"

int main(int argc, char **argv)
{
    TApplication app("app", &argc, argv);
   
    srand(argc > 0 ? TString(argv[0]).Atoi() : 1);
    
    std::cout << cli_red << std::flush;
    std::cout << TKFRLegacy *Complex::Version() << std::endl;
    std::cout << TFFTComplex::Version() << std::endl;
    std::cout << cli_nc << std::flush;

    TFile *f = TFile::Open(fname, "RECREATE", TFFTComplex::Version() + " - " + TKFRLegacy *Complex::Version());

    // /////// 
    // Initialization step
    // ///////
    gStyle->SetOptStat(0);
    TLegend *legend = NULL;
    
    std::vector<TString>    types  = {"C2C"  , "R2C" ,  "C2R"};
    std::vector<Color_t>    colors = {kBlue+2, kGreen+2,  kRed+2};
    std::vector<int (*)(int, time_ns &, time_ns &, TString)> funcs = {&C2C, &R2C, &C2R};
    std::vector<TGraph*>   gFFTW(3), TKFRLegacy *(3), gTimeRatio(3), gTimeRelative(3);
    
    std::vector<TCanvas*> c(3);
    for(int w = 0, W = types.size(); w < W; w++) {

        c[w] = new TCanvas("canvas["+types[w]+"]", TFFTComplex::Version() + " - " + TKFRLegacy *Complex::Version(), 1500, 400);
        c[w]->Divide(3,1);
    }

    for(int w = 0, W = types.size(); w < W; w++) {

        c[w]->cd(1);
        c[w]->cd(1)->SetGridx(true);
        c[w]->cd(1)->SetGridy(true);
        c[w]->cd(1)->SetLogx(bLogx);

        gFFTW[w] = new TGraph(n);
        gFFTW[w]->SetName("gFFTW["+types[w]+"]");
        gFFTW[w]->SetTitle("Performance test: absolute computing time; FFT size; t (in ns)");
        gFFTW[w]->SetMarkerColor(colors[w]);
        gFFTW[w]->SetLineColor(colors[w]);
        gFFTW[w]->SetMarkerStyle(70);
        gFFTW[w]->SetMarkerSize(1.5);

        TKFRLegacy *[w] = new TGraph(n);
        TKFRLegacy *[w]->SetName("TKFRLegacy *["+types[w]+"]");
        TKFRLegacy *[w]->SetTitle("Performance test: absolute computing time; FFT size; t (in ns)");
        TKFRLegacy *[w]->SetMarkerColor(colors[w]);
        TKFRLegacy *[w]->SetLineColor(colors[w]);
        TKFRLegacy *[w]->SetMarkerStyle(71);
        TKFRLegacy *[w]->SetMarkerSize(1.5);

        if(gDisplay) {

            gFFTW[w]->Draw("AP");
            TKFRLegacy *[w]->Draw("P");
             
            TLegend *legend = new TLegend(0.125,0.7,0.7,0.875);
            legend->AddEntry(gFFTW[w], "FFTW - "+types[w]+" (double) - iter = " + TString::Itoa(iterations,10) + " - power of 2","lp+");
            legend->AddEntry(TKFRLegacy *[w], "KFR - "+types[w]+" (double) - iter = " + TString::Itoa(iterations,10) + " - power of 2","lp+");
            if(gDisplay) legend->Draw();
        }
    }

    for(int w = 0, W = types.size(); w < W; w++) {

        c[w]->cd(2);
        c[w]->cd(2)->SetGridx(true);
        c[w]->cd(2)->SetGridy(true);
        c[w]->cd(2)->SetLogx(bLogx);

        gTimeRatio[w] = new TGraph(n);
        gTimeRatio[w]->SetName("gTimeRatio["+types[w]+"]");
        gTimeRatio[w]->SetTitle("Time ratio KFR/FFTW; FFT size; KFR/FFTW");
        gTimeRatio[w]->SetMarkerColor(colors[w]);
        gTimeRatio[w]->SetLineColor(colors[w]);
        gTimeRatio[w]->SetMarkerStyle(68);
        gTimeRatio[w]->SetMarkerSize(1.5);

        if(gDisplay) {

            gTimeRatio[w]->Draw("ACP");
        }
    }

    for(int w = 0, W = types.size(); w < W; w++) {

        c[w]->cd(3);
        c[w]->cd(3)->SetGridx(true);
        c[w]->cd(3)->SetGridy(true);
        c[w]->cd(3)->SetLogx(bLogx);
        
        gTimeRelative[w] = new TGraph(n);
        
        gTimeRelative[w]->SetName("gTimeRelative["+types[w]+"]");
        gTimeRelative[w]->SetTitle("Average #Deltat_{kfr - fftw}; FFT size; t (in us)");
        gTimeRelative[w]->SetMarkerColor(colors[w]);
        gTimeRelative[w]->SetLineColor(colors[w]);
        gTimeRelative[w]->SetMarkerStyle(70);
        gTimeRelative[w]->SetMarkerSize(1.5);

        if(gDisplay) {
        
            gTimeRelative[w]->Draw("ACP");
        }
    }








    // /////// 
    // Rapid power of two computation
    // ///////
    for(int i = 1; i < n+1; i++) {
    
        for(int w = 0, W = types.size(); w < W; w++) {

            int pow2 = TMath::Power(2,i);

            std::chrono::duration<double> delay = std::chrono::duration<double>(0);
            time_ns kfr(0), fftw(0);

            bool verbose = true;

            if(verbose) std::cout << cli_green << types[w] << "(size = 2^"<< i <<" = "<< pow2 << ", iterations = " << iterations<< ").. " << cli_nc << std::endl; 
            for(int k = 0; k < iterations; k++) {

                time_ns _kfr(0), _fftw(0);

                TString kfr_flags = (k == 0 && verbose) ? "D" : "";
                if(funcs[w](TMath::Power(2,i), _fftw, _kfr, kfr_flags)) {
                
                    std::cout << "Not matching.." << std::endl;
                    return 1;
                }

                delay = delay + (_kfr - _fftw);
                kfr   = kfr   + _kfr;
                fftw  = fftw  + _fftw;
            }

            delay /= iterations;
            kfr /= iterations;
            fftw /= iterations;

            double dKFR   = std::chrono::duration<double>(kfr).count()  * 1'000'000;
            double dFFTW  = std::chrono::duration<double>(fftw).count() * 1'000'000;
            double dDelay = std::chrono::duration<double>(delay).count()* 1'000'000;

            if (verbose) std::cout << cli_blue 
                << "=> <t>_{kfr} = " << dKFR << "ns / " 
                << "=> <t>_{fftw} = " << dFFTW << "ns / " 
                << "<dt>{kfr - fftw} = " << dDelay << "us " << cli_nc << std::endl; 

            gFFTW[w]->SetPoint(i, pow2, dFFTW);
            TKFRLegacy *[w]->SetPoint(i, pow2, dKFR);
            gTimeRelative[w]->SetPoint(i, pow2, dDelay);
            gTimeRatio[w]->SetPoint(i, pow2, same(dFFTW,0) ? 0 : dKFR/dFFTW);

            if (gDisplay) {

                TList *primitivesList = c[w]->GetListOfPrimitives();
                TIter next(primitivesList);
                while (TPad *pad = (TPad*)next()) {
                    pad->cd();
                    c[w]->Update();
                    pad->Draw();
                }

                gSystem->ProcessEvents();
            }
        }
    }







    // /////// 
    // Saving files and drawing plots
    // ///////

    for(int w = 0, W = types.size(); w < W; w++) {
        gFFTW[w]->Write("gFFTW["+types[w]+"]");
        TKFRLegacy *[w]->Write("TKFRLegacy *["+types[w]+"]");
        gTimeRatio[w]->Write("gTimeRatio["+types[w]+"]");
        gTimeRelative[w]->Write("gTimeRelative["+types[w]+"]");
    }
    if(gDisplay) app.Run();

    f->Close();
    std::cout << std::endl << "Benchmark test is over. Results have been saved into: `" << fname << "`" << std::endl;
    
    return 0;
}
