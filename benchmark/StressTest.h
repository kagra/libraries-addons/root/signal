#include <Riostream.h>
#include <TVirtualFFT.h>
#include <TFFTComplex.h>
#include <TKFRLegacy *Complex.h>

#include <TFFTRealComplex.h>
#include <TKFRLegacy *RealComplex.h>

#include <TFFTComplexReal.h>
#include <TKFRLegacy *ComplexReal.h>

#include <TCanvas.h>
#include <TPad.h>
#include <TClass.h>
#include <TFile.h>
#include <TROOT.h>
#include <complex.h>

#include "TApplication.h"
#include <chrono>
#include <TProfile.h>
#include <TSystem.h>
#include <TF1.h>
#include <TMath.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TGraph.h>

const char *cli_red = "\033[1;31m";
const char *cli_green = "\033[1;32m";
const char *cli_blue = "\033[1;34m";
const char *cli_nc = "\033[0m";

const bool check_precision = false;
const double relative_epsilon = NAN;//1e-5;
const double absolute_epsilon = 1e-2;

const int iterations = 10;
const int n = 18;
const int N = pow(2, n);
const int Ni = 512;

const int bLogx = n > 10;
const int refresh = 16;
bool gDisplay = !gROOT->IsBatch();
TString fname = "StressTest.root";

using time_ns = std::chrono::nanoseconds;
// const bool check_precision = false;
// const double relative_epsilon = 1e-5;
// const double absolute_epsilon = 1e-5;

// NB: R2R is not implemented on purpose.. 
//     The point of this benchmark is to compare dft and real dft, not data manipulation.

using complex = std::complex<double>;
bool same(double a, double b, double absolute_epsilon = NAN, double relative_epsilon = NAN)
{
    if(!std::isnan(relative_epsilon) && !std::isnan(absolute_epsilon)) {
        return (abs(a - b)/abs(a) < relative_epsilon) && (abs(a - b) < absolute_epsilon);
    }

    if(!std::isnan(relative_epsilon)) {
        return (abs(a - b)/abs(a) < relative_epsilon);
    }

    if(!std::isnan(absolute_epsilon)) {
        return (abs(a - b) < absolute_epsilon);
    }

    return (abs(a - b) < std::numeric_limits<double>::epsilon());
}

bool same(complex a, complex b, double absolute_epsilon = NAN, double relative_epsilon = NAN)
{ 
    if(check_precision) std::cout << a << " ?= " << b << std::endl;
    return same(a.real(), b.real(), absolute_epsilon, relative_epsilon) && same(a.imag(), b.imag(), absolute_epsilon, relative_epsilon);
}

std::vector<complex> Transform(std::vector<double> in, TVirtualFFT *fft)
{ 
    std::vector<double> temp(in.size(), 0);
    fft->SetPointsComplex(&in[0], &temp[0]);
    fft->Transform();
    
    std::vector<complex> out;

    Double_t re, im;
    for (Int_t i=0, II = in.size(); i < II; i++) {
        fft->GetPointComplex(i, re, im);
        out.push_back(complex(re, im));
    }

    return out;
}

int C2C(int size, time_ns &fftw_duration, time_ns &kfr_duration, TString kfr_flags = "") {
 
    std::vector<double> in;
    for(int i = 0; i < size; i++) {
        in.push_back(rand());
    }

    // FFTW complex-to-complex plan initialization
    TFFTComplex *fftw = new TFFTComplex(in.size(), false);
    fftw->Init("C2C", 1, 0);
    Transform(in, fftw); // Pre-heat

    // KFR complex-to-complex plan initialization
    TKFRLegacy *Complex *kfr  = new TKFRLegacy *Complex(in.size(), false);
    kfr->Init("C2C "+ kfr_flags, 1, 0);
    Transform(in, kfr); // Pre-heat

    auto fftw_start = std::chrono::high_resolution_clock::now();
    std::vector<complex> fftw_out = Transform(in, fftw);
    fftw_duration = std::chrono::high_resolution_clock::now() - fftw_start;

    auto kfr_start = std::chrono::high_resolution_clock::now();
    std::vector<complex> kfr_out = Transform(in, kfr);
    kfr_duration = std::chrono::high_resolution_clock::now() - kfr_start;

    // Precision check
    bool ret = 0;
    for (Int_t i = 1, II = fftw_out.size(); i < II; i++) {
        if(check_precision && !same(fftw_out[II-i], kfr_out[i], absolute_epsilon, relative_epsilon)) ret = 1;
    }

    if(fftw) {
        delete fftw;
        fftw = NULL;
    }

    if(kfr) {
        delete kfr;
        kfr = NULL;
    }

    return ret;
}

int R2C(int size, time_ns &fftw_duration, time_ns &kfr_duration, TString kfr_flags = "") {
 
    std::vector<double> in;
    for(int i = 0; i < size; i++) {
        in.push_back(rand());
    }

    // FFTW real-to-complex plan
    TFFTRealComplex *fftw = new TFFTRealComplex(in.size(), false);
    fftw->Init("R2C", 1, 0);

    auto fftw_start = std::chrono::high_resolution_clock::now();
    std::vector<complex> fftw_out = Transform(in, fftw);
    fftw_duration = std::chrono::high_resolution_clock::now() - fftw_start;
    
    // KFR real-to-complex plan
    TKFRLegacy *RealComplex *kfr  = new TKFRLegacy *RealComplex(in.size(), false);
    kfr->Init("R2C "+kfr_flags, 1, 0);

    auto kfr_start = std::chrono::high_resolution_clock::now();
    std::vector<complex> kfr_out = Transform(in, kfr);
    kfr_duration = std::chrono::high_resolution_clock::now() - kfr_start;

    // Precision check
    bool ret = 0;
    for (Int_t i = 1, II = fftw_out.size(); i <= II; i++) {
        if(check_precision && !same(fftw_out[II-i], kfr_out[i], absolute_epsilon, relative_epsilon)) ret = 1;
    }

    delete fftw;
    fftw = NULL;

    delete kfr;
    kfr = NULL;

    return ret;
}

int C2R(int size, time_ns &fftw_duration, time_ns &kfr_duration, TString kfr_flags = "") {
 
    std::vector<double> in;
    for(int i = 0; i < size; i++) {
        in.push_back(rand());
    }

    // FFTW complex-to-real plan
    TFFTComplexReal *fftw = new TFFTComplexReal(in.size(), false);
                     fftw->Init("C2R", 1, 0);
    
    auto fftw_start = std::chrono::high_resolution_clock::now();
    std::vector<complex> fftw_out = Transform(in, fftw);
    fftw_duration = std::chrono::high_resolution_clock::now() - fftw_start;

    // KFR complex-to-real plan
    TKFRLegacy *ComplexReal *kfr  = new TKFRLegacy *ComplexReal(in.size(), false);
                     kfr->Init("C2R "+kfr_flags, 1, 0);

    auto kfr_start = std::chrono::high_resolution_clock::now();
    std::vector<complex> kfr_out = Transform(in, kfr);
    kfr_duration = std::chrono::high_resolution_clock::now() - kfr_start;

    // Precision check
    bool ret = 0;
    for (Int_t i = 1, II = fftw_out.size(); i < II; i++) {
        if(check_precision && !same(fftw_out[II-i], kfr_out[i], absolute_epsilon, relative_epsilon)) ret = 1;
    }

    delete fftw;
    fftw = NULL;

    delete kfr;
    kfr = NULL;

    return ret;
}
