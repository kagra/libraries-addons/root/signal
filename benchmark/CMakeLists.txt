
# Version used during development
cmake_minimum_required(VERSION 3.5)
project(RSignal/benchmark)

# Include custom CMake modules and utilities
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/../cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

include(MessageColor)
include(FindPackageStandard)
include(ProjectArchitecture)
include(ParentCMakeOnly)
include(LoadTests)

#
# Looking for benchmark structure
message_title("Benchmark Test")

# Compile libraries
add_subdirectory(fftw)
add_subdirectory(kfr)

# Version used during development

# Collect test sources and display project architecture
FILE(GLOB TESTS "${CMAKE_CURRENT_SOURCE_DIR}/*.C")
DUMP_ARCHITECTURE(TESTS)
message("")

# Get common path for tests
GET_COMMON_PATH(TESTPATH_COMMON TESTS)

# Add a custom target 'benchmark' for running benchmark tests
add_custom_target(benchmark 
    COMMAND ${CMAKE_CTEST_COMMAND} --rerun-failed --output-on-failure 
    COMMENT "Running benchmark tests"
)

# Find test files, prepare test targets, and link required libraries
set(TEST_TARGETS)
foreach(SRC ${TESTS})

    get_filename_component(TEST ${SRC} NAME_WE)
    get_filename_component(TESTPATH ${SRC} PATH)
    file(RELATIVE_PATH TESTPATH_REL ${TESTPATH_COMMON} ${TESTPATH})
    if(NOT "${TESTPATH_REL}" STREQUAL "")
        list(APPEND TESTPATH_REL "/")
    endif()

    message_color("-- Preparing target test `./${TESTPATH_REL}${TEST}`" COLOR GREEN)

    # Prepare test program
    add_gtest(${TEST} ${SRC})
    target_link_package(${TEST} ${LIBRARY}_KFR REQUIRED)
    target_link_package(${TEST} ${LIBRARY}_FFTW REQUIRED)

    # Custom `make tests`
    add_dependencies(benchmark ${TEST})

    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TEST}
        DESTINATION bin
        PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE
        OPTIONAL
    )

endforeach()

