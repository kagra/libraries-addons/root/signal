#! /bin/bash

[[ $_ != $0 ]] && REALPATH=`dirname $(readlink -f ${BASH_SOURCE[0]})` || REALPATH=`dirname $(readlink -f $0)`

if [[ ! -z "$1" || ! -d "$REALPATH/var/build" ]]; then

  mkdir -p $REALPATH/var/build
  cd $REALPATH/var/build

  [ "$1" == "Debug" ] && CMAKE_BUILDTYPE_FLAGS="-DCMAKE_BUILD_TYPE=Debug"
  [ "$1" == "Release" ] && CMAKE_BUILDTYPE_FLAGS="-DCMAKE_BUILD_TYPE=Release"
  [ "$1" == "Doc" ] && CMAKE_OPTIONAL_FLAGS="-DBUILD_LIBRARY='OFF'" || CMAKE_OPTIONAL_FLAGS="-DBUILD_LIBRARY='ON'"

  cmake $REALPATH -DCMAKE_INSTALL_PREFIX=$REALPATH/var/install -DMOTD='"$MOTD"' $CMAKE_OPTIONAL_FLAGS $CMAKE_BUILDTYPE_FLAGS
  [ $? -eq 1 ] && exit 1
fi

cd $REALPATH/var/build
if [[ ! -z `which nproc` ]]; then
        make install -j$(($(nproc --all)-1))
else
        make install -j
fi

[ $? -eq 1 ] && exit 1
cd - > /dev/null 2> /dev/null
