# Include custom standard package
list(PREPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_LIST_DIR}/../Modules)
include(FindPackageStandard)

# Load using standard package finder
find_package_standard(
    NAMES RIOPlus
    HEADERS "TCli.h"
    PATHS ${ROOTPLUS} ${ROOTPLUS_DIR} $ENV{ROOTPLUS} $ENV{ROOTPLUS_DIR}
)
