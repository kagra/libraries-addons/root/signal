#
# ROOT
list(APPEND CMAKE_MODULE_PATH $ENV{ROOTSYS}/etc/cmake)
find_package(ROOT REQUIRED MathMore Matrix) # You can add some additional components from here (e.g. TreePlayer Geom Spectrum Matrix)

include(${ROOT_USE_FILE})
include_directories(${ROOT_INCLUDE_DIRS})

if (ROOT_VERSION VERSION_LESS 6.15/99 OR NOT ROOT_VERSION) # Actually it should be at least 6.18..
        message( FATAL_ERROR "ROOT has to be at least 6.16/00" )
endif()

# Execute root-config --features and capture the output
execute_process(
    COMMAND root-config --features
    OUTPUT_VARIABLE ROOT_FEATURES
    RESULT_VARIABLE RESULT
    ERROR_QUIET
    OUTPUT_STRIP_TRAILING_WHITESPACE
)

# Check if root-config command executed successfully
if(RESULT EQUAL 0)
    # Use a regular expression to find the C++ standard version
    string(REGEX MATCH "cxx[0-9][0-9]" ROOT_CXX_STANDARD_MATCH ${ROOT_FEATURES})
    string(REGEX REPLACE "cxx" "" ROOT_CXX_STANDARD ${ROOT_CXX_STANDARD_MATCH})
    
    if(NOT ${CMAKE_CXX_STANDARD} STREQUAL ${ROOT_CXX_STANDARD})
        if(NOT CMAKE_CONFIGURATION_TYPES)
            message(STATUS "ROOT Standard Compiler set to C++${ROOT_CXX_STANDARD} (not matching `CMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}`)")
        else()
            message(WARNING "ROOT Standard Compiler set to C++${ROOT_CXX_STANDARD} (not matching `CMAKE_CXX_STANDARD=${CMAKE_CXX_STANDARD}`)")
        endif()
        set(CMAKE_CXX_STANDARD ${ROOT_CXX_STANDARD} CACHE STRING "Default value for CXX_STANDARD property of targets")
        message(WARNING "Updating to C++${CMAKE_CXX_STANDARD} standard compiler: ${CMAKE_CXX_COMPILER}")
    endif()
endif()
