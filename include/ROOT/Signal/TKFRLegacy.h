#pragma once

#include <Riostream.h>
#include <TString.h>
#include <TObject.h>
#include <TMatrixT.h>
#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFormula.h>
#include <Math/Interpolator.h>
#include <Math/Polynomial.h>

#include <Math/Polynomial.h>
#include <TComplex.h>
#include <TMatrixDEigen.h>
#include <TDecompLU.h>

#include <thread>
#include <complex>
#include <vector>
#include <map>

#include "ROOT/Signal/EnumTypes.h"
#include "ROOT/Signal/TransferTypes.h"

namespace ROOT {
    namespace Signal {

       template <typename T = double>
       class TKFRLegacy {

       public:
              
              const static std::complex<T> i;

              // Complex calculation aliases
              using NumericT = T;
              using ComplexT = std::complex<T>;

              using Vector1N = std::vector<NumericT>;
              using Vector1C = std::vector<ComplexT>;

              using Vector2N = std::vector<Vector1N>;
              using Vector2C = std::vector<Vector1C>;
              
              using ZPK = ROOT::Signal::_ZPK<T>;
              using SS  = ROOT::Signal::_SS<T>;
              using TF  = ROOT::Signal::_TF<T>;
              using SOS = ROOT::Signal::_SOS<T>;

              using TQuad = ROOT::Signal::_TQuad<T>;
              using TBiquad = ROOT::Signal::_TBiquad<T>;
              
       private:
              class Impl;
              Impl *pImpl = NULL;
              
       public:

              static bool IsAlreadyRunningMT;
              static std::atomic<int> finishedWorkers;

              TKFRLegacy();
              ~TKFRLegacy();

              const char *Version();

              NumericT Get(ComplexPart part, const ComplexT &z);
              Vector1N Get(ComplexPart part, const Vector1C &z);
              Vector2N Get(ComplexPart part, const Vector2C &z);

              ComplexT Complex(const NumericT &a, const NumericT &b = 0);
              Vector1C Complex(const Vector1N &a, const Vector1N &b = Vector1N());
              Vector2C Complex(const Vector2N &a, const Vector2N &b = Vector2N());
              
              NumericT Mag(const ComplexT &z) { return this->Get(ComplexPart::Magnitude, z); }
              Vector1N Mag(const Vector1C &z) { return this->Get(ComplexPart::Magnitude, z); }
              Vector2N Mag(const Vector2C &z) { return this->Get(ComplexPart::Magnitude, z); }

              NumericT dB(const ComplexT &z) { return this->Get(ComplexPart::Decibel, z); }
              Vector1N dB(const Vector1C &z) { return this->Get(ComplexPart::Decibel, z); }
              Vector2N dB(const Vector2C &z) { return this->Get(ComplexPart::Decibel, z); }

              NumericT Argument(const ComplexT &z) { return this->Get(ComplexPart::Argument, z); }
              Vector1N Argument(const Vector1C &z) { return this->Get(ComplexPart::Argument, z); }
              Vector2N Argument(const Vector2C &z) { return this->Get(ComplexPart::Argument, z); }

              NumericT Phase(const ComplexT &z) { return this->Get(ComplexPart::Phase, z); }
              Vector1N Phase(const Vector1C &z) { return this->Get(ComplexPart::Phase, z); }
              Vector2N Phase(const Vector2C &z) { return this->Get(ComplexPart::Phase, z); }

              NumericT Real(const ComplexT &z) { return this->Get(ComplexPart::Real, z); }
              Vector1N Real(const Vector1C &z) { return this->Get(ComplexPart::Real, z); }
              Vector2N Real(const Vector2C &z) { return this->Get(ComplexPart::Real, z); }
              
              NumericT Imag(const ComplexT &z) { return this->Get(ComplexPart::Imag, z); }
              Vector1N Imag(const Vector1C &z) { return this->Get(ComplexPart::Imag, z); }
              Vector2N Imag(const Vector2C &z) { return this->Get(ComplexPart::Imag, z); }

              Vector1N Add(const Vector1N &a, const Vector1N &b );
              Vector1C Add(const Vector1C &z1, const Vector1N &z2 );
              Vector1C Add(const Vector1N &z1, const Vector1C &z2 );
              Vector1N Add(const Vector1N &A, const NumericT &b);
              Vector1C Add(const Vector1C &z1, const NumericT &z2);
              Vector1C Add(const NumericT &z2, const Vector1C &z1);
              Vector1C Add(const Vector1C &z1, const ComplexT &z2);
              Vector1C Add(const ComplexT &z2, const Vector1C &z1);
              Vector1C Add(const Vector1C &z1, const Vector1C &z2);

              Vector1C Divide(const Vector1C &z, const Vector1N &x, bool discardNaN = false);
              Vector1C Divide(const Vector1N &z, const Vector1C &x, bool discardNaN = false);
              Vector1C Divide(const Vector1C &z, const NumericT &k, bool discardNaN = false);
              Vector1N Divide(const Vector1N &x, const Vector1N &y, bool discardNaN = false);
              Vector1N Divide(const Vector1N &a, const NumericT &k, bool discardNaN = false);
              Vector1C Divide(const Vector1C &z1, const Vector1C &z2, bool discardNaN = false);

              Vector1C Multiply(const Vector1C &z, const Vector1N &x);
              Vector1C Multiply(const Vector1N &x, const Vector1C &z);
              Vector1C Multiply(const Vector1C &z, const NumericT &k);
              Vector1C Multiply(const NumericT &k, const Vector1C &z);
              Vector1N Multiply(const Vector1N &a, const Vector1N &b);
              Vector1C Multiply(const Vector1C &z1, const Vector1C &z2);

              Vector1N Multiply(const Vector1N &a, const NumericT &k);
              Vector1N Multiply(const NumericT &k, const Vector1N &a);
              NumericT Sum(const Vector1N &v);
              ComplexT Sum(const Vector1C &v);
              NumericT SumX(const Vector1N &v);
              ComplexT SumX(const Vector1C &v);
              NumericT SumX2(const Vector1N &a);
              ComplexT SumX2(const Vector1C &a);

              NumericT SumXY(const Vector1N &a, const Vector1N &b);
              ComplexT SumXY(const Vector1N &a, const Vector1C &b);
              ComplexT SumXY(const Vector1C &a, const Vector1N &b);
              ComplexT SumXY(const Vector1C &a, const Vector1C &b);

              Vector1C Subtract(const Vector1C &z1, const Vector1C &z2);
              Vector1C Subtract(const Vector1N &z1, const Vector1C &z2);
              Vector1C Subtract(const Vector1C &z1, const Vector1N &z2);
              Vector1N Subtract(const Vector1N &a, const Vector1N &b );
              Vector1N Subtract(const Vector1N &a, const NumericT &b);
              Vector1C Subtract(const Vector1C &a, const ComplexT &b);
              Vector1C Subtract(const Vector1C &a, const NumericT &b);
              Vector1C Subtract(const NumericT &b, const Vector1C &a);
              Vector1C Subtract(const ComplexT &b, const Vector1C &a);
              Vector1C Subtract(const Vector1N &a, const ComplexT &b);

              Vector1C Power(const Vector1C &X, const NumericT &y, bool handmade = true);
              Vector1N Power(const Vector1N &X, const NumericT &y, bool handmade = true);
              NumericT Power(const NumericT &x, const NumericT &y, bool handmade = true);
              ComplexT Power(const ComplexT &x, const NumericT &y, bool handmade = true);
              
              Vector1C Sqrt(const Vector1C &X);
              Vector1N Sqrt(const Vector1N &X);
              NumericT Sqrt(const NumericT &x);
              ComplexT Sqrt(const ComplexT &X);

              NumericT Mean(const Vector1N &V);
              ComplexT Mean(const Vector1C &V);
              Vector1N Mean(const Vector2N &V);
              Vector1C Mean(const Vector2C &V);

              Vector1C Scale(const NumericT &k, const Vector1C &z);
              Vector1C Scale(const Vector1C &z, const NumericT &k);
              Vector1N Scale(const NumericT &k, const Vector1N &a);
              Vector1N Scale(const Vector1N &a, const NumericT &k);

              NumericT Abs(const NumericT &z);
              NumericT Abs(const ComplexT &z);
              Vector1N Abs(const Vector1N &x);
              Vector1N Abs(const Vector1C &z);

       public:
              void Print(TBiquad biquad, TString prefix = "");
              void Print(ZPK zpk);
              void Print(SOS sos);
              void Print(TF TF);
              void Print(SS ss, TString prefix = "");
              void Print(std::vector<ZPK> zpk);
              void Print(std::vector<SOS> sos);
              void Print(std::vector<TF> tf);
              void Print(std::vector<SS> ss);
              
              template <typename Tx>
              void Print(std::map<Tx, T> m);
              template <typename Tx>
              void Print(std::vector<std::map<Tx, T>> vm);

       public:
       
              //
              // Handling DFd::vectorsT for st
              inline Vector1N  DFT(ComplexPart part, const Vector1N &x, Domain domain = Domain::Frequency,        Option_t *option = "", int nDFT = 0) { return Get(part, DFT(Complex(x), domain, option, nDFT)); }
              inline Vector1C  DFT(                  const Vector1N &x, Domain domain = Domain::Frequency,        Option_t *option = "", int nDFT = 0) { return           DFT(Complex(x), domain, option, nDFT); }
                     Vector1C  DFT(                  const Vector1C &x, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0);
                     // Vector1C  DFT(                  TH1 *h, Option_t *option = "x", Domain domain = Domain::ComplexFrequency, int nDFT = 0);
                     // TH1*      DFT(ComplexPart part, TH1 *h, Option_t *option = "x", int nDFT = 0);

              inline Vector1N iDFT(ComplexPart part, const Vector1C &X, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0)  { return Get(part, iDFT(X, domain, option, nDFT)); }
                     Vector1C iDFT(                  const Vector1C &X, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0);

              // https://research.iaun.ac.ir/pd/naghsh/pdfs/UploadFile_2230.pdf (Oppenheim, 1999, p173)
              Vector1N Interpolate(const Vector1N &x0,                                       const ROOT::Math::Interpolator &itp);
              Vector1N Interpolate(int N,                                 const Vector1N &y, ROOT::Math::Interpolation::Type type = ROOT::Math::Interpolation::kLINEAR);
              Vector1N Interpolate(const Vector1N &x0, const Vector1N &x, const Vector1N &y, ROOT::Math::Interpolation::Type type = ROOT::Math::Interpolation::kLINEAR);
              // TH1D    *Interpolate(const Vector1N &x0, TH1D *h,                              ROOT::Math::Interpolation::Type type = ROOT::Math::Interpolation::kLINEAR);

              inline Vector1C Adjust(const Vector1N &x,         double amplitude, NumericT phase, double offset, NumericT fs, Domain domain = Domain::Frequency,        Option_t *option = "x") { return Adjust(Complex(x), amplitude, phase, offset, fs, domain, option); }
                     Vector1C Adjust(const Vector1C &x,         double amplitude, NumericT phase, double offset, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "x");
                     Vector1C Adjust(                  TH1 *h1, double amplitude, NumericT phase, double offset,              Domain domain = Domain::ComplexFrequency, Option_t *option = "x");
                     // TH1*     Adjust(ComplexPart part, TH1 *h1, double amplitude, NumericT phase, double offset,              Domain domain = Domain::ComplexFrequency, Option_t *option = "x");

              inline Vector2C  SFT(                  const Vector1N &x, NumericT fs, Domain domain = Domain::Frequency,        Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return                 SFT(Complex(x), fs, domain, option, nDFT, noverlap, window, ab, symmetry); }
              inline Vector2N  SFT(ComplexPart part, const Vector1N &x, NumericT fs, Domain domain = Domain::Frequency,        Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return this->Get(part, SFT(Complex(x), fs, domain, option, nDFT, noverlap, window, ab, symmetry)); }
                     Vector2C  SFT(                  const Vector1C &x, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
              inline Vector2N  SFT(ComplexPart part, const Vector1C &x, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return this->Get(part, SFT(x, fs, domain, option, nDFT, noverlap, window, ab, symmetry)); }
                     // TH2*      SFT(ComplexPart part, TH1 *h                        , Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              #ifndef NDEBUG
                     inline Vector1N Mirroring   (const Vector1N &input, SpectrumProperty spectrum = SpectrumProperty::OneSided, unsigned int offset = 0, unsigned int foldingOffset = 0                        ) { return this->pImpl->Mirroring(input, offset, foldingOffset); }
                     inline Vector1C Mirroring   (const Vector1C &input, SpectrumProperty spectrum = SpectrumProperty::TwoSided, unsigned int offset = 0, unsigned int foldingOffset = 0, bool bConjugate = true) { return this->pImpl->Mirroring(input, offset, foldingOffset, bConjugate); }
                     inline Vector1N MirroringDFT(const Vector1N &input, SpectrumProperty spectrum = SpectrumProperty::OneSided                        ) { return this->pImpl->MirroringDFT(input); }
                     inline Vector1C MirroringDFT(const Vector1C &input, SpectrumProperty spectrum = SpectrumProperty::TwoSided, bool bConjugate = true) { return this->pImpl->MirroringDFT(input, bConjugate); }
                     inline Vector1N MirroringPSD(const Vector1N &input, SpectrumProperty spectrum = SpectrumProperty::OneSided                        ) { return this->pImpl->MirroringPSD(input); }
              #endif

              inline Vector1N iSFT(ComplexPart part, const Vector2C &X, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return this->Get(part,              iSFT(X, fs, domain, option, nDFT, noverlap, window, ab, symmetry)); }
                     Vector1C iSFT(                  const Vector2C &X, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              bool NOLA(double nDFT, NumericT noverlap, WindowType window, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric, double tolerance = 1e-10);
              bool COLA(double nDFT, NumericT noverlap, WindowType window, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric, double tolerance = 1e-10);

              inline Vector2N Spectrogram(const Vector1N &x, NumericT fs, Domain domain = Domain::Frequency,        Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Mag(SFT(x, fs, domain, option, nDFT, noverlap, window, ab, symmetry)); }
                     Vector2N Spectrogram(const Vector1C &x, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
                     // Vector2N Spectrogram(TH2 *h,                         Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0, NumericT noverlap = 0.5, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              //
              // Handling signal injection
              inline Vector1N Inject(const Vector1N &x, const Vector1N &y,                                           Option_t *option = "",  int nDFT = 0) { return Real(Inject(Complex(x), Complex(y), Domain::Frequency, option, nDFT)); }
                     Vector1C Inject(const Vector1C &x, const Vector1C &y, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0);
                     // TH1*     Inject(TH1 *h1,           TH1 *h2,                                                     Option_t *option = "x", int nDFT = 0);

              // Auto correlation
              inline Vector1N Autocorrelate(const Vector1N &x,                                           Option_t *option = "",  int nDFT = 0) { return Real(Correlate(x, x,         option, nDFT)); }
              inline Vector1C Autocorrelate(const Vector1C &x, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0) { return      Correlate(x, x, domain, option, nDFT); }
              // inline TH1*     Autocorrelate(TH1 *h,                                                      Option_t *option = "x", int nDFT = 0) { return      Correlate(ComplexPart::Real, h, h, option, nDFT); }

              // Cross correlation
              inline Vector1C Correlate(                  const Vector1N  &x, const Vector1N  &y,                                           Option_t *option = "",  int nDFT = 0) { return               Correlate(Complex(x), Complex(y), Domain::Frequency, option, nDFT); }
              inline Vector1N Correlate(ComplexPart part, const Vector1N  &x, const Vector1N  &y,                                           Option_t *option = "",  int nDFT = 0) { return Get(part, Correlate(Complex(x), Complex(y), Domain::Frequency, option, nDFT)); }
                     Vector1C Correlate(                        Vector1C   x,       Vector1C   y, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0);
              inline Vector1N Correlate(ComplexPart part,       Vector1C   x,       Vector1C   y, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0) { return Get(part, Correlate(                x ,                 y , domain           , option, nDFT)); }
              // inline TH1*     Correlate(                                     TH1 *h1,            TH1 *h2,                                           Option_t *option = "x", int nDFT = 0) { return           Correlate(ComplexPart::Real, h1, h2,         option, nDFT); }
                     // TH1*     Correlate(ComplexPart part,            TH1 *h1,            TH1 *h2,                                           Option_t *option = "x", int nDFT = 0);

              // Convolution
              inline Vector1C Convolve(                  const Vector1N &x, const Vector1N  &y,                                           Option_t *option = "",  int nDFT = 0) { return               Convolve(Complex(x), Complex(y), Domain::Frequency, option, nDFT); }
              inline Vector1N Convolve(ComplexPart part, const Vector1N &x, const Vector1N  &y,                                           Option_t *option = "",  int nDFT = 0) { return Get(part, Convolve(Complex(x), Complex(y), Domain::Frequency, option, nDFT)); }
                     Vector1C Convolve(                        Vector1C  x,       Vector1C   y, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0);
              inline Vector1N Convolve(ComplexPart part,       Vector1C  x,       Vector1C   y, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0) { return Get(part, Convolve(                x ,                 y , domain           , option, nDFT)); }
              // inline TH1*     Convolve(                        TH1 *h1,           TH1 *h2,                                                Option_t *option = "x", int nDFT = 0) { return               Convolve(ComplexPart::Real, h1, h2,         option, nDFT); }
                     // TH1*     Convolve(ComplexPart part,       TH1 *h1,           TH1 *h2,                                                Option_t *option = "x", int nDFT = 0);

              // Spectrum and spectral densities
              inline Vector1N AmplitudeSpectrum(const Vector1N &x, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return PowerSpectrum(Complex(x), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     Vector1N AmplitudeSpectrum(const Vector1C &x, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
                     // TH1*     AmplitudeSpectrum(TH1 *h1,                                                                  Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N AmplitudeSpectralDensity(const Vector1N &x, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return this->Sqrt(PowerSpectralDensity(Complex(x), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
              inline Vector1N AmplitudeSpectralDensity(const Vector1C &x, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return this->Sqrt(PowerSpectralDensity(        x , fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
                     // TH1*     AmplitudeSpectralDensity(TH1 *h,                                                                   Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N PowerSpectrum(const Vector1N &x, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Real(CrossSpectrum(Complex(x), Complex(x), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
              inline Vector1N PowerSpectrum(const Vector1C &x, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Real(CrossSpectrum(        x ,         x , fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
                     // TH1*     PowerSpectrum(TH1 *h1,                                                                  Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N PowerSpectralDensity(const Vector1N &x, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Real(CrossSpectralDensity(Complex(x), Complex(x), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
              inline Vector1N PowerSpectralDensity(const Vector1C &x, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Real(CrossSpectralDensity(        x ,         x , fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
                     // TH1*     PowerSpectralDensity(TH1 *h,                                                                   Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N CrossSpectrum(ComplexPart part, const Vector1N &x, const Vector1N &y, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectrum(part, Complex(x), Complex(y), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1C CrossSpectrum(                  const Vector1N &x, const Vector1N &y, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectrum(      Complex(x), Complex(y), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1N CrossSpectrum(ComplexPart part, const Vector1C &x, const Vector1N &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectrum(part,         x ,         y , fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1C CrossSpectrum(                  const Vector1C &x, const Vector1N &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectrum(              x ,         y , fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1N CrossSpectrum(ComplexPart part, const Vector1N &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectrum(part, Complex(x),         y , fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1C CrossSpectrum(                  const Vector1N &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectrum(      Complex(x),         y , fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1N CrossSpectrum(ComplexPart part, const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Get(part, CrossSpectrum(              x ,         y , fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
                     Vector1C CrossSpectrum(                  const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
                     // TH1*     CrossSpectrum(ComplexPart part, TH1 *h1,           TH1 *h2,                                                                  Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N CrossSpectralDensity(ComplexPart part, const Vector1N &x, const Vector1N &y, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectralDensity(part, Complex(x), Complex(y), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1C CrossSpectralDensity(                  const Vector1N &x, const Vector1N &y, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectralDensity(      Complex(x), Complex(y), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1N CrossSpectralDensity(ComplexPart part, const Vector1C &x, const Vector1N &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectralDensity(part,         x , Complex(y), fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1C CrossSpectralDensity(                  const Vector1C &x, const Vector1N &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectralDensity(              x , Complex(y), fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1N CrossSpectralDensity(ComplexPart part, const Vector1N &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectralDensity(part, Complex(x),         y , fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1C CrossSpectralDensity(                  const Vector1N &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return           CrossSpectralDensity(      Complex(x),         y , fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry);  }
              inline Vector1N CrossSpectralDensity(ComplexPart part, const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Get(part, CrossSpectralDensity(              x ,         y , fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry)); }
                     Vector1C CrossSpectralDensity(                  const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
                     // TH1*     CrossSpectralDensity(ComplexPart part, TH1 *h1,           TH1 *h2,                                                                  Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N Coherence(const Vector1N &x, const Vector1N &y, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Coherence(Complex(x), Complex(y), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     Vector1N Coherence(const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
                     // TH1*     Coherence(TH1 *h1,           TH1 *h2,                                                                  Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = -1, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N Periodogram(const Vector1N &x, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return CrossPeriodogram(Complex(x), Complex(x), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
              inline Vector1N Periodogram(const Vector1C &x, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return CrossPeriodogram(x, x,                   fs, domain,            option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     // TH1*     Periodogram(TH1 *h,                                                                   Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N CrossPeriodogram(const Vector1N &x, const Vector1N &y, NumericT fs,                                           Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return CrossPeriodogram(Complex(x), Complex(y), fs, Domain::Frequency, option, nDFT, estimator, noverlap, average, detrend, window, ab, symmetry); }
                     Vector1N CrossPeriodogram(const Vector1C &x, const Vector1C &y, NumericT fs, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
                     // TH1*     CrossPeriodogram(TH1 *h1,           TH1 *h2,                                                                  Option_t *option = "x", int nDFT = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              inline Vector1N Impulse(NumericT fs, double duration, const NumericT  amplitude,  const NumericT  freq               ) { return Real(Impulse(fs, duration, Vector1N({amplitude}), Vector1N({freq}), Domain::Time)); };
              inline Vector1C Impulse(NumericT fs, double duration, const NumericT  amplitude,  const NumericT  freq, Domain domain) { return Impulse(fs,duration, Vector1N({amplitude}), Vector1N({freq}), domain); };
              inline Vector1N Impulse(NumericT fs, double duration, const Vector1N &amplitudes, const Vector1N &freqs) { return Real(Impulse(fs, duration, amplitudes, freqs, Domain::Time)); }
                     Vector1C Impulse(NumericT fs, double duration, const Vector1N &amplitudes, const Vector1N &freqs, Domain domain);

              inline Vector1N Waveform(NumericT fs, double duration, NumericT  formulaStr                                                           ) { return Waveform(fs, duration,      "[0]", Vector1N({formulaStr})          ); }
              inline Vector1N Waveform(NumericT fs, double duration, TString   formulaStr                                                           ) { return Waveform(fs, duration, formulaStr, Vector1N({})                    ); }
              inline Vector1N Waveform(NumericT fs, double duration, TString   formulaStr, Vector1N params = {},                  int iterations = 1) { return Waveform(fs, duration, formulaStr, params, Vector1N({}), iterations); }
                     Vector1N Waveform(NumericT fs, double duration, TString   formulaStr, Vector1N params, Vector1N paramSigmas, int iterations = 1);
              inline Vector1N Waveform(NumericT fs, double duration, TFormula *formula) { return Waveform(fs, duration, formula); }
              inline Vector1N Waveform(NumericT fs, double duration, TFormula *formula, Vector1N params,                       int iterations = 1) { return Waveform(fs, duration, formula   , params, Vector1N({}), iterations); }
                     Vector1N Waveform(NumericT fs, double duration, TFormula *formula, Vector1N params, Vector1N paramSigmas, int iterations = 1);
              
              inline Vector1N Waveform(NumericT fs, double duration,                   WindowType window, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Waveform(fs, duration, 1.0, window, ab, symmetry); }
                     Vector1N Waveform(NumericT fs, double duration, double dutyCycle, WindowType window, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
       
              inline Vector1N Window(size_t N,                   WindowType window, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Window(N, 1.0, window, ab, symmetry); }
                     Vector1N Window(size_t N, double dutyCycle, WindowType window, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
              
              inline Vector1N Windowing(const Vector1N &x,                   WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Windowing(x, 1.0,         window, ab, symmetry); }
              inline Vector1C Windowing(const Vector1C &x,                   WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Windowing(x, 1.0,         window, ab, symmetry); }
                     Vector1N Windowing(const Vector1N &x, double dutyCycle, WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
                     Vector1C Windowing(const Vector1C &x, double dutyCycle, WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
              // inline TH1*     Windowing(TH1 *h,                                          WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Windowing(h, "x"   , 1.0      , window, ab, symmetry); }
              // inline TH1*     Windowing(TH1 *h, Option_t *option,                        WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Windowing(h, option, 1.0      , window, ab, symmetry); }
              // inline TH1*     Windowing(TH1 *h,                        double dutyCycle, WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Windowing(h, "x"   , dutyCycle, window, ab, symmetry); }
              //        TH1*     Windowing(TH1 *h, Option_t *option,      double dutyCycle, WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              Vector1N Detrend(const Vector1N &x, Trend detrend = Trend::Constant);
              Vector1C Detrend(const Vector1C &x, Trend detrend = Trend::Constant);
              // TH1*     Detrend(TH1 *h,            Trend detrend = Trend::Constant, Option_t *option = "");                

              // compute random variable based on inverse transform method.
              Vector1N Noise(int N, TF1 *pdf);
              Vector1N Noise(int N,                                                       NumericT amplitude = 1, NumericT mean = 0, NumericT sigma = 1, NumericT alpha = 1, NumericT mean2 = 0, NumericT sigma2 = 1);
              // TH1*     Noise(TString name, TString title, NumericT fs, NumericT duration, NumericT amplitude = 1, NumericT mean = 0, NumericT sigma = 1, NumericT alpha = 1, NumericT mean2 = 0, NumericT sigma2 = 1);                   

              //  Finite Impulse Response
              inline Vector1C Filtering(const Vector1C &x, FilterType filter, const Vector1N &cutoff, WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric){ return Complex(Filtering(Real(x), filter, cutoff, window, ab, symmetry), Filtering(Imag(x), filter, cutoff, window, ab, symmetry)); }
                     Vector1N Filtering(const Vector1N &x, FilterType filter, const Vector1N &cutoff, WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
              // inline TH1*     Filtering(TH1 *h,                        FilterType filter, const Vector1N &cutoff, WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Filtering(h, "x", filter, cutoff, window, ab, symmetry); }
              //        TH1*     Filtering(TH1 *h,      Option_t *option, FilterType filter, const Vector1N &cutoff, WindowType window = WindowType::None, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              // Infinite Impulse Response
              inline Vector1N Filtering(const Vector1N &x,                       ZPK  zpk, Direction direction = Direction::Both) { return Filtering(x, SecondOrderSections(zpk), direction); }
              inline Vector1C Filtering(const Vector1C &x,                       ZPK  zpk, Direction direction = Direction::Both) { return Filtering(x, SecondOrderSections(zpk), direction); }
              inline Vector1N Filtering(const Vector1N &x,                       SOS  sos, Direction direction = Direction::Both) { return Filtering(x, std::vector<SOS>({sos}), direction); }
              inline Vector1C Filtering(const Vector1C &x,                       SOS  sos, Direction direction = Direction::Both) { return Complex(Filtering(Real(x), std::vector<SOS>({sos}), direction), Filtering(this->Imag(x), std::vector<SOS>({sos}), direction)); }
              inline Vector1C Filtering(const Vector1C &x,           std::vector<SOS> sos, Direction direction = Direction::Both) { return Complex(Filtering(Real(x),                   sos,   direction), Filtering(this->Imag(x),                   sos,   direction)); }
                     Vector1N Filtering(const Vector1N &x,           std::vector<SOS> sos, Direction direction = Direction::Both);

              // inline TH1*     Filtering(TH1 *h,                   TString alias, ZPK  zpk, Direction direction = Direction::Both) { return Filtering(h,         alias, SecondOrderSections(zpk), direction); }
              // inline TH1*     Filtering(TH1 *h, Option_t *option, TString alias, ZPK  zpk, Direction direction = Direction::Both) { return Filtering(h, option, alias, SecondOrderSections(zpk), direction); }
              // inline TH1*     Filtering(TH1 *h,                   TString alias,             SOS  sos, Direction direction = Direction::Both) { return Filtering(h,    "x", alias, std::vector<SOS>({sos}), direction); }
              // inline TH1*     Filtering(TH1 *h, Option_t *option, TString alias,             SOS  sos, Direction direction = Direction::Both) { return Filtering(h, option, alias, std::vector<SOS>({sos}), direction); }
              // inline TH1*     Filtering(TH1 *h,                   TString alias, std::vector<SOS> sos, Direction direction = Direction::Both) { return Filtering(h,    "x", alias,  sos , direction); }
              //        TH1*     Filtering(TH1 *h, Option_t *option, TString alias, std::vector<SOS> sos, Direction direction = Direction::Both);

              inline Vector1N  MagnitudeResponse(                             NumericT fs, const             SOS  &sos) { return MagnitudeResponse(fs, std::vector<SOS>({sos})); }
              inline Vector1N  MagnitudeResponse(                             NumericT fs, const std::vector<SOS> &sos) { return FrequencyResponse(ComplexPart::Decibel, fs, std::vector<SOS>({sos}), Domain::Frequency); }
              inline TH1*      MagnitudeResponse(TString name, TString title, NumericT fs, const             SOS  &sos) { return MagnitudeResponse(name, title, fs, sos); }
                     TH1*      MagnitudeResponse(TString name, TString title, NumericT fs, const std::vector<SOS> &sos);

              inline Vector1N  PhaseResponse(                             NumericT fs, const             SOS  &sos) { return PhaseResponse(fs, std::vector<SOS>({sos})); }
              inline Vector1N  PhaseResponse(                             NumericT fs, const std::vector<SOS> &sos) { return FrequencyResponse(ComplexPart::Phase, fs, std::vector<SOS>({sos}), Domain::Frequency); }
              inline TH1*      PhaseResponse(TString name, TString title, NumericT fs, const             SOS  &sos) { return PhaseResponse(name, title, fs, sos); }
                     TH1*      PhaseResponse(TString name, TString title, NumericT fs, const std::vector<SOS> &sos);

              inline Vector1N  FrequencyResponse(                             ComplexPart part, NumericT fs, const             SOS  &sos, Domain domain = Domain::ComplexFrequency) { return           FrequencyResponse(part, fs, std::vector<SOS>({sos}), domain);  }
              inline Vector1N  FrequencyResponse(                             ComplexPart part, NumericT fs, const std::vector<SOS> &sos, Domain domain = Domain::ComplexFrequency) { return Get(part, FrequencyResponse(      fs, std::vector<SOS>({sos}), domain)); }
              inline Vector1C  FrequencyResponse(                                               NumericT fs, const             SOS  &sos, Domain domain = Domain::ComplexFrequency) { return           FrequencyResponse(      fs, std::vector<SOS>({sos}), domain);  }
                     Vector1C  FrequencyResponse(                                               NumericT fs, const std::vector<SOS> &sos, Domain domain = Domain::ComplexFrequency);
              inline TH1*      FrequencyResponse(TString name, TString title, ComplexPart part, NumericT fs, const             SOS  &sos, Domain domain = Domain::ComplexFrequency) { return FrequencyResponse(name, title, part ,fs, sos, domain); }
                     TH1*      FrequencyResponse(TString name, TString title, ComplexPart part, NumericT fs, const std::vector<SOS> &sos, Domain domain = Domain::ComplexFrequency);

              inline TCanvas* BodePlot(TString name, TString title, Option_t *opt, NumericT fs,             SOS  sos) { return BodePlot(name, title, opt, fs, std::vector<SOS>({sos})); }
                     TCanvas* BodePlot(TString name, TString title, Option_t *opt, NumericT fs, std::vector<SOS> sos);

              // Wiener filter
              inline Vector1N WienerFiltering(const Vector1N &x, NumericT fs, const Vector1N &PSD = Vector1N{}, Option_t *option = "", int nDFT = 0) { return Real(WienerFiltering(Complex(x), fs, PSD, Domain::Frequency, option, nDFT)); }
                     Vector1C WienerFiltering(const Vector1C &x, NumericT fs, Vector1N PSD = Vector1N{}, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0);
                     // TH1*     WienerFiltering(TH1 *h, TH1 *hNoisePSD, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0);

              // Matched filter
              inline Vector1N MatchedFiltering(const Vector1N &x, NumericT fs, const Vector1N &PSD = Vector1N{}, Option_t *option = "", int nDFT = 0) { return Real((MatchedFiltering(Complex(x), fs, PSD, Domain::Frequency, option, nDFT))); }
                     Vector1C MatchedFiltering(const Vector1C &x, NumericT fs,       Vector1N  PSD = Vector1N{}, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0);
                     // Vector1C MatchedFiltering(TH1 *h, TH1 *hNoisePSD, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0);

              Vector1N Axis(const double duration, const NumericT fs) { return this->pImpl->Range(duration*fs+1, 1./fs); }
              Vector1N Range(const int N) { return this->Axis(N, 1.); }

              inline Vector1C Hilbert(                  const Vector1N &x, Option_t *option = "",  int nDFT = 0) { return Hilbert(Complex(x), option, nDFT); }
              inline Vector1N Hilbert(ComplexPart part, const Vector1N &x, Option_t *option = "",  int nDFT = 0) { return Get(part, Hilbert(Complex(x), option, nDFT)); }
                     Vector1C Hilbert(                        Vector1C  x, Option_t *option = "",  int nDFT = 0);
              inline Vector1N Hilbert(ComplexPart part, const Vector1C &x, Option_t *option = "",  int nDFT = 0) { return Get(part, Hilbert(x , option, nDFT)); }
              // inline TH1*     Hilbert(                  TH1 *h1,           Option_t *option = "x", int nDFT = 0) { return Hilbert(ComplexPart::Magnitude, h1, option, nDFT); }
              //        TH1*     Hilbert(ComplexPart part, TH1 *h1,           Option_t *option = "x", int nDFT = 0);

              inline Vector1N  Envelope(const Vector1N &x, Option_t *option = "" , int nDFT = 0) { return Hilbert(ComplexPart::Magnitude, x, option, nDFT); }
              inline Vector1N  Envelope(const Vector1C &x, Option_t *option = "" , int nDFT = 0) { return Hilbert(ComplexPart::Magnitude, x, option, nDFT); }
                     // TH1*      Envelope(TH1 *h,            Option_t *option = "x", int nDFT = 0);

              Vector1N Gaussian(NumericT fs, const double duration, double mean = 0, double sigma = 1);

              // NB: https://colab.research.google.com/github/kastnerkyle/kastnerkyle.github.io/blob/master/posts/polyphase-signal-processing/polyphase-signal-processing.ipynb#scrollTo=JbT-73-Z-q83
              Vector1N           Chirp(NumericT fs, const double duration, double t0, double t1, double f0, double f1, Trend method, double taper = NAN, const NumericT phase = 0);
              Vector1N SweepPolynomial(NumericT fs, const double duration, double t0, double t1, const Vector1N coeff,    double taper = NAN,       NumericT phase = 0);                                  
              Vector1N    SweepFormula(NumericT fs, const double duration, TF1 *formula,  const Vector1N params,          double taper = NAN,       NumericT phase = 0);

              inline Vector1N Taper(Vector1N  x, NumericT fs, double T0, double T1, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric) { return Real((Taper(Complex(x), fs, T0, T1, ab, symmetry))); }
                     Vector1C Taper(Vector1C x, NumericT fs, double T0, double T1, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
                     // TH1*     Taper(TH1 *h, double T0, double T1, Option_t *opt = "x", NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              // 1D Autoregressif models
              inline Vector1N AutoRegressive(NumericT fs, const double duration, double c, Vector1N p, Vector1N AR0, double sigma = 1     ) { return AutoRegressive(fs*duration, c, p, AR0, Gaussian(fs*duration, 0, sigma)); }
              inline Vector1N AutoRegressive(NumericT fs,                        double c, Vector1N p, Vector1N AR0, double sigma = 1     ) { return AutoRegressive(fs,          c, p, AR0, Gaussian(fs,       1, 0, sigma)); }
              inline Vector1N AutoRegressive(NumericT fs, const double duration, double c, Vector1N p, Vector1N AR0, Vector1N w) { return AutoRegressive(fs*duration, c, p, AR0, w); }
                     Vector1N AutoRegressive(NumericT fs,                        double c, Vector1N p, Vector1N AR0, Vector1N w);
              
              inline Vector1N MovingAverage(NumericT fs, const double duration, Vector1N q, double sigma = 1) { return MovingAverage(fs*duration, q, Gaussian(fs, duration, 0, sigma)); }
              inline Vector1N MovingAverage(NumericT fs,                        Vector1N q, double sigma = 1) { return MovingAverage(fs,          q, Gaussian(fs,        1, 0, sigma)); }
              inline Vector1N MovingAverage(NumericT fs, const double duration, Vector1N q, Vector1N w) { return MovingAverage(fs*duration, q, w); }
                     Vector1N MovingAverage(NumericT fs,                        Vector1N q, Vector1N w);
              
              inline Vector1N AutoRegressiveMovingAverage(NumericT fs, const double duration, double c, Vector1N p, Vector1N q, Vector1N AR0, double sigma = 1         ) { return AutoRegressiveMovingAverage(fs*duration, c, p, q, AR0, Gaussian(fs, duration, 0, sigma)); }
              inline Vector1N AutoRegressiveMovingAverage(NumericT fs,                        double c, Vector1N p, Vector1N q, Vector1N AR0, double sigma = 1         ) { return AutoRegressiveMovingAverage(fs,          c, p, q, AR0, Gaussian(fs,        1, 0, sigma)); }
              inline Vector1N AutoRegressiveMovingAverage(NumericT fs, const double duration, double c, Vector1N p, Vector1N q, Vector1N AR0, Vector1N w) { return this->Add(AutoRegressive(fs*duration, c, p,    AR0, Vector1N(fs*duration, 0)), MovingAverage(fs*duration, q, w)); }
              inline Vector1N AutoRegressiveMovingAverage(NumericT fs,                        double c, Vector1N p, Vector1N q, Vector1N AR0, Vector1N w) { return this->Add(AutoRegressive(fs,          c, p,    AR0, Vector1N(fs,          0)), MovingAverage(fs,          q, w)); }

              inline Vector1N RandomWalk(NumericT fs, const double duration, double step, double sigma = 1) { return RandomWalk(fs, duration, step, Gaussian(fs, duration, 0, sigma)); }
                     Vector1N RandomWalk(NumericT fs, const double duration, double step, Vector1N w) { return AutoRegressive(fs*duration, step, Vector1N({1}), Vector1N({0}), w); }
              
              // Signal whitening
              Vector1C Whitening(const Vector1C &x, const Vector1N &ASD, NumericT fs, Option_t *option = "",  int nDFT = 0, NumericT timeshift = 0, NumericT phase = 0);
              Vector1C Whitening(const Vector1C &x,                      NumericT fs, Option_t *option = "",  int nDFT = 0, NumericT timeshift = 0, NumericT phase = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
              Vector1N Whitening(const Vector1N &x, const Vector1N &ASD, NumericT fs, Option_t *option = "",  int nDFT = 0, NumericT timeshift = 0, NumericT phase = 0);
              Vector1N Whitening(const Vector1N &x,                      NumericT fs, Option_t *option = "",  int nDFT = 0, NumericT timeshift = 0, NumericT phase = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);
              // TH1*     Whitening(TH1 *h,                      TH1 *hASD,              Option_t *option = "x", int nDFT = 0, NumericT timeshift = 0, NumericT phase = 0);
              // TH1*     Whitening(TH1 *h,                                              Option_t *option = "x", int nDFT = 0, NumericT timeshift = 0, NumericT phase = 0, Estimator estimator = Estimator::Welch, NumericT noverlap = 0.5, Average average = Average::Mean, Trend detrend = Trend::Constant, WindowType window = WindowType::Hann, NumericT ab = NAN, WindowSymmetry symmetry = WindowSymmetry::symmetric);

              Vector1C Downsample(const Vector1C &x, NumericT fs, NumericT fdown, int offset = 0);
              Vector1N Downsample(const Vector1N &x, NumericT fs, NumericT fdown, int offset = 0);
              // TH1 *    Downsample(TH1 *h1, Option_t *option,      NumericT fdown, int offset = 0);
              
              Vector1C Upsample(const Vector1C &x, NumericT fs, NumericT fup,   int offset = 0);
              Vector1N Upsample(const Vector1N &x, NumericT fs, NumericT fup,   int offset = 0);
              // TH1 *    Upsample(TH1 *h1, Option_t *option,      NumericT fup,   int offset = 0);
              
              Vector1C Decimate(const Vector1C &x, NumericT fs, NumericT fdown, int offset = 0, Method method = Method::DFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<NumericT, NumericT> &filterRpRs = {});
              Vector1N Decimate(const Vector1N &x, NumericT fs, NumericT fdown, int offset = 0, Method method = Method::DFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<NumericT, NumericT> &filterRpRs = {});
              // TH1*     Decimate(TH1 *h1, Option_t *option,      NumericT fdown, int offset = 0, Method method = Method::DFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<NumericT, NumericT> &filterRpRs = {});

              Vector1C Interpolate(const Vector1C &x, NumericT fs, NumericT fup, int offset = 0, Method method = Method::DFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<NumericT, NumericT> &filterRpRs = {});
              Vector1N Interpolate(const Vector1N &x, NumericT fs, NumericT fup, int offset = 0, Method method = Method::DFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<NumericT, NumericT> &filterRpRs = {});
              // TH1*     Interpolate(TH1 *h1, Option_t *option,      NumericT fup, int offset = 0, Method method = Method::DFT, int filterOrder = 4, FilterDesign filter = FilterDesign::Butterworth, const std::pair<NumericT, NumericT> &filterRpRs = {});

              //http://www.ws.binghamton.edu/fowler/fowler%20personal%20page/EE521_files/IV-05%20Polyphase%20FIlters%20Revised.pdf
              //https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.resample.html
              inline Vector1C ZoomFFT (const Vector1N &x, NumericT fs, double fc, double bw,          Method method = Method::DFT) { return Resample(x, fs, fc, bw, method); }
                     Vector1C ZoomFFT (const Vector1C &x, NumericT fs, double fc, double bw,          Method method = Method::DFT) { return Resample(x, fs, fc, bw, method); }
              inline Vector1C Resample(const Vector1N &x, NumericT fs, double fc, double bw,          Method method = Method::DFT) { return Resample(Complex(x), fs, fc, bw, method); }
                     Vector1C Resample(const Vector1C &x, NumericT fs, double fc, double bw,          Method method = Method::DFT);
              inline Vector1N Resample(const Vector1N &x, NumericT fs, NumericT newFs,                Method method = Method::DFT) { return Real((Resample(Complex(x), fs, newFs, Domain::Frequency, method))); }
                     Vector1C Resample(const Vector1C &x, NumericT fs, NumericT newFs, Domain domain, Method method = Method::DFT);
                     // TH1*     Resample(TH1 *h, Option_t *option,       NumericT newFs,                SpectrumProperty spectrum, Method method = Method::DFT);

              Vector1C FrequencyCut(const Vector1C &input, NumericT fs, double fLow = NAN, double fHigh = NAN);

              inline Vector1N FrequencyMoment(int n, const Vector1N &h, NumericT fs, const Vector1N &PSD = Vector1N{},                                           Option_t *option = "", int nDFT = 0, double fLow = NAN, double fHigh = NAN) { return FrequencyMoment(n, Complex(h), fs, PSD, Domain::Frequency,        option, nDFT, fLow, fHigh); }
                     Vector1N FrequencyMoment(int n, const Vector1C &h, NumericT fs,       Vector1N  PSD = Vector1N{}, Domain domain = Domain::ComplexFrequency, Option_t *option = "", int nDFT = 0, double fLow = NAN, double fHigh = NAN);

              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs,                  Domain domain, ComplexT dc, NumericT  formulaRe,    NumericT  formulaIm = 0                                                               ) { return Get(part, FrequencySpectrum(fs,          domain, dc, "[0]",     "[1]",       Vector1N({formulaRe, formulaIm}))); }
              inline Vector1C FrequencySpectrum(                  NumericT fs,                  Domain domain, ComplexT dc, NumericT  formulaRe,    NumericT  formulaIm = 0                                                               ) { return           FrequencySpectrum(fs,          domain, dc, "[0]",     "[1]",       Vector1N({formulaRe, formulaIm})); }
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs, double duration, Domain domain, ComplexT dc, NumericT  formulaRe,    NumericT  formulaIm = 0                                                               ) { return Get(part, FrequencySpectrum(fs*duration, domain, dc, formulaRe, formulaIm)); }
              inline Vector1C FrequencySpectrum(                  NumericT fs, double duration, Domain domain, ComplexT dc, NumericT  formulaRe,    NumericT  formulaIm = 0                                                               ) { return           FrequencySpectrum(fs*duration, domain, dc, formulaRe, formulaIm); }
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs,                  Domain domain, ComplexT dc, TString   formulaReStr, TString   formulaImStr, Vector1N params = {},                       int iterations = 1) { return Get(part, FrequencySpectrum(fs,          domain, dc, formulaReStr, formulaImStr, params, Vector1N({}), iterations)); }
              inline Vector1C FrequencySpectrum(                  NumericT fs,                  Domain domain, ComplexT dc, TString   formulaReStr, TString   formulaImStr, Vector1N params = {},                       int iterations = 1) { return           FrequencySpectrum(fs,          domain, dc, formulaReStr, formulaImStr, params, Vector1N({}), iterations); }
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs, double duration, Domain domain, ComplexT dc, TString   formulaReStr, TString   formulaImStr, Vector1N params = {},                       int iterations = 1) { return Get(part, FrequencySpectrum(fs*duration, domain, dc, formulaReStr, formulaImStr, params, iterations)); }
              inline Vector1C FrequencySpectrum(                  NumericT fs, double duration, Domain domain, ComplexT dc, TString   formulaReStr, TString   formulaImStr, Vector1N params = {},                       int iterations = 1) { return           FrequencySpectrum(fs*duration, domain, dc, formulaReStr, formulaImStr, params, iterations); }
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs, double duration, Domain domain, ComplexT dc, TString   formulaReStr, TString   formulaImStr, Vector1N params,      Vector1N paramSigmas, int iterations = 1) { return Get(part, FrequencySpectrum(fs*duration, domain, dc, formulaReStr, formulaImStr, params, paramSigmas, iterations)); }
              inline Vector1C FrequencySpectrum(                  NumericT fs, double duration, Domain domain, ComplexT dc, TString   formulaReStr, TString   formulaImStr, Vector1N params,      Vector1N paramSigmas, int iterations = 1) { return           FrequencySpectrum(fs*duration, domain, dc, formulaReStr, formulaImStr, params, paramSigmas, iterations); }
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs,                  Domain domain, ComplexT dc, TString   formulaReStr, TString   formulaImStr, Vector1N params,      Vector1N paramSigmas, int iterations = 1) { return Get(part, FrequencySpectrum(fs, domain, dc, formulaReStr, formulaImStr, params, paramSigmas, iterations)); }
                     Vector1C FrequencySpectrum(                  NumericT fs,                  Domain domain, ComplexT dc, TString   formulaReStr, TString   formulaImStr, Vector1N params,      Vector1N paramSigmas, int iterations = 1);
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs,                  Domain domain, ComplexT dc, TFormula *formulaRe,    TFormula *formulaIm,    Vector1N params = {},                       int iterations = 1) { return Get(part, FrequencySpectrum(fs,          domain, dc, formulaRe, formulaIm, params, Vector1N({}), iterations)); }
              inline Vector1C FrequencySpectrum(                  NumericT fs,                  Domain domain, ComplexT dc, TFormula *formulaRe,    TFormula *formulaIm,    Vector1N params = {},                       int iterations = 1) { return           FrequencySpectrum(fs,          domain, dc, formulaRe, formulaIm, params, Vector1N({}), iterations); }
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs, double duration, Domain domain, ComplexT dc, TFormula *formulaRe,    TFormula *formulaIm,    Vector1N params = {},                       int iterations = 1) { return Get(part, FrequencySpectrum(fs*duration, domain, dc, formulaRe, formulaIm, params, iterations)); }
              inline Vector1C FrequencySpectrum(                  NumericT fs, double duration, Domain domain, ComplexT dc, TFormula *formulaRe,    TFormula *formulaIm,    Vector1N params = {},                       int iterations = 1) { return           FrequencySpectrum(fs*duration, domain, dc, formulaRe, formulaIm, params, iterations); }
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs, double duration, Domain domain, ComplexT dc, TFormula *formulaRe,    TFormula *formulaIm,    Vector1N params,      Vector1N paramSigmas, int iterations = 1) { return Get(part, FrequencySpectrum(fs*duration, domain, dc, formulaRe, formulaIm, params, paramSigmas, iterations)); }
              inline Vector1C FrequencySpectrum(                  NumericT fs, double duration, Domain domain, ComplexT dc, TFormula *formulaRe,    TFormula *formulaIm,    Vector1N params,      Vector1N paramSigmas, int iterations = 1) { return           FrequencySpectrum(fs*duration, domain, dc, formulaRe, formulaIm, params, paramSigmas, iterations); }
              inline Vector1N FrequencySpectrum(ComplexPart part, NumericT fs,                  Domain domain, ComplexT dc, TFormula *formulaRe,    TFormula *formulaIm,    Vector1N params,      Vector1N paramSigmas, int iterations = 1) { return Get(part, FrequencySpectrum(fs, domain, dc, formulaRe, formulaIm, params, paramSigmas, iterations)); }
                     Vector1C FrequencySpectrum(                  NumericT fs,                  Domain domain, ComplexT dc, TFormula *formulaRe,    TFormula *formulaIm,    Vector1N params,      Vector1N paramSigmas, int iterations = 1);

              //
              // NB: About Signal-to-Noise ratio.. 
              //     If not provided, PSD is computed using the first four seconds of the signal.
              inline std::pair<Vector1C, double> SignalNoiseRatio(const Vector1N &x, const Vector1N &y, NumericT fs, const Vector1N &PSD = Vector1N{},                                           Option_t *option = "",  int nDFT = 0, double fLow = NAN, double fHigh = NAN) { return SignalNoiseRatio(Complex(x), Complex(y), fs, PSD, Domain::Frequency,        option, nDFT, fLow, fHigh); }
              inline std::pair<Vector1C, double> SignalNoiseRatio(const Vector1N &x, const Vector1C &y, NumericT fs, const Vector1N &PSD = Vector1N{},                                           Option_t *option = "",  int nDFT = 0, double fLow = NAN, double fHigh = NAN) { return SignalNoiseRatio(Complex(x),                 y , fs, PSD, Domain::ComplexFrequency, option, nDFT, fLow, fHigh); }
              inline std::pair<Vector1C, double> SignalNoiseRatio(const Vector1C &x, const Vector1N &y, NumericT fs, const Vector1N &PSD = Vector1N{},                                           Option_t *option = "",  int nDFT = 0, double fLow = NAN, double fHigh = NAN) { return SignalNoiseRatio(                x , Complex(y), fs, PSD, Domain::ComplexFrequency, option, nDFT, fLow, fHigh); }
                     std::pair<Vector1C, double> SignalNoiseRatio(const Vector1C &x, const Vector1C &y, NumericT fs,       Vector1N  PSD = Vector1N{}, Domain domain = Domain::ComplexFrequency, Option_t *option = "",  int nDFT = 0, double fLow = NAN, double fHigh = NAN);
                     // std::pair<Vector1C, double> SignalNoiseRatio(                  TH1 *h1,                       TH1 *h2,                  TH1 *hPSD = NULL,                                                                           Option_t *option = "x", int nDFT = 0, double fLow = NAN, double fHigh = NAN);
                     // std::pair<TH1*, double>     SignalNoiseRatio(ComplexPart part, TH1 *h1,                       TH1 *h2,                  TH1 *hPSD = NULL,                                                                           Option_t *option = "x", int nDFT = 0, double fLow = NAN, double fHigh = NAN);

              //
              // Some considerations about signal filtering..
              // N is the filter order
              // wp/ws is the pass/stop width
              // rp/rs is the "passband ripple"/"bandstop attenuation"
              //
              // NB-1: In case you provide [wp,ws] no need to provide the order.. (The smallest order will be computed)
              // NB-2: For notching, you can just provide [df and df2] the smallest will be used as bandstop
              // NB-3: Filters output "SOS" have be used in combination with Filtering() call
              SOS Elliptic   (const Vector1N &wp, const Vector1N &ws, FilterType filterType, const std::pair<NumericT, NumericT> &RpRs, const Vector1N &f, NumericT fs);
              SOS ChebyshevI (const Vector1N &wp, const Vector1N &ws, FilterType filterType, const std::pair<NumericT, NumericT> &RpRs, const Vector1N &f, NumericT fs);
              SOS ChebyshevII(const Vector1N &wp, const Vector1N &ws, FilterType filterType, const std::pair<NumericT, NumericT> &RpRs, const Vector1N &f, NumericT fs);
              SOS Butterworth(const Vector1N &wp, const Vector1N &ws, FilterType filterType, const std::pair<NumericT, NumericT> &RpRs, const Vector1N &f, NumericT fs);

              SOS Elliptic   (int N, FilterType filterType, NumericT rp, NumericT rs, const Vector1N &f, NumericT fs);
              SOS ChebyshevI (int N, FilterType filterType, NumericT rp,              const Vector1N &f, NumericT fs);
              SOS ChebyshevII(int N, FilterType filterType,              NumericT rs, const Vector1N &f, NumericT fs);
              SOS Butterworth(int N, FilterType filterType,                           const Vector1N &f, NumericT fs);
              SOS Bessel     (int N, FilterType filterType,                           const Vector1N &f, NumericT fs);

              SOS Biquad(int N,                                  FilterDesign design, const std::pair<NumericT,NumericT> &filterRpRs, FilterType filter, const Vector1N &f, NumericT fs);
              SOS Biquad(const Vector1N &wp, const Vector1N &ws, FilterDesign design, const std::pair<NumericT,NumericT> &filterRpRs, FilterType filter, const Vector1N &f, NumericT fs);
              SOS Biquad(FilterType filter, NumericT f, NumericT fs, NumericT Q = 0.5, NumericT gain = 1);

              inline SOS Notch(NumericT f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, NumericT rp, NumericT rs, int N) { return Notch(f, df, fs, filterDesign, std::make_pair(rp, rs), N); }
              inline SOS Notch(NumericT f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, NumericT rp, NumericT rs) { return Notch(f, df, fs, filterDesign, std::make_pair(rp, rs)); }
              inline SOS Notch(NumericT f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, const std::pair<NumericT,NumericT> &filterRpRs, int N) { return Notch(f, df, fs, filterDesign, filterRpRs, N); }
              inline SOS Notch(NumericT f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, const std::pair<NumericT,NumericT> &filterRpRs) { return Notch(Vector1N({f}), df, fs, filterDesign, filterRpRs); };
              inline SOS Notch(NumericT f, const std::pair<NumericT,NumericT> &df, NumericT fs) { return Notch(Vector1N({f}), df, fs); };

              inline SOS Notch(const Vector1N &f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, NumericT rp, NumericT rs, int N) { return Notch(f, df, fs, filterDesign, std::make_pair(rp, rs), N); }
              inline SOS Notch(const Vector1N &f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, const std::pair<NumericT,NumericT> &filterRpRs, int N);
              inline SOS Notch(const Vector1N &f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, NumericT rp, NumericT rs) { return Notch(f, df, fs, filterDesign, std::make_pair(rp, rs)); }
              inline SOS Notch(const Vector1N &f, const std::pair<NumericT,NumericT> &df, NumericT fs) { return Notch(f, df, fs, FilterDesign::Butterworth, NAN, NAN); };
                     SOS Notch(const Vector1N &f, const std::pair<NumericT,NumericT> &df, NumericT fs, FilterDesign filterDesign, const std::pair<NumericT,NumericT> &filterRpRs);

              inline  TF Analog(const  TF &digital, NumericT Ts, FilterTransform method = FilterTransform::Tustin, NumericT alpha = NAN) { return TransferFunction(Analog(StateSpace(digital), Ts, method, alpha)); }
              inline ZPK Analog(const ZPK &digital, NumericT Ts, FilterTransform method = FilterTransform::Tustin, NumericT alpha = NAN) { return ZeroPoleGain(Analog(StateSpace(digital), Ts, method, alpha)); }
              inline SOS Analog(const SOS &digital, NumericT Ts, FilterTransform method = FilterTransform::Tustin, NumericT alpha = NAN) { return SecondOrderSections(Analog(StateSpace(digital), Ts, method, alpha)); }
                     SS Analog (const  SS &digital, NumericT Ts, FilterTransform method = FilterTransform::Tustin, NumericT alpha = NAN);

              inline  TF Digital(const  TF &analog, NumericT Ts, FilterTransform method = FilterTransform::Tustin, NumericT alpha = NAN) { return TransferFunction(Digital(StateSpace(analog), Ts, method, alpha)); }
              inline ZPK Digital(const ZPK &analog, NumericT Ts, FilterTransform method = FilterTransform::Tustin, NumericT alpha = NAN) { return ZeroPoleGain(Digital(StateSpace(analog), Ts, method, alpha)); }
              inline SOS Digital(const SOS &analog, NumericT Ts, FilterTransform method = FilterTransform::Tustin, NumericT alpha = NAN) { return SecondOrderSections(Digital(StateSpace(analog), Ts, method, alpha)); }
                     SS Digital (const  SS &analog, NumericT Ts, FilterTransform method = FilterTransform::Tustin, NumericT alpha = NAN);

              Vector1N Eval(const Vector1N &x, const TF &H);
              Vector1C Eval(const Vector1C &x, const TF &H);
              NumericT Eval(const NumericT &x, const TF &H);
              ComplexT Eval(const ComplexT &x, const TF &H);

              ZPK ZeroPoleGain(const TBiquad &params) { return ZeroPoleGain(TransferFunction(params)); }
              SOS SecondOrderSections(const TBiquad &params) { return SecondOrderSections(TransferFunction(params)); }
              SS  StateSpace(const TBiquad &params) { return StateSpace(TransferFunction(params)); }
              TF  TransferFunction(const TBiquad &params) { return TransferFunction(params); }

              std::vector<TF> TransferFunction(const std::vector<ZPK> &zpk);
              std::vector<TF> TransferFunction(const std::vector<SOS> &sos);
              std::vector<TF> TransferFunction(const std::vector<SS>   &ss);
              std::vector<TF> TransferFunction(const std::vector<TF>   &tf);
              
              TF TransferFunction(const SOS &sos);
              TF TransferFunction(const TF  &tf );
              TF TransferFunction(const ZPK &zpk);
              TF TransferFunction(const SS  &ss );
              
              SS StateSpace(const SOS &sos);
              SS StateSpace(const TF  &tf );
              SS StateSpace(const ZPK &zpk);
              SS StateSpace(const SS  &ss );
              
              ZPK ZeroPoleGain(const TF  &tf );
              ZPK ZeroPoleGain(const SOS &sos);
              ZPK ZeroPoleGain(const ZPK &zpk);
              ZPK ZeroPoleGain(const SS  &ss );
              
              std::vector<ZPK> ZeroPoleGain(const std::vector<TF>  &tf);
              std::vector<ZPK> ZeroPoleGain(const std::vector<SOS> &sos);
              std::vector<ZPK> ZeroPoleGain(const std::vector<SS>  &ss);
              std::vector<ZPK> ZeroPoleGain(const std::vector<ZPK> &zpk);
              
              SOS SecondOrderSections(const TF  &tf);
              SOS SecondOrderSections(const SOS &sos);
              SOS SecondOrderSections(const SS  &ss);
              SOS SecondOrderSections(const ZPK &zpk);
              
              SOS SecondOrderSections(const std::vector<SS>  &ss );
              SOS SecondOrderSections(const std::vector<ZPK> &zpk);
              SOS SecondOrderSections(const std::vector<TF>  &tf );
              SOS SecondOrderSections(const std::vector<SOS> &sos);
              
              TF  TransferFunction(const Vector1N &B, const Vector1N &A);
              ZPK ZeroPoleGain(const Vector1N &B, const Vector1N &A);
              SOS SecondOrderSections(const Vector1N &B, const Vector1N &A);
              SS  StateSpace(const Vector1N &B, const Vector1N &A);
              
              TF  TransferFunction(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A);
              ZPK ZeroPoleGain(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A);
              SS  StateSpace(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A);
              SOS SecondOrderSections(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A);
              
              TF  TransferFunction(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D);
              ZPK ZeroPoleGain(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D);
              SS  StateSpace(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D);
              SOS SecondOrderSections(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D);
              
              ClassDef(TKFRLegacy, 1);
       };

       using Complex = TKFRLegacy<double>::ComplexT;
       using Real    = TKFRLegacy<double>::NumericT;
       using Imag    = TKFRLegacy<double>::NumericT;
    }
}

using TKFRLegacy = ROOT::Signal::TKFRLegacy<double>;
R__EXTERN TKFRLegacy *gKFRLegacy;
