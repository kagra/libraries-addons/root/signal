#pragma once

#include <kfr/all.hpp>

namespace kfr
{
    const char* library_version_dft();

    inline namespace CMT_ARCH_NAME
    {
        //****************************************************************************80

        // ////////////
        // Elliptic filtering
        // ////////

        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        KFR_INTRINSIC T1 cmpl(T1 kx) { return sqrt((1. - kx) * (1. + kx)); }

        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        KFR_INTRINSIC complex<T1> cmpl(complex<T1> kx) { return sqrt((complex<T1>(1.) - kx) * (complex<T1>(1.) + kx)); }


        //
        // Elliptical filtering
        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        KFR_INTRINSIC T1 cephes_polevl(T1 x, T1 *coef, int N)
        {
            T1	ans;
            int		i;
            T1	*p;

            p = coef;
            ans = *p++;
            i = N;

            do ans = ans * x  +  *p++;
            while ( --i );

            return ans;
        }


        //****************************************************************************80

        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        void sncndn ( T1 u, T1 m, T1 &sn, T1 &cn, T1 &dn )

        //****************************************************************************80
        //
        //  Purpose:
        //
        //    SNCNDN evaluates Jacobi elliptic functions.
        //
        //  Licensing:
        //
        //    This code is distributed under the GNU LGPL license.
        //
        //  Modified:
        //
        //    26 June 2018
        //
        //  Author:
        //
        //    Original ALGOL version by Roland Bulirsch.
        //    C++ version by John Burkardt
        //
        //  Reference:
        //
        //    Roland Bulirsch,
        //    Numerical calculation of elliptic integrals and elliptic functions,
        //    Numerische Mathematik,
        //    Volume 7, Number 1, 1965, pages 78-90.
        //
        //  Parameters:
        //
        //    Input, T U, M, the arguments.
        //
        //    Output, T &SN, &CN, &DN, the value of the Jacobi
        //    elliptic functions sn(u,m), cn(u,m), and dn(u,m).
        //
        {
            T1 a;
            T1 b;
            T1 c;
            T1 ca;
            T1 d;
            int i;
            int l;
            T1 *m_array;
            T1 m_comp;
            T1 *n_array;
            const T1 r8_epsilon = 2.220446049250313E-16;
            T1 u_copy;

            m_comp = 1.0 - m;
            u_copy = u;

            if ( m_comp == 0.0 )
            {
                cn = 1.0 / cosh ( u_copy );
                dn = cn;
                sn = tanh ( u_copy );
                return;
            }

            if ( 1.0 < m )
            {
                d = 1.0 - m_comp;
                m_comp = - m_comp / d;
                d = sqrt ( d );
                u_copy = d * u_copy;
            }

            ca = sqrt ( r8_epsilon );

            a = 1.0;
            dn = 1.0;
            l = 24;

            m_array = new T1[25];
            n_array = new T1[25];

            for ( i = 0; i < 25; i++ )
            {
                m_array[i] = a;
                m_comp = sqrt ( m_comp );
                n_array[i] = m_comp;
                c = 0.5 * ( a + m_comp );
                if ( fabs ( a - m_comp ) <= ca * a )
                {
                l = i;
                break;
                }
                m_comp = a * m_comp;
                a = c;
            }

            u_copy = c * u_copy;
            sn = sin ( u_copy );
            cn = cos ( u_copy );

            if ( sn != 0.0 )
            {
                a = cn / sn;
                c = a * c;

                for ( i = l; 0 <= i; i-- )
                {
                b = m_array[i];
                a = c * a;
                c = dn * c;
                dn = ( n_array[i] + a ) / ( b + a );
                a = c / b;
                }

                a = 1.0 / sqrt ( c * c + 1.0 );

                if ( sn < 0.0 )
                {
                sn = - a;
                }
                else
                {
                sn = a;
                }
                cn = c * sn;
            }

            if ( 1.0 < m )
            {
                a = dn;
                dn = cn;
                cn = a;
                sn = sn / d;
            }

            delete [] m_array;
            delete [] n_array;

            return;
        }

        //****************************************************************************80

        /**  Inverse Jacobian elliptic sn

            See [1], Eq. (56)

            References
            ----------
            .. [1] Orfanidis, "Lecture Notes on Elliptic Filter Design",
                https://www.ece.rutgers.edu/~orfanidi/ece521/notes.pdf

        */

        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        KFR_INTRINSIC complex<T1> inv_jacobi_sn(complex<T1> w, T1 m) 
        {
            /* Maximum number of iterations in Landen transformation recursion
            sequence.  10 is conservative; unit tests pass with 4, Orfanidis
            (see jacobi_cn [1]) suggests 5. */
            const int INV_JACOBI_SN_MAXITER = 10;

            T1 k = sqrt(m);
            if(k > 1) return NAN;
            else if( k == 1) return atanh(w);

            int niter = 0;
            univector<T1> ks(1, k);
            while (ks[ks.size() - 1] != 0)
            {
                T1 k_  = ks[ks.size() - 1];
                T1 k_p = cmpl(k_);
                ks.push_back((1 - k_p) / (1 + k_p));
        
                niter += 1;
                if (niter > INV_JACOBI_SN_MAXITER)
                throw std::invalid_argument("Landen transformation not converging");
            }

            T1 K = product(1 + ks.slice(1)) * c_pi<T1> / 2.;

            univector<complex<T1>> wns(1, w);
            univector<T1> kn    = ks.slice(0, ks.size()-1);
            univector<T1> knext = ks.slice(1, ks.size());

            for (int i = 0, ii = kn.size(); i < ii; i++)
            {
                complex<T1> wn    = wns[wns.size()-1];
                complex<T1> wnext = (complex<T1>(2.) * wn / complex<T1>(1. + knext[i]) / complex<T1>(1.) + cmpl(kn[i] * wn) /* @WARN double precision limitation in cmpl operation for i = 1 */);
                wns.push_back(wnext); // @TODO : Precision issue to fix.. https://www.sciencedirect.com/science/article/pii/S0010465515001733
            }

            complex<T1> u = 2 / c_pi<T1> * asin(wns[wns.size()-1]);
            
            return K * u;
        }

        /*** Real inverse Jacobian sc, with complementary modulus

            References
            ----------
            # noqa: E501
            .. [1] https://functions.wolfram.com/EllipticFunctions/JacobiSC/introductions/JacobiPQs/ShowAll.html,
            "Representations through other Jacobi functions"

            ***/

        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        KFR_INTRINSIC T1 inv_jacobi_sc1(T1 w, T1 m)
        {
            complex<T1> zcomplex = inv_jacobi_sn(complex<T1> (0, w), m);

            const T1 _epsilon = 1e-14;
            if (abs(zcomplex.real()) > _epsilon)
            throw std::invalid_argument("pure imaginary number expected");

            return zcomplex.imag();
        }

        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        KFR_INTRINSIC T1 elliptic_k (T1 m)
        {
            //
            // P,Q approximation uses Cephes library 
            // (https://github.com/deepmind/torch-cephes/blob/master/cephes/ellf/ellpk.c)
            double MACHEP =  1.11022302462515654042E-16;   /* 2**-53 */
            // double MACHEP =  1.38777878078144567553E-17;   /* 2**-56 */

            static double P[] =
            {
                1.37982864606273237150E-4,
                2.28025724005875567385E-3,
                7.97404013220415179367E-3,
                9.85821379021226008714E-3,
                6.87489687449949877925E-3,
                6.18901033637687613229E-3,
                8.79078273952743772254E-3,
                1.49380448916805252718E-2,
                3.08851465246711995998E-2,
                9.65735902811690126535E-2,
                1.38629436111989062502E0
            };

            static double Q[] =
            {
                2.94078955048598507511E-5,
                9.14184723865917226571E-4,
                5.94058303753167793257E-3,
                1.54850516649762399335E-2,
                2.39089602715924892727E-2,
                3.01204715227604046988E-2,
                3.73774314173823228969E-2,
                4.88280347570998239232E-2,
                7.03124996963957469739E-2,
                1.24999999999870820058E-1,
                4.99999999999999999821E-1
            };

            static double C1 = 1.3862943611198906188E0; /* log(4) */

            double p = 1.-m;
            if( p < 0 || p > 1.0) return 0;
            if( p > MACHEP ) return cephes_polevl(p, P, 10) - log(p)*cephes_polevl(p, Q, 10);
            if( p == 0 ) return NAN;
            
            return C1 - 0.5 * log(p);
        }

        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        KFR_INTRINSIC T1 elliptic_km1 (T1 p) { return elliptic_k<T1>(1.-p); }

        template <typename T1, KFR_ENABLE_IF(is_numeric<T1>)>
        KFR_INTRINSIC T1 elliptic_deg ( int N, T1 m1)
        {
            T1 ellipk   = elliptic_k<T1>(m1);
            T1 ellipkm1 = elliptic_km1<T1>(m1);
            
            T1 q1 = exp(-c_pi<T1> / ellipk);
            T1 q  = pow(q1, ellipkm1/N);

            int const _ELLIPDEG_MMAX = 7;
            static T1 mnum[_ELLIPDEG_MMAX+1] = { 0, 1, 2, 3, 4, 5, 6, 7 };
            static T1 mden[_ELLIPDEG_MMAX+1] = { 1, 2, 3, 4, 5, 6, 7, 8 };

            T1 num = 0;
            T1 den = 0;
            for (int i = 0; i < _ELLIPDEG_MMAX; i++ )
            {
                num += pow(q, (mnum[i] * (mnum[i]+1)));
                den += pow(q, (mden[i]*mden[i]));
            }

            return 16 * q * pow(num / (1 + 2*den), 4);
        }
        
        template <typename T>
        KFR_FUNCTION zpk<T> elliptic(int N, identity<T> rp, identity<T> rs)
        {
            if (N == 0)
            {
                return { {}, {}, exp10(-rp/20) };
            }

            if (N == 1)
            {
                univector<complex<T>> p = {-csqrt(complex<T>(1 / (exp10(0.1*rp) - 1), 0) )};
                T k = -p[0].real();
                return { {}, p, k };
            }

            T eps_sq = exp10(0.1*rp) - 1;
            T eps = sqrt(eps_sq);

            T ck1_sq = eps_sq / (exp10(0.1*rs) - 1);
            if(ck1_sq == 0)
            throw std::invalid_argument("Cannot design a filter with given rp and rs specifications.");

            T ellipk = elliptic_k(ck1_sq);

            T m      = elliptic_deg(N, ck1_sq);
            T capk   = elliptic_k(m);

            univector<complex<T>> z1, z2;
            univector<T> j;
            for(int i = 1 - N % 2; i <= N; i = i+2)
                j.push_back(i);

            const T _EPSILON = 2e-16;
            int jj = j.size();

            univector<T> s(jj);
            univector<T> c(jj);
            univector<T> d(jj);

            for(int n = 0; n < jj; n++)
            {
                sncndn(j[n] * capk / N, m, s[n], c[n], d[n]);

                if(abs(s[n]) > _EPSILON) 
                {
                    z1.push_back( complex<T>(0,  1.0 / (sqrt(m) * s[n])) );
                    z2.push_back( complex<T>(0,  1.0 / (sqrt(m) * s[n])) );
                }
            }

            zpk<T> result;
            result.z = concatenate(z1, cconj(z2));

            T sv, cv, dv;
            T r = inv_jacobi_sc1((T) 1. / eps, (T) ck1_sq);
            T v0 = capk * r / (N * ellipk);
            sncndn(v0, 1 - m, sv, cv, dv);

            univector<T> mag2;
            univector<complex<T>> p1, p2;
            for(int n = 0, jj = j.size(); n < jj; n++)
            {
                p1.push_back(- complex<T>(c[n] * d[n] * sv * cv, s[n] * dv) / complex<T>(1 - pow((d[n] * sv), 2.0), 0));
                mag2.push_back( p1[n].real()*p1[n].real() + p1[n].imag()*p1[n].imag() );
            }

            for(int n = 0, jj = j.size(); n < jj; n++)
            {
                if(N % 2 == 0 )
                    p2.push_back( - complex<T>(c[n] * d[n] * sv * cv, s[n] * dv) / complex<T>(1 - pow((d[n] * sv), 2.0), 0) );
                else if(abs(p1[n].imag()) > _EPSILON * sqrt(sum(mag2)))
                    p2.push_back( - complex<T>(c[n] * d[n] * sv * cv, s[n] * dv) / complex<T>(1 - pow((d[n] * sv), 2.0), 0) );
            }

            result.p = concatenate(p1, cconj(p2));
            result.k = (product(-result.p) / product(-result.z)).real();

            if (N % 2 == 0)
                result.k = result.k / sqrt((1 + eps_sq));
            
            return result;
        }

    }
}
