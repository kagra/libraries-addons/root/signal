#pragma once

#include <Rioplus.h>
#include <TPrint.h>
#include <TVarexp.h>
#include <TComplex.h>
#include <TVectorT.h>
#include <TMatrixT.h>

#include "../TKFRLegacy.h"
#include <kfr/all.hpp>
#include "kfr/elliptic.hpp"
#include "kfr/iir_order.hpp"

#include <Math/Interpolator.h>
#include <Math/Polynomial.h>
#include <gsl/gsl_linalg.h>

namespace ROOT {
    
    namespace Signal {

        template <typename T>
        class TKFRLegacy<T>::Impl {

            friend class TKFRLegacy;

            protected: 
                Impl() { }

                kfr::zpk<T> kfrzpk(const ZPK &zpk);
                kfr::biquad_params<T> kfrbiquad(const TBiquad &params);
                std::vector<kfr::biquad_params<T>> kfrsos(const SOS &sos);

                ZPK stdzpk(const kfr::zpk<T> &zpk);
                TBiquad stdbiquad(const kfr::biquad_params<T> &params);
                SOS stdsos(const std::vector<kfr::biquad_params<T>>  &sos);

                Vector1N stdvector(const TVectorT<NumericT> &v);
                TVectorT<NumericT> rootvector(const Vector1N &v);

                kfr::univector<T> kfrvector(const Vector1N &u);
                kfr::univector<std::complex<T>> kfrvector(const Vector1C &u);
                kfr::window_symmetry kfrsymmetry(const WindowSymmetry &w);
                Vector1N stdvector(const kfr::univector<T> &u);
                Vector1C stdvector(const kfr::univector<std::complex<T>> &u);

                template <typename Tx, class UnaryOperator>
                auto Transform(const Tx& input, UnaryOperator op)
                {
                        std::vector<std::decay_t<decltype(op(*input.begin()))>> output(input.size());

                        std::transform(input.begin(), input.end(), std::back_inserter(output), op);
                        return output;
                }

            public:

                ~Impl();

                const char *Version();

                constexpr static int FILTER_ORDER_DEFAULT = 4;
                constexpr static double WINDOW_DEFAULT_TUKEY_ALPHA = 0.5;
                constexpr static double WINDOW_DEFAULT_PLANCK_TAPER_EPSILON = 0.1;
                
                static Vector1N SymmetricResize(Vector1N v, int n);
                static Vector1C SymmetricResize(Vector1C v, int n);

                /**
                 * Complex manipulation
                 */

                NumericT Real(const ComplexT &z);
                Vector1N Real(const Vector1C &z);
                Vector2N Real(const Vector2C &z);

                NumericT Imag(const ComplexT &z);
                Vector1N Imag(const Vector1C &z);
                Vector2N Imag(const Vector2C &z);
                ComplexT Complex(const NumericT &a, const NumericT &b = 0);
                Vector1C Complex(const Vector1N &a, const Vector1N &b = Vector1N());
                Vector2C Complex(const Vector2N &a, const Vector2N &b = Vector2N());

                Vector1C Minus(const Vector1C &a);
                Vector1N Minus(const Vector1N &a);

                Vector1C Negate(const Vector1C &x);
                Vector1N Negate(const Vector1N &v);

                Vector1C Power(const Vector1C &X, const NumericT &y, bool handmade = true);
                Vector1N Power(const Vector1N &X, const NumericT &y, bool handmade = true);
                ComplexT Power(const ComplexT &x, const NumericT &y, bool handmade = true);
                NumericT Power(const NumericT &x, const NumericT &y, bool handmade = true);
                
                Vector1C Sqrt(const Vector1C &X);
                Vector1N Sqrt(const Vector1N &X);
                NumericT Sqrt(const NumericT &x);
                ComplexT Sqrt(const ComplexT &X);

                inline TH1* Sqrt  (TH1 *h) { return PowerX(h, 1/2.); }
                inline TH1* Power2(TH1 *h) { return PowerX(h, 2); }
                       TH1* PowerX(TH1 *h, double X);
                
                ComplexT Conj(const ComplexT &z);
                Vector1C Conj(const Vector1C &z);
                
                NumericT Mag2(const NumericT &z);
                NumericT Mag2(const ComplexT &z);
                Vector1N Mag2(const Vector1N &x);
                Vector1N Mag2(const Vector1C &z);
                
                NumericT Mag(const NumericT &z);
                NumericT Mag(const ComplexT &z);
                Vector1N Mag(const Vector1N &x);
                Vector1N Mag(const Vector1C &z);

                NumericT Abs(const NumericT &z);
                NumericT Abs(const ComplexT &z);
                Vector1N Abs(const Vector1N &x);
                Vector1N Abs(const Vector1C &z);
                
                TH1* Abs   (TH1 *h);

                NumericT dB(const NumericT &z, const NumericT &mag0 = 1, bool powerQuantity = true);
                NumericT dB(const ComplexT &z, const NumericT &mag0 = 1, bool powerQuantity = true);
                Vector1N dB(const Vector1N &z, const NumericT &mag0 = 1, bool powerQuantity = true);
                Vector1N dB(const Vector1C &z, const NumericT &mag0 = 1, bool powerQuantity = true);

                NumericT dB2Mag(const NumericT &dB, const NumericT &mag0 = 1, bool powerQuantity = true);
                Vector1N dB2Mag(const Vector1N &dB, const NumericT &mag0 = 1, bool powerQuantity = true);

                NumericT Mag2dB(const NumericT &mag, const NumericT &mag0 = 1, bool powerQuantity = true);
                Vector1N Mag2dB(const Vector1N &mag, const NumericT &mag0 = 1, bool powerQuantity = true);

                NumericT Argument(const ComplexT &z, const NumericT &offset = 0, bool extend = true);
                Vector1N Argument(const Vector1C &z, const NumericT &offset = 0, bool extend = true);
                Vector1N Unwrap(Vector1N v /*, NumericT discont = NAN*/, NumericT period = NAN);

                NumericT Phase(const ComplexT &z, const NumericT &offset = 0, bool extend = true);
                Vector1N Phase(const Vector1C &z, const NumericT &offset = 0, bool extend = true, bool unwrap = true);
                Vector1N Phase(const Vector1N &v, const NumericT &offset = 0, bool unwrap = true);
                
                inline NumericT SumX2(const Vector1N &a) { return SumXY(a,a); }
                       NumericT SumXY(const Vector1N &a, const Vector1N &b);

                inline ComplexT SumX2(const Vector1C &a) { return SumXY(a,a); }
                inline ComplexT SumXY(const Vector1N &a, const Vector1C &b) { return SumXY(Complex(a),b); }
                inline ComplexT SumXY(const Vector1C &a, const Vector1N &b) { return SumXY(a,Complex(b)); }
                       ComplexT SumXY(const Vector1C &a, const Vector1C &b);


                /**
                 * Complex operations
                 */
                inline Vector1N Add(const Vector1N &A,  const NumericT &b)  { return Add(A,  Vector1N(A.size(), b)); }
                inline Vector1C Add(const Vector1C &z1, const NumericT &z2) { return Add(z1, Vector1N(z1.size(), z2)); }
                inline Vector1C Add(const NumericT &z2, const Vector1C &z1) { return Add(z1, z2); }
                inline Vector1C Add(const Vector1C &z1, const ComplexT &z2) { return Add(z1, Vector1C(z1.size(), z2)); }
                inline Vector1C Add(const ComplexT &z2, const Vector1C &z1) { return Add(z1, z2); }
                inline Vector1C Add(const Vector1C &z1, const Vector1N &z2) { return Add(z1, Complex(z2)); }
                inline Vector1C Add(const Vector1N &z1, const Vector1C &z2) { return Add(Complex(z1), z2); }
                       Vector1N Add(const Vector1N &a, const Vector1N &b);
                       Vector1C Add(const Vector1C &z1, const Vector1C &z2);

                inline Vector1C Multiply(const Vector1C &z, const Vector1N &x) { return Multiply(z, Complex(x)); }
                inline Vector1C Multiply(const Vector1N &x, const Vector1C &z) { return Multiply(z, Complex(x)); }
                inline Vector1C Multiply(const Vector1C &z, const NumericT &k) { return Multiply(z, Vector1C(z.size(), ComplexT(k,0))); }
                inline Vector1C Multiply(const NumericT &k, const Vector1C &z) { return Multiply(z, Vector1C(z.size(), ComplexT(k,0))); }
                inline Vector1N Multiply(const Vector1N &a, const NumericT &k) { return Multiply(a, Vector1N(a.size(), k)); }
                inline Vector1N Multiply(const NumericT &k, const Vector1N &a) { return Multiply(a, k); }
                       Vector1C Multiply(const Vector1C &z1, const Vector1C &z2);
                       Vector1N Multiply(const Vector1N &a, const Vector1N &b);
                

                inline Vector1N Subtract(const Vector1N &a, const NumericT &b) { return Subtract(a, Vector1N(a.size(), b)); }
                inline Vector1C Subtract(const Vector1C &a, const ComplexT &b) { return Subtract(a, Vector1C(a.size(),b)); }
                inline Vector1C Subtract(const Vector1C &a, const NumericT &b) { return Subtract(a, ComplexT(b,0)); }
                inline Vector1C Subtract(const NumericT &b, const Vector1C &a) { return Subtract(ComplexT(b,0), a); }
                inline Vector1C Subtract(const ComplexT &b, const Vector1C &a) { return Subtract(Vector1C(a.size(),b), a); }
                inline Vector1C Subtract(const Vector1N &a, const ComplexT &b) { return Subtract(Complex(a), Vector1C(a.size(),b)); }
                inline Vector1C Subtract(const Vector1C &a, const Vector1N &b) { return Subtract(a, Complex(b)); }
                inline Vector1C Subtract(const Vector1N &a, const Vector1C &b) { return Subtract(Complex(a), b); }
                       Vector1N Subtract(const Vector1N &a, const Vector1N &b);
                       Vector1C Subtract(const Vector1C &z1, const Vector1C &z2);

                inline NumericT SumX(const Vector1N &v) { return Sum(v); }
                       NumericT Sum(const Vector1N &v);
                
                inline ComplexT SumX(const Vector1C &v) { return Sum(v); }
                       ComplexT Sum(const Vector1C &v);

                inline Vector1C Divide(const Vector1C &z,  const Vector1N &x, bool discardNaN = false) { return Divide(z, Complex(x), discardNaN); }
                inline Vector1C Divide(const Vector1C &z,  const NumericT &k, bool discardNaN = false) { return Divide(z, Vector1C(z.size(), ComplexT(k,0)), discardNaN); }
                       Vector1C Divide(const Vector1N &z1, const Vector1C &z2, bool discardNaN = false) { return Divide(Complex(z1), z2, discardNaN); };
                       Vector1C Divide(const Vector1C &z1, const Vector1C &z2, bool discardNaN = false);
                inline Vector1N Divide(const Vector1N &a,  const NumericT &k, bool discardNaN = false) { return Divide(a, Vector1N(a.size(), k), discardNaN); }
                       Vector1N Divide(const Vector1N &x,  const Vector1N &y, bool discardNaN = false);
                
                NumericT Mean(const Vector1N &V);
                ComplexT Mean(const Vector1C &V);
                Vector1N Mean(const Vector2N &V);
                Vector1C Mean(const Vector2C &V);

                inline Vector1C Scale(const NumericT &k, const Vector1C &z) { return this->Scale(z, k); }
                       Vector1C Scale(const Vector1C &z, const NumericT &k);
                inline Vector1N Scale(const NumericT &k, const Vector1N &a) { return this->Scale(a, k); }
                       Vector1N Scale(const Vector1N &a, const NumericT &k);

                inline TH1* Scale (TH1 *h, double scaleY, double scaleX = 1, Option_t *option = "x") { return ScaleY(ScaleX(h, scaleX, option), scaleY); }
                       TH1* ScaleX(TH1 *h, double scaleX, Option_t *option = "x");
                       TH1* ScaleY(TH1 *h, double scaleY);

                inline NumericT Radian(const NumericT& z) { return 0; }
                inline Vector1N Radian(const Vector1N &v) { return Vector1N(v.size(), 0); }
                inline NumericT Radian(const ComplexT& z) { return Argument(z); }
                inline Vector1N Radian(const Vector1C &z) { return Argument(z); }

                inline NumericT Degree(const NumericT& z) { return 0; }
                inline Vector1N Degree(const Vector1N &v) { return Vector1N(v.size(), 0); }
                inline NumericT Degree(const ComplexT& z) { return Radian(z) * 180/TMath::Pi() + 180; }
                inline Vector1N Degree(const Vector1C &Z) { return Transform(Z, [this](const ComplexT &z) { return Degree(z); }); }

                NumericT StdDev( const Vector1N &V);
                ComplexT StdDev( const Vector1C &V);
                Vector1C StdDev( const Vector2C &V);

                NumericT Max( const Vector1N &V);
                NumericT Min( const Vector1N &V);

                NumericT Median( const Vector1N &V);
                ComplexT Median( const Vector1C &V);
                Vector1N Median( const Vector2N &V, bool transpose = true);
                Vector1C Median( const Vector2C &V, bool transpose = true);

                NumericT Mode( const Vector1N &V);
                ComplexT Mode( const Vector1C &V);

                NumericT Q1( const Vector1N &v);
                NumericT Q2( const Vector1N &v);
                NumericT Q3( const Vector1N &v);
                NumericT IQR( const Vector1N &v);

                /**
                 * DFT operations
                 */

                bool  IsPowerOf(int n, int base);
                int NextPowerOf(int n, int base);
                
                bool ValidSizeDFT(const Vector1C &input);
                bool IsOneSidedDFT(const Vector1N &input);
                bool IsOneSidedDFT(const Vector1C &input);

                Vector1C RealDFT(const Vector1N &input);
                inline Vector1C DFT(const Vector1N &input) { return RealDFT(input); }
                       Vector1C DFT(const Vector1C &input);

                Vector1C iDFT(const Vector1C &input);
                Vector1N iRealDFT(const Vector1C &input);
                
                /**
                 * Vector manipulation
                 */
                Vector1N Rotate(const Vector1N &v, int n);
                Vector1C Rotate(const Vector1C &v, int n);
                Vector1N Shift(const Vector1N &v, int n);
                Vector1C Shift(const Vector1C &v, int n);
                Vector1N Repeat(const Vector1N &v, double length);
                Vector1C Repeat(const Vector1C &v, double length);

                inline Vector1N Mirroring(const Vector1N &input, unsigned int offset = 0, unsigned int foldingOffset = 0) { return Real(Mirroring(Complex(input), offset, foldingOffset)); }
                       Vector1C Mirroring(const Vector1C &input, unsigned int offset = 0, unsigned int foldingOffset = 0, bool bConjugate = true);
                inline Vector1N MirroringDFT(const Vector1N &input) { return Real(MirroringDFT(Complex(input))); }
                       Vector1C MirroringDFT(const Vector1C &input, bool bConjugate = true);

                Vector1N MirroringPSD(const Vector1N &input);

                Vector1N GetVectorAxis(TH1* h, int iAxis = 0);
                Vector1N GetVectorAxis(TH1* h, Option_t *axis);
                TH1* Empty(TH1 *);

                template<typename Tx>
                std::vector<Tx> GetVectorContent(TH1* h);
                std::vector<double> GetVectorContent(TH1D* h);
                std::vector<float> GetVectorContent(TH1F* h);

                template<typename Tx>
                std::vector<Tx> GetVectorError(TH1* h);
                std::vector<double> GetVectorError(TH1D* h);
                std::vector<float> GetVectorError(TH1F* h);
                
                TH1* SetVectorError(TH1* h, Vector1N v, int first = 0);
                TH1* SetVectorContent(TH1* h, Vector1N v, int first = 0);

                TH1* AddVectorContent(TH1* h, Vector1N v, int c1 = 1);
                TH1* AddVectorContent(TH1* h, Vector2N v, int c1 = 1);


                Vector1N Slice(const Vector1N& u, int start, int length = -1);
                Vector1C Slice(const Vector1C& u, int start, int length = -1);
                Vector1C SliceXn(const Vector1C& Xn, int a, int b = 0); // Y = X[a*k+b], b = [0..a-1]
                Vector1N SliceXn(const Vector1N& Xn, int a, int b = 0); // Y = X[a*k+b], b = [0..a-1]
                Vector1C InsetXn(const Vector1C& Xn, int a, int b = 0, const ComplexT & v = ComplexT (0,0)); // Y[a*k+b] = X[k], b = [0..a-1]
                Vector1N InsetXn(const Vector1N& Xn, int a, int b = 0, const T& v = 0); // Y[a*k+b] = X[k], b = [0..a-1]

                int Index(int index, const Vector1N &input);
                int Index(int index, const Vector1C &input);
                
                Vector1N Resize(const Vector1N &input, int N, int offset = 0);
                Vector1C Resize(const Vector1C &input, unsigned int N, int offset = 0);
                Vector1C ResizeDFT(const Vector1C &inputDFT, SpectrumProperty spectrum, unsigned int outputSize);

                double Length(const Vector1N &v);
                Vector1C TimeShift(const Vector1C &Xn, NumericT fs, NumericT time);

                Vector1C PhaseShift(const Vector1C &Xn, NumericT phase);
                Vector1N Roll(const Vector1N &Xn, int nbins);
                
                Vector1C Roll(const Vector1C &Xn, int nbins);
                
                Vector1C FrequencyShift(const Vector1N &x, NumericT fs, NumericT fc);
                Vector1C FrequencyShift(const Vector1C &x, NumericT fs, NumericT fc);

                Vector1N Flip(Vector1N v);
                Vector1C Flip(Vector1C v);
                Vector1N Filter(const Vector1N &_v, std::vector<int> indexes);
                Vector1C Filter(const Vector1C &_v, std::vector<int> indexes);
                Vector1N Range(unsigned int N, const NumericT &dx);
                Vector1N Range(unsigned int N, const NumericT &min, const NumericT &max);

                Vector1N Range(TH1D *h);
                Vector1N Range(TAxis *x);
                                
                Vector1N Convolve(const Vector1N &x, const Vector1N &y);
                Vector1C Convolve(const Vector1C &x, const Vector1C &y);
                Vector1N Correlate(Vector1N x, const Vector1N &y);
                Vector1C Correlate(Vector1C x, const Vector1C &y);
                Vector1N Autocorrelate(const Vector1N & x);
                Vector1N Autocorrelate(const Vector1C & x);

                NumericT Distance(const Vector1N &a, const Vector1N &b);
                std::pair<NumericT,NumericT> /* a,b */ Regression(const Vector1N &x, const Vector1N &y);
                std::pair<NumericT,NumericT> /* a,b */ Regression(const Vector1N &x);

                /**
                 * Numerical operations
                 */
                bool EpsilonEqualTo(const T& a, const T& b, const T& epsilon = std::numeric_limits<T>::epsilon());
                bool EpsilonEqualTo(const TMatrixT<NumericT>& M, const T& b, const T& epsilon = std::numeric_limits<T>::epsilon());
                bool EpsilonEqualTo(const ComplexT & z1, const ComplexT & z2, const T& epsilon = std::numeric_limits<T>::epsilon() );
                bool EpsilonEqualTo(const ComplexT & z, const T& r, const T& epsilon = std::numeric_limits<T>::epsilon() );
                bool EpsilonEqualTo(const Vector1N& a, const Vector1N& b, const T& epsilon = std::numeric_limits<T>::epsilon() );
                bool EpsilonEqualTo(const Vector1N& a, const T& b, const T& epsilon = std::numeric_limits<T>::epsilon() );
                bool EpsilonEqualTo(const Vector1N& a, const ComplexT & b, const T& epsilon = std::numeric_limits<T>::epsilon() );
                bool EpsilonEqualTo(const Vector1C& a, const T& b, const T& epsilon = std::numeric_limits<T>::epsilon() );
                bool EpsilonEqualTo(const Vector1C& a, const ComplexT & b, const T& epsilon = std::numeric_limits<T>::epsilon() );
                bool EpsilonEqualTo(const Vector1C& z1, const Vector1C& z2, const T& epsilon = std::numeric_limits<T>::epsilon() );
                bool EpsilonEqualTo(const Vector1C& z, const Vector1N& r, const T& epsilon = std::numeric_limits<T>::epsilon() );

                NumericT StripNaN( const T& a, NumericT replacement = 0 );
                Vector1N StripNaN( const Vector1N& v, NumericT replacement = 0 );
                Vector1C StripNaN( const Vector1C& z, NumericT replacement = 0);

                NumericT StripInf( const T& a, NumericT replacement = 0 );
                Vector1N StripInf( const Vector1N& v, NumericT replacement = 0 );
                Vector1C StripInf( const Vector1C& z, NumericT replacement = 0);

                bool Contains( const NumericT &a, const Vector1N& b );

                ROOT::Math::Interpolator Interpolator(Vector1N x, Vector1N y, ROOT::Math::Interpolation::Type type);

                NumericT MathMod(NumericT a, NumericT b);

                /**
                 * Matrix shortcut
                 */
                
                template<typename Ty, typename Tx = T>
                TMatrixT<Ty> Matrix(const TMatrixT<Tx>& A) {

                        if(std::is_same<Tx, Ty>::value) return A;

                        int nRows = A.GetNrows();
                        int nCols = A.GetNcols();

                        TMatrixT<Ty> B(nRows, nCols);
                        for (int i = 0; i < nRows; ++i) {
                                for (int j = 0; j < nCols; ++j) {
                                        B(i, j) = static_cast<Ty>(A(i, j));
                                }
                        }

                        return B;
                }

                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> Exp(const TMatrixT<Tx>& a) {

                        const size_t Nx = a.GetNrows();
                        TMatrixT<Tx> b(Nx, Nx);

                        gsl_matrix_const_view va = gsl_matrix_const_view_array(reinterpret_cast<const double*>(a.GetMatrixArray()), Nx, Nx);
                        gsl_matrix_const_view vb = gsl_matrix_const_view_array(reinterpret_cast<const double*>(b.GetMatrixArray()), Nx, Nx);

                        gsl_linalg_exponential_ss((const gsl_matrix*)&va, (gsl_matrix*)&vb, 0);

                        return std::is_same<Tx, Ty>::value ? b : this->template Matrix<Ty>(b);
                }

                template <typename Ty = T, typename Tx = T>
                TVectorT<Ty> Zeros(const TVectorT<Tx> &V)
                {

                        TVectorT<Ty> Vc(V.GetNoElements());
                        for(int i = 0, N = V.GetNoElements(); i < N; i++) {
                                Vc(i) = static_cast<Ty>(0);
                        }

                        return Vc;
                }

                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> Zeros(const TMatrixT<Tx> &M)
                {
                        TMatrixT<Tx> Mc(M.GetNrows(), M.GetNcols());
                        for(int i = 0, I = M.GetNrows(); i < I; i++) {
                                for(int j = 0, J = M.GetNcols(); j < J; j++) {
                                        Mc(i,j) = static_cast<Ty>(0);
                                }
                        }

                        return std::is_same<Tx, Ty>::value ? M : this->template Matrix<Ty>(M);
                }
                  
                template <typename Ty = T>
                TMatrixT<Ty> Eye(int n, int offset = 0)
                {
                        TMatrixT<Ty> M(n,n); 
                        for(int i = 0, I = TMath::Max(0, M.GetNrows() - std::abs(offset)); i < I; i++) {

                                if(offset < 0) M(i + std::abs(offset),i) = static_cast<Ty>(1);
                                else if(offset > 0) M(i,i + std::abs(offset)) = static_cast<Ty>(1);
                                else M(i,i) = static_cast<Ty>(1);
                        }

                        return std::is_same<T, Ty>::value ? M : this->template Matrix<Ty>(M);
                }

                template <typename Ty = T>
                TMatrixT<Ty> Identity(int n) { return Identity(n,n); }
                template <typename Ty = T>
                TMatrixT<Ty> Identity(int n, int m) { 
                        
                        TMatrixT<NumericT> M(n,m);
                        for(int i = 0; i < TMath::Min(n,m); i++)
                                M(i,i) = 1;

                        return M;
                }

                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> Transpose(const TMatrixT<Tx> &M)
                {
                        TMatrixT<Tx> Mt = M;
                                     Mt.T();

                        return std::is_same<Tx, Ty>::value ? Mt : this->template Matrix<Ty>(Mt);
                }

                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> GetVerticalBlockMatrix(const std::vector<TMatrixT<Tx>> &M) 
                {
                        if(!M.size()) return TMatrixT<Ty>();

                        TMatrixT<Ty> S = M[0];
                        for(int k = 1, K = M.size(); k < K; k++) {

                                if(S.GetNcols() != M[k].GetNcols()) throw std::invalid_argument("Unexpected column dimension provided for M["+TString::Itoa(k,10)+"] matrix");
                                
                                S.ResizeTo(S.GetNrows()+M[k].GetNrows(), S.GetNcols());

                                for(int i = 0, I = M[k].GetNrows(); i < I; i++) {

                                        for(int j = 0, J = M[k].GetNcols(); j < J; j++) {

                                                S(S.GetNrows()-1+i, j) = static_cast<Ty>(M[k](i,j));
                                        }              
                                }
                        }

                        return S;
                }

                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> GetHorizontalBlockMatrix(const std::vector<TMatrixT<Tx>> &M) 
                {
                        if(!M.size()) return TMatrixT<Ty>();

                        TMatrixT<Ty> S = M[0];
                        
                        for(int k = 1, K = M.size(); k < K; k++) {

                                if(S.GetNrows() != M[k].GetNrows()) throw std::invalid_argument("Unexpected row dimension provided for M["+TString::Itoa(k,10)+"] matrix");
                                
                                int Scols = S.GetNcols();
                                S.ResizeTo(S.GetNrows(), S.GetNcols() + M[k].GetNcols());

                                for(int i = 0, I = M[k].GetNrows(); i < I; i++) {

                                        for(int j = 0, J = M[k].GetNcols(); j < J; j++) {

                                                S(i, Scols+j) = static_cast<Ty>(M[k](i,j));
                                        }
                                }
                        }

                        return S;
                }

                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> GetVerticalBlockMatrix(const TMatrixT<Tx> &A, const TMatrixT<Tx> &B) { return GetVerticalBlockMatrix<Tx>(std::vector<TMatrixT<Tx>>({A,B})); }
                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> GetHorizontalBlockMatrix(const TMatrixT<Tx> &A, const TMatrixT<Tx> &B) { return GetHorizontalBlockMatrix<Tx>(std::vector<TMatrixT<Tx>>({A,B})); }
                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> GetBlockMatrix(const TMatrixT<Tx> &A, const TMatrixT<Tx> &B) { return GetHorizontalBlockMatrix<Tx>(A,B); }
                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> GetBlockMatrix(const std::vector<std::vector<TMatrixT<Tx>>> &M) 
                {
                        std::vector<TMatrixT<Tx>> S;
                        for(int i = 0, N = M.size(); i < N; i++)
                                S.push_back(this->GetHorizontalBlockMatrix<Ty>(M[i]));

                        return this->GetVerticalBlockMatrix<Ty>(S);
                }

                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> GetDiagonalBlockMatrix(const TMatrixT<Tx> &A, const TMatrixT<Tx> &B) { return GetDiagonalBlockMatrix<Tx, Ty>(std::vector<TMatrixT<T>>({A,B})); }
                template <typename Ty = T, typename Tx = T>
                TMatrixT<Ty> GetDiagonalBlockMatrix(const std::vector<TMatrixT<Tx>> &M) 
                {
                        if(!M.size()) return TMatrixT<Ty>();

                        TMatrixT<Ty> S = M[0];
                        for(int k = 1, K = M.size(); k < K; k++) {

                                S.ResizeTo(S.GetNrows()+M[k].GetNrows(), S.GetNcols()+M[k].GetNcols());
                                
                                for(int i = 0, I = M[k].GetNrows(); i < I; i++) {

                                        for(int j = 0, J = M[k].GetNcols(); j < J; j++) {

                                                S(S.GetNrows()-M[k].GetNrows()+i, S.GetNcols()-M[k].GetNrows()+j) = static_cast<Ty>(M[k](i,j));
                                        }              
                                }
                        }

                        return S;
                }

                template <typename Tx = T>
                typename TKFRLegacy<T>::Vector1N GetMatrixRow(const TMatrixT<Tx> &M, int i)
                {
                        int J = M.GetNcols();
                        if(i >= M.GetNrows()) return {};

                        const T *array = M.GetMatrixArray();
                        return Vector1N(&array[i*J], &array[i*J] + J);
                }

                template <typename Tx = T>
                typename TKFRLegacy<T>::Vector1N GetMatrixCol(const TMatrixT<Tx> &M, int j)
                {
                        Vector1N col;
                        if(j >= M.GetNcols()) return col;

                        const T* array = M.GetMatrixArray();
                        for (int i = 0; i < M.GetNrows(); i++)
                                col.push_back(array[i * M.GetNcols() + j]);

                        return col;
                }

                Vector1N Flatten(Vector2N const &vec);
                int Closest(Vector1N v, NumericT value);
                Vector1N Remove(Vector1N v, const NumericT &element);
                Vector1N RemoveLeading(Vector1N v, const NumericT &element);
                Vector1N RemoveLeadingZeros(Vector1N v);
                Vector1N RemoveTrailing(Vector1N v, const NumericT &element);
                Vector1N RemoveTrailingZeros(Vector1N v);
                Vector2N Inflatten(Vector1N const &vec, int length);

                Vector2C Transpose(const Vector2C &v);
                Vector2N Transpose(const Vector2N &v);

                Vector1C EigenValues(const TMatrixT<NumericT> &matrix);
                Vector1C EigenVector(const TMatrixT<NumericT> &matrix);

                /**
                 * Digital filtering 
                 */
                ZPK ZeroPoleGain(const kfr::zpk<T> &params);
                SOS SecondOrderSections(const kfr::zpk<T> &params);
                SS  StateSpace (const kfr::zpk<T> &params);
                TF  TransferFunction (const kfr::zpk<T> &params);

                ZPK ZeroPoleGain(const kfr::biquad_params<T> &params);
                SOS SecondOrderSections(const kfr::biquad_params<T> &params);
                SS  StateSpace (const kfr::biquad_params<T> &params);
                TF  TransferFunction (const kfr::biquad_params<T> &params);

                ZPK ZeroPoleGain(const TBiquad &params);
                SOS SecondOrderSections(const TBiquad &params);
                SS  StateSpace (const TBiquad &params);
                TF  TransferFunction (const TBiquad &params);
                
                TF TransferFunction(const SOS &sos);
                TF TransferFunction(const TF  &tf);
                TF TransferFunction(const ZPK &zpk);
                TF TransferFunction(const SS  &ss);

                std::vector<TF> TransferFunction(const std::vector<ZPK> &zpk);
                std::vector<TF> TransferFunction(const std::vector<SOS> &sos);
                std::vector<TF> TransferFunction(const std::vector<SS>   &ss);
                std::vector<TF> TransferFunction(const std::vector<TF>   &tf);
                
                TF  TransferFunction (const Vector1N &B, const Vector1N &A);
                ZPK ZeroPoleGain(const Vector1N &B, const Vector1N &A);
                SOS SecondOrderSections(const Vector1N &B, const Vector1N &A);
                SS  StateSpace (const Vector1N &B, const Vector1N &A);
                
                SS StateSpace(const TF  &tf);
                SS StateSpace(const ZPK &zpk);
                SS StateSpace(const SOS &sos);
                SS StateSpace(const SS  &ss);
                
                ZPK ZeroPoleGain(const TF  &tf);
                ZPK ZeroPoleGain(const SOS &sos);
                ZPK ZeroPoleGain(const SS  &ss);
                ZPK ZeroPoleGain(const ZPK &zpk);
                
                std::vector<ZPK> ZeroPoleGain(const std::vector<TF>  &tf);
                std::vector<ZPK> ZeroPoleGain(const std::vector<SOS> &sos);
                std::vector<ZPK> ZeroPoleGain(const std::vector<SS>  &ss);
                std::vector<ZPK> ZeroPoleGain(const std::vector<ZPK>  &zpk);

                SOS SecondOrderSections(const ZPK &zpk);
                SOS SecondOrderSections(const TF  &tf) ;
                SOS SecondOrderSections(const SS  &ss) ;
                SOS SecondOrderSections(const SOS &sos);

                SOS SecondOrderSections(const std::vector<SS>   &ss);
                SOS SecondOrderSections(const std::vector<ZPK> &zpk);
                SOS SecondOrderSections(const std::vector<TF>   &tf);
                SOS SecondOrderSections(const std::vector<SOS> &sos);

                TF  TransferFunction (const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A);
                ZPK ZeroPoleGain(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A);
                SS  StateSpace (const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A);
                SOS SecondOrderSections(const ROOT::Math::Polynomial &B, const ROOT::Math::Polynomial &A);

                TF  TransferFunction (const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D);
                ZPK ZeroPoleGain(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D);
                SS  StateSpace (const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D);
                SOS SecondOrderSections(const TMatrixT<NumericT> &A, const TMatrixT<NumericT> &B, const TMatrixT<NumericT> &C, const TMatrixT<NumericT> &D);

                ROOT::Math::Polynomial Polynomial(const Vector1N &coeffs);
                Vector1N PolynomialCoefficients(ROOT::Math::Polynomial &P);
                Vector1C PolynomialCoefficients(Vector1C &vroots);
                Vector1N Multiply(ROOT::Math::Polynomial &A, ROOT::Math::Polynomial &B);
                
                Vector1C Solve(ROOT::Math::Polynomial &P);
                Vector1C Solve(const Vector1N &coeffs);
                Vector1C Solve(const TMatrixT<NumericT> &M);

                ComplexT Eval(const ComplexT &z, const Vector1N &par);
                NumericT Eval(const NumericT &x, const Vector1N &par);
        };
    }
}
