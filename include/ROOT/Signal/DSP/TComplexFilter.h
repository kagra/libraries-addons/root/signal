/**
 * *********************************************
 *
 * \file TComplexFilter.h
 * \brief Header of the TComplexFilter class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TComplexFilter
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 *
 * *********************************************
 */

#pragma once

#include <Riostream.h>
#include <TMarker.h>
#include <TExec.h>
#include <TComplex.h>

#include "../TKFRLegacy.h"
#include <ROOT/IOPlus/Helpers.h>

namespace ROOT { namespace Signal {

        class TComplexFilter
        {
                protected:

                        ROOT::Math::Polynomial *B = NULL;
                        ROOT::Math::Polynomial *A = NULL;
                        Filter type;

                public:

                        TComplexFilter() {};
                        ~TComplexFilter() {
                                
                                delete this->B;
                                this->B = NULL;
                                
                                delete this->A;
                                this->B = NULL;
                        };

                        TComplexFilter(                       std::vector<double> B, std::vector<double> A): TComplexFilter(Filter::Analog, 1, B, A) { }
                        TComplexFilter(Filter type,           std::vector<double> B, std::vector<double> A): TComplexFilter(type,           1, B, A) { }
                        TComplexFilter(             double k, std::vector<double> B, std::vector<double> A): TComplexFilter(Filter::Analog, k, B, A) { }
                        TComplexFilter(Filter type, double k, std::vector<double> B, std::vector<double> A);

                        TComplexFilter(                       TF tf): TComplexFilter(         tf.B, tf.A) { }
                        TComplexFilter(Filter type,           TF tf): TComplexFilter(type,    tf.B, tf.A) { }
                        TComplexFilter(             double k, TF tf): TComplexFilter(      k, tf.B, tf.A) { }
                        TComplexFilter(Filter type, double k, TF tf): TComplexFilter(type, k, tf.B, tf.A) { }

                        inline bool IsDigital() const { return type == Filter::Digital; }
                        inline bool IsAnalog () const { return type == Filter::Analog; }

                        inline std::vector<std::complex<double>> GetPoles() { return this->A->FindRoots(); }
                        inline std::vector<std::complex<double>> GetZeros() { return this->B->FindRoots(); }
                        inline double GetGain() { return this->B->NPar() > 0 ? this->B->Parameters()[0] : 0; }
                        void SetGain(double k);

                        inline TF  TransferFunction() { return GetTransferFunction(); }
                        inline TF  GetTransferFunction() { return gKFRLegacy->TransferFunction(*this->B, *this->A); }
                        inline SOS SecondOrderSections() { return GetSecondOrderSection(); }
                        inline SOS GetSecondOrderSection() { return gKFRLegacy->SecondOrderSections(*this->B, *this->A); }
                        inline ZPK ZeroPoleGain() { return GetZerosPolesGain(); }
                        inline ZPK GetZerosPolesGain() { return gKFRLegacy->ZeroPoleGain(*this->B, *this->A); }
                        inline SS  StateSpace() { return GetStateSystem(); }
                        inline SS  GetStateSystem() { return gKFRLegacy->StateSpace(*this->B, *this->A); }
        
                        TComplexFilter *Cast(Filter type, double Ts, FilterTransform method = FilterTransform::Bilinear, double alpha = NAN);
                        std::vector<std::complex<double>> Response(std::vector<std::complex<double>> X);
                        std::vector<double> Response(std::vector<double> x);
                        
                        void Print(Option_t *option = "") const;

                protected:
                        std::vector<double> RemoveTrailing(std::vector<double> v, const double &element);

                ClassDef(TComplexFilter, 1);
        };
}}
