/**
 * *********************************************
 *
 * \file TPoleZeroMap.h
 * \brief Header of the TPoleZeroMap class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TPoleZeroMap
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 *
 * *********************************************
 */

#pragma once

#include <Riostream.h>
#include <TMarker.h>
#include <TExec.h>

#include <TSpectrum.h>
#include <TPolyMarker.h>
#include <TArrow.h>

#include <TFitResultPtr.h>
#include <TFitResult.h>

#include "ROOT/Signal/EnumTypes.h"
#include "ROOT/Signal/DSP/TComplexPlan.h"
#include "ROOT/Signal/DSP/TComplexFilter.h"

namespace ROOT { namespace Signal {

        class TPoleZeroMap: public TComplexPlan, public TComplexFilter
        {
                protected:
                        static double Distance(std::vector<double> a, std::vector<double> b);
                        static Double_t Eval(const Double_t *x, const Double_t *par);

                public:

                        using TComplexPlan::TComplexPlan;
                        TPoleZeroMap() {};
                        ~TPoleZeroMap() { };

                        TPoleZeroMap(const char *name,                          const std::vector<double> &B, const std::vector<double> &A, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, Filter::Analog, 1, B, A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name, Filter filter,           const std::vector<double> &B, const std::vector<double> &A, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, filter,         1, B, A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name,                double k, const std::vector<double> &B, const std::vector<double> &A, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, Filter::Analog, k, B, A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name, Filter filter, double k, const std::vector<double> &B, const std::vector<double> &A, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr);

                        TPoleZeroMap(const char *name,                          const TF &tf, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, Filter::Analog, 1, tf.B, tf.A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name, Filter filter,           const TF &tf, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, filter,         1, tf.B, tf.A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name,                double k, const TF &tf, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, Filter::Analog, k, tf.B, tf.A, xmin, xmax, ymin, ymax, opt) { }
                        TPoleZeroMap(const char *name, Filter filter, double k, const TF &tf, Double_t xmin=-1, Double_t xmax=1, Double_t ymin=-1, Double_t ymax=1, Option_t * opt = nullptr): TPoleZeroMap(name, filter,         k, tf.B, tf.A, xmin, xmax, ymin, ymax, opt) { }

                        void Draw(Option_t *option = "", int level = 0);
                        void DrawNyquist(Option_t *option = "", int N = 4096);

                        TGraph *Nyquist(int N = 4096);
                        static void NyquistArrows(TString contourName, int dmin = 25);
                                
                        TPoleZeroMap *Cast(Filter type, double Ts, FilterTransform method = FilterTransform::Bilinear, double alpha = NAN);

                        using TComplexFilter::Print;
                        
                ClassDef(ROOT::Signal::TPoleZeroMap, 1);
        };
}}