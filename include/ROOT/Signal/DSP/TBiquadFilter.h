/**
 * *********************************************
 *
 * \file TBiquadFilter.h
 * \brief Header of the TBiquadFilter class
 * \author Marco Meyer \<marco.meyer@omu.ac.jp\>
 *
 * *********************************************
 *
 * \class TBiquadFilter
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 * \version Revision: 1.0
 *
 * *********************************************
 */

#pragma once

#include <Riostream.h>
#include <Math/Polynomial.h>
#include <TMarker.h>
#include <TExec.h>

#include "ROOT/Signal/DSP/TComplexFilter.h"
#include <tuple>

namespace ROOT {
        namespace Signal {

                class TBiquadFilter: public TComplexFilter
                {      
                        public:
                                
                                using TComplexFilter::TComplexFilter;
                                TBiquadFilter() {};
                                ~TBiquadFilter() { };

                                TBiquadFilter(Filter filter, TQuad B, TQuad A): TBiquadFilter(filter,          1, B, A) { }
                                TBiquadFilter(               TQuad B, TQuad A): TBiquadFilter(Filter::Digital, 1, B, A) { }
        
                                TBiquadFilter(Filter filter, double k, TQuad B, TQuad A);
                                TBiquadFilter(               double k, TQuad B, TQuad A): TBiquadFilter(Filter::Digital,          k, B, A) { }
        
                        ClassDef(ROOT::Signal::TBiquadFilter, 1);
                };
        }
}

#pragma once
