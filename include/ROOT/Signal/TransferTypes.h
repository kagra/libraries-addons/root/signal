#pragma once

#include <vector>
#include <complex>
#include <tuple>

namespace ROOT {
namespace Signal {

        // _TQuad<T> struct
        template<typename T>
        struct _TQuad {
                T x0, x1, x2;

                _TQuad(T x0 = 0, T x1 = 0, T x2 = 0) : x0(x0), x1(x1), x2(x2) {}
                _TQuad(std::tuple<T,T,T> x) : x0(std::get<0>(x)), x1(std::get<1>(x)), x2(std::get<2>(x)) {}

                _TQuad<T>& operator=(const _TQuad<T>& other) {
                        if (this != &other) {
                        this->x0 = other.x0;
                        this->x1 = other.x1;
                        this->x2 = other.x2;
                        }
                        return *this;
                }

                bool operator==(const _TQuad<T>& other) const {
                        return this->x0 == other.x0 && this->x1 == other.x1 && this->x2 == other.x2;
                }

        };

        // _TBiquad<T> struct
        template<typename T>
        struct _TBiquad {
                _TQuad<T> B;
                _TQuad<T> A;

                _TBiquad(_TQuad<T> B = _TQuad<T>(), _TQuad<T> A = _TQuad<T>()) : B(B), A(A) {}
                _TBiquad(std::tuple<T,T,T> B, std::tuple<T,T,T> A) : B(B), A(A) {}

                _TBiquad<T>& operator=(const _TBiquad<T>& other) {
                        if (this != &other) {
                        this->B = other.B;
                        this->A = other.A;
                        }
                        return *this;
                }

                bool operator==(const _TBiquad<T>& other) const {
                        return this->B == other.B && this->A == other.A;
                }
        };

        // _SOS<T> struct
        template<typename T>
        struct _SOS {

                std::vector<_TBiquad<T>> sections;

                void push_back(const _TBiquad<T>& section) {
                        sections.push_back(section);
                }

                void reserve(int N) {
                        sections.reserve(N);
                }

                void resize(int N) {
                        sections.resize(N);
                }

                void pop_back() {
                        sections.pop_back();
                }

                const typename std::vector<_TBiquad<T>>::iterator end() {
                        return sections.end();
                }

                const typename std::vector<_TBiquad<T>>::iterator begin() {
                        return sections.begin();
                }

                size_t size() const {
                        return sections.size();
                }

                void insert(typename std::vector<_TBiquad<T>>::iterator it, typename std::vector<_TBiquad<T>>::const_iterator begin, typename std::vector<_TBiquad<T>>::const_iterator end) {
                        sections.insert(it, begin, end);
                }

                void push_back(const _SOS<T>& _sos) {
                        for (int i = 0; i < _sos.size(); i++) {
                        sections.push_back(_sos[i]);
                        }
                }

                const _TBiquad<T>& operator[](size_t ind) const {
                        return sections[ind];
                }

                _TBiquad<T>& operator[](size_t ind) {
                        return sections[ind];
                }

                _SOS(std::vector<_TBiquad<T>> sec = {}) : sections(sec) {}

                _SOS(size_t N, _TBiquad<T> biquad = {}) {
                        for (int i = 0; i < N; i++) {
                        sections.push_back(biquad);
                        }
                }

                _SOS<T>& operator=(const _SOS<T>& other) {
                        if (this != &other) {
                        this->sections = other.sections;
                        }
                        return *this;
                }

                bool operator==(const _SOS<T>& other) const {
                        return this->sections == other.sections;
                }
        };

        // _TF<T> struct
        template<typename T>
        struct _TF {
        std::vector<T> B;
        std::vector<T> A;

        void push_back(const T& b, const T& a) {
                B.push_back(b);
                A.push_back(a);
        }

        void pop_back() {
                B.pop_back();
                A.pop_back();
        }

        _TF(std::vector<T> _B = {}, std::vector<T> _A = {}) : B(_B), A(_A) {}

        _TF<T>& operator=(const _TF<T>& other) {
                if (this != &other) {
                this->B = other.B;
                this->A = other.A;
                }
                return *this;
        }

        bool operator==(const _TF<T>& other) const {
                return this->B == other.B && this->A == other.A;
        }

        };

        // _SS<T> struct
        template<typename T>
        struct _SS {
                TMatrixT<T> A;
                TMatrixT<T> B;
                TMatrixT<T> C;
                TMatrixT<T> D;

                _SS(TMatrixT<T> A = TMatrixT<T>(), TMatrixT<T> B = TMatrixT<T>(), TMatrixT<T> C = TMatrixT<T>(), TMatrixT<T> D = TMatrixT<T>()) : A(A), B(B), C(C), D(D) {}

                _SS<T>& operator=(const _SS<T>& other) {
                        if (this != &other) {
                        this->A = other.A;
                        this->B = other.B;
                        this->C = other.C;
                        this->D = other.D;
                        }
                        return *this;
                }

                bool operator==(const _SS<T>& other) const {
                        return this->A == other.A && this->B == other.B && this->C == other.C && this->D == other.D;
                }
        };

        // _ZPK<T> struct
        template<typename T>
        struct _ZPK {
                std::vector<std::complex<T>> zeros;
                std::vector<std::complex<T>> poles;
                T gain;

                _ZPK(std::vector<std::complex<T>> z = {}, std::vector<std::complex<T>> p = {}, T k = 1) : zeros(z), poles(p), gain(k) {}

                _ZPK<T>& operator=(const _ZPK<T>& other) {
                        if (this != &other) {
                        this->zeros = other.zeros;
                        this->poles = other.poles;
                        this->gain = other.gain;
                        }
                        return *this;
                }

                bool operator==(const _ZPK<T>& other) const {
                        return this->zeros == other.zeros && this->poles == other.poles && this->gain == other.gain;
                }
        };

        // Operator<< overloads for outputting to streams
        template<typename T>
        std::ostream& operator<<(std::ostream& os, const _TBiquad<T>& biquad) {
                os << "-- Biquad parameters (b,a):" << std::endl;
                os << "-- b = " << biquad.B << std::endl;
                os << "-- a = " << biquad.A << std::endl;

                return os;
        }

        template<typename T>
        std::ostream& operator<<(std::ostream& os, const _SOS<T>& sos) {

                os << "-- SOS representation (" << sos.size() << " sections)" << std::endl;
                for (size_t i = 0; i < sos.size(); ++i) {
                        os << sos[i] << std::endl;
                }

                return os;
        }

        // Overload operator<< for vectors of SOS
        template<typename T>
        std::ostream& operator<<(std::ostream& os, const std::vector<_SOS<T>>& sos) {

                if (sos.empty()) os << "-- Empty SOS representation" << std::endl;
                else {
                
                        for (const auto& so : sos) {
                                os << so << std::endl;
                        }
                }
                
                return os;
        }

        template<typename T>
        std::ostream& operator<<(std::ostream& os, const _TF<T>& tf) {

                os << "-- Transfer function (B/A):" << std::endl;
                os << "-- B = " << tf.first << std::endl;
                os << "-- A = " << tf.second << std::endl;

                return os;
        }

        // Overload operator<< for vectors of TF
        template<typename T>
        std::ostream& operator<<(std::ostream& os, const std::vector<_TF<T>>& tf) {

                if (tf.empty()) os << "-- Empty TF representation" << std::endl;
                else {

                        for (const auto& t : tf) {
                                os << t << std::endl;
                        }
                }

                return os;
        }

        template<typename T>
        std::ostream& operator<<(std::ostream& os, const _SS<T>& ss) {

                os << "-- State-system representation (A,B,C,D):" << std::endl;
                os << "-- Matrix A:" << std::endl << ss.A << std::endl;
                os << "-- Matrix B:" << std::endl << ss.B << std::endl;
                os << "-- Matrix C:" << std::endl << ss.C << std::endl;
                os << "-- Matrix D:" << std::endl << ss.D << std::endl;

                return os;
        }

        // Overload operator<< for vectors of SS
        template<typename T>
        std::ostream& operator<<(std::ostream& os, const std::vector<_SS<T>>& ss) {

                if (ss.empty()) os << "-- Empty SS representation" << std::endl;
                else {
                
                        for (const auto& s : ss) {
                                os << s << std::endl;
                        }
                }
                return os;
        }

        template<typename T>
        std::ostream& operator<<(std::ostream& os, const _ZPK<T>& zpk) {
                
                os << "-- Zero-Pole-Gain model (z,p,k):" << std::endl;
                os << "-- z = " << zpk.zeros << std::endl;
                os << "-- p = " << zpk.poles << std::endl;
                os << "-- k = " << zpk.gain << std::endl;

                return os;
        }

        // Overload operator<< for vectors of ZPK
        template<typename T>
        std::ostream& operator<<(std::ostream& os, const std::vector<_ZPK<T>>& zpk) {

                if (zpk.empty()) os << "-- Empty ZPK representation" << std::endl;
                else {
                
                        for (const auto& zp : zpk) {
                                os << zp << std::endl;
                        }
                }

                return os;
        }


        // Default "double" declaration
        using ZPK     = _ZPK<double>;
        using SOS    = _SOS<double>;
        using TF      = _TF<double>;
        using SS      = _SS<double>;
        using TBiquad = _TBiquad<double>;
        using TQuad   = _TQuad<double>;

} // namespace Signal
} // namespace ROOT
