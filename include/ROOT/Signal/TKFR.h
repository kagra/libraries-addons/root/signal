#pragma once

#include <Riostream.h>
#include <TString.h>
#include <TObject.h>
#include <TMatrixT.h>
#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TCanvas.h>
#include <TFormula.h>
#include <Math/Interpolator.h>
#include <Math/Polynomial.h>

#include <Math/Polynomial.h>
#include <TComplex.h>
#include <TMatrixDEigen.h>
#include <TDecompLU.h>

#include <thread>
#include <complex>
#include <vector>
#include <map>

#include "ROOT/Signal/EnumTypes.h"
#include "ROOT/Signal/TransferTypes.h"

namespace ROOT {
    namespace Signal {

       template <typename T = double>
       class TKFR {

       public:
              
              const static std::complex<T> i;

              // Complex calculation aliases
              using NumericT = T;
              using ComplexT = std::complex<T>;

              using Vector1N = std::vector<NumericT>;
              using Vector1C = std::vector<ComplexT>;

              using Vector2N = std::vector<Vector1N>;
              using Vector2C = std::vector<Vector1C>;
              
              using ZPK = ROOT::Signal::_ZPK<T>;
              using SS  = ROOT::Signal::_SS<T>;
              using TF  = ROOT::Signal::_TF<T>;
              using SOS = ROOT::Signal::_SOS<T>;

              using TQuad = ROOT::Signal::_TQuad<T>;
              using TBiquad = ROOT::Signal::_TBiquad<T>;
              
       private:
              class Impl;
              Impl *pImpl = NULL;
              
       public:

              static bool IsAlreadyRunningMT;
              static std::atomic<int> finishedWorkers;

              TKFR();
              ~TKFR();

              const char *Version();

              ClassDef(TKFR, 1);
       };

       using Complex = TKFR<double>::ComplexT;
       using Real    = TKFR<double>::NumericT;
       using Imag    = TKFR<double>::NumericT;
    }
}

using TKFR = ROOT::Signal::TKFR<double>;
R__EXTERN TKFR *gKFR;