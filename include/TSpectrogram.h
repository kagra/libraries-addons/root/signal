#pragma once

#include "ROOT/Signal/TSpectrogramT.h"

class TSpectrogram : public ROOT::Signal::TSpectrogramT<double> {

    using TSpectrogramT<double>::TSpectrogramT;

    ClassDef(TSpectrogram, 1);  // ROOT Class Definition
};