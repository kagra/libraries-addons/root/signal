#pragma once

#include "ROOT/Signal/TFrequencySeriesT.h"

class TFrequencySeries : public ROOT::Signal::TFrequencySeriesT<std::complex<double>> {

    using TFrequencySeriesT<double>::TFrequencySeriesT;

    ClassDef(TFrequencySeries, 1);  // ROOT Class Definition
};