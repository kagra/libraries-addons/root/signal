#include <complex>
#include <type_traits>

template<typename T>
inline constexpr bool is_complex_v = std::is_complex<T>::value;

// Helper function to compare two vectors of a generic type
template <typename T>
void _EXPECT_NEAR(const std::vector<T>& expected, const std::vector<T>& actual, T tolerance = std::numeric_limits<T>::epsilon()) {

    ASSERT_EQ(expected.size(), actual.size()) << "Vectors are of unequal length";

    // fix: epsilon limit for complex is 0
    if constexpr (std::is_complex_v<T>) {

        using Tx = std::extract_complex_type_t<T>;
        Tx _tolerance = std::numeric_limits<Tx>::epsilon();
        
        tolerance = std::complex<Tx>(std::max(tolerance.real(), _tolerance), std::max(tolerance.imag(), _tolerance));
    }

    for (size_t i = 0; i < expected.size(); ++i) {

        if constexpr (std::is_complex_v<T>) {

            EXPECT_NEAR(expected[i].real(), actual[i].real(), tolerance.real()) << "Real parts differ at index " << i;
            EXPECT_NEAR(expected[i].imag(), actual[i].imag(), tolerance.imag()) << "Imaginary parts differ at index " << i;

        } else {

            EXPECT_NEAR(expected[i], actual[i], tolerance) << "Vectors differ at index " << i;
        }
    }
}

template <typename T>
void _EXPECT_NEAR(const std::vector<std::complex<T>>& expected, const std::vector<std::complex<T>>& actual, T tolerance = std::numeric_limits<T>::epsilon()) { _EXPECT_NEAR(expected, actual, std::complex<T>(tolerance,tolerance)); }
