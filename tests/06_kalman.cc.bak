#include <Riostream.h>
#include <TMatrixD.h>

#include <vector>
#include <ROOT/Signal/DSP/TKalmanFilter.h>

//
// Adapted from https://github.com/hmartiro/kalman-cpp/tree/master
// Source: http://www.cs.unc.edu/~welch/media/pdf/kalman_intro.pdf

// NB: Some other examples that might be implemented..
// https://dsp.stackexchange.com/questions/19372/how-to-get-rid-of-noise-and-oscillations-using-kalman-filtering
// https://dsp.stackexchange.com/questions/73132/kalman-filter-for-harmonic-oscillator-state-variable-and-covariance-matrix

using namespace ROOT::Signal;
#include <TKFRLegacy *.h>
#include <ROOT/IOPlus/Helpers.h>
#include <gtest/gtest.h>

using namespace ROOT::Signal;

// Test case for the add function
TEST(KalmanTest, KalmanCpp) {


    // Number of states
    int n = 3; 
    // Number of measurements
    int m = 1; 

    // Time step
    double dt = 1.0/30; 

    //
    // Discrete LTI projectile motion, measuring position only
    //
    // System dynamics matrix
    double a[] = {1, dt, 0, //0.5*dt*dt,
                  0,  1, dt, 
                  0,  0,  1};

    // Output matrix
    double h[] = {1, 0, 0};

    // Estimate error covariance
    double p[] = {.1,    .1,  .1, 
                  .1, 10000,  10, 
                  .1,    10, 100};

    // Reasonable covariance matrices
    // Process noise covariance
    double q[] = {.05, .05, .0, 
                  .05, .05, .0, 
                   .0,  .0, .0};

    // Measurement noise covariance
    double r[] = {5};

    // Matrix definitions
    TMatrixD A(n, n, a); 
    TMatrixD B(n, n); 
    TMatrixD H(m, n, h); 

    TMatrixD P(n, n, p); 
    TMatrixD Q(n, n, q);
    TMatrixD R(m, m, r);     
    
    std::cout << "-- Matrix A";
    A.Print();
    std::cout << "-- Matrix B";
    B.Print();
    std::cout << "-- Matrix H";
    H.Print();

    std::cout << "-- Covariance matrix P";
    P.Print();
    std::cout << "-- Covariance matrix Q";
    Q.Print();
    std::cout << "-- Covariance matrix R";
    R.Print();

    // List of noisy position measurements (y)
    std::vector<double> measurements = {
      1.04202710058, 1.10726790452, 1.2913511148, 1.48485250951, 1.72825901034,
      1.74216489744, 2.11672039768, 2.14529225112, 2.16029641405, 2.21269371128,
      2.57709350237, 2.6682215744, 2.51641839428, 2.76034056782, 2.88131780617,
      2.88373786518, 2.9448468727, 2.82866600131, 3.0006601946, 3.12920591669,
      2.858361783, 2.83808170354, 2.68975330958, 2.66533185589, 2.81613499531,
      2.81003612051, 2.88321849354, 2.69789264832, 2.4342229249, 2.23464791825,
      2.30278776224, 2.02069770395, 1.94393985809, 1.82498398739, 1.52526230354,
      1.86967808173, 1.18073207847, 1.10729605087, 0.916168349913, 0.678547664519,
      0.562381751596, 0.355468474885, -0.155607486619, -0.287198661013, -0.602973173813
  };

    // Best guess of initial states
    TKalmanFilter::State xk0(std::vector<double>({measurements[0], 0, -9.81}), 0); // position, speed, acceleration
                         xk0._cov = P;

    // Construct the filter
    TKalmanFilter kf(A, H);
                  kf.First(xk0, Q, R, dt);
   
    std::cout << "=====================" << std::endl;
    std::cout << "=== KALMAN FILTER ===" << std::endl;
    std::cout << "=====================" << std::endl << std::endl;

    // Feed measurements into filter, output estimated states
    std::cout << "t = 0; ";
    std::cout << "x[0] = " << xk0()(0) << ", "<< xk0()(1) << ", "<< xk0()(2) << std::endl;
    std::cout << kf.Gain() << std::endl;

    std::vector<double> tReference    = {0.0333333, 0.0666667, 0.1, 0.133333, 0.166667, 0.2, 0.233333, 0.266667, 0.3, 0.333333, 0.366667, 0.4, 0.433333, 0.466667, 0.5, 0.533333, 0.566667, 0.6, 0.633333, 0.666667, 0.7, 0.733333, 0.766667, 0.8, 0.833333, 0.866667, 0.9, 0.933333, 0.966667, 1, 1.03333, 1.06667, 1.1, 1.13333, 1.16667, 1.2, 1.23333, 1.26667, 1.3, 1.33333, 1.36667, 1.4, 1.43333, 1.46667, 1.5};
    std::vector<double> xPosition     = {1.04203, 1.08708, 1.21726, 1.37662, 1.56866, 1.6825, 1.8836, 2.0217, 2.11578, 2.19069, 2.3334, 2.45705, 2.50951, 2.59713, 2.68559, 2.74987, 2.80713, 2.82256, 2.86204, 2.914, 2.89637, 2.86967, 2.8097, 2.74668, 2.71797, 2.688, 2.67404, 2.62113, 2.51844, 2.3864, 2.28523, 2.13925, 1.99756, 1.85172, 1.66611, 1.57765, 1.36503, 1.16943, 0.965763, 0.746617, 0.53849, 0.322791, 0.0427047, -0.219931, -0.501726};
    std::vector<double> xSpeed        = {-0.327, 0.185739, 1.1065, 1.70821, 2.20488, 2.08472, 2.33911, 2.2488, 1.99214, 1.69607, 1.63238, 1.50983, 1.1982, 1.00888, 0.839945, 0.625611, 0.415231, 0.11541, -0.0909276, -0.237813, -0.558878, -0.876857, -1.26203, -1.62325, -1.85087, -2.05625, -2.18991, -2.41872, -2.76889, -3.17278, -3.45258, -3.83319, -4.16841, -4.48514, -4.88236, -4.98385, -5.40203, -5.74174, -6.07385, -6.41768, -6.70629, -6.98964, -7.40495, -7.7479, -8.10938};
    std::vector<double> xAcceleration = {-9.81, -9.80886, -9.80545, -9.79885, -9.78506, -9.77798, -9.74143, -9.71669, -9.70565, -9.69904, -9.61208, -9.52416, -9.52086, -9.43237, -9.31366, -9.22392, -9.12306, -9.11823, -9.00104, -8.80868, -8.84423, -8.87484, -8.99406, -9.07626, -8.97629, -8.85203, -8.64065, -8.56416, -8.6464, -8.79086, -8.77464, -8.88051, -8.92665, -8.94876, -9.06039, -8.83891, -8.97247, -9.01545, -9.04817, -9.09098, -9.07666, -9.058, -9.16592, -9.20081, -9.25087};

    for(int i = 0, N = measurements.size(); i < N; i++) {

        TKalmanFilter::Measurement zk(m, &measurements[i]);
        const TKalmanFilter::State &xk = kf.Update(zk);

        std::cout << "t = " << xk._time << "; ";
        std::cout << "z[" << i << "] = " << zk()(0) << "; "; // Measurement 
        std::cout << "x[" << i << "] = " << xk()(0) << ", "<< xk()(1) << ", "<< xk()(2) << std::endl; // Outgoing state
        std::cout << kf.Gain() << std::endl;

        double epsilon = 1e-5;
        EXPECT_TRUE(ROOT::IOPlus::Helpers::EpsilonEqualTo(tReference[i], xk._time, epsilon));
        EXPECT_TRUE(ROOT::IOPlus::Helpers::EpsilonEqualTo(xk()(0), xPosition[i], epsilon));
        EXPECT_TRUE(ROOT::IOPlus::Helpers::EpsilonEqualTo(xk()(1), xSpeed[i], epsilon));
        EXPECT_TRUE(ROOT::IOPlus::Helpers::EpsilonEqualTo(xk()(2), xAcceleration[i], epsilon));
    }
}

int main(int argc,char **argv) {

    // Initialize Google Test framework
    ::testing::InitGoogleTest(&argc,argv);

    // Run all tests
    return RUN_ALL_TESTS();
}