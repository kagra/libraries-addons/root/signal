#include "ROOT/Signal/EnumTypes.h"
#include "ROOT/IOPlus/Helpers.h"

namespace ROOT { 
    
    namespace Signal {

        TString enum2str(Domain domain) {

                switch(domain) {

                        case Domain::Time:             return "time";
                        case Domain::Frequency:        return "freq";
                        case Domain::ComplexFrequency: return "freqc";

                        default: enum2str();
                }
        }

        TString enum2str(ComplexPart part) {

                switch(part) {

                        case ComplexPart::Real:      return "re";
                        case ComplexPart::Imag:      return "im";
                        case ComplexPart::Magnitude: return "mag";
                        case ComplexPart::Phase:     return "ph";
                        case ComplexPart::Decibel:   return "db";
                        case ComplexPart::Argument:  return "arg";
                        default: enum2str();
                }
        }

        TString enum2str(Filter type) {

                switch(type) {

                        case Filter::Analog:  return "s";
                        case Filter::Digital: return "z";
                        default: enum2str();
                }
        }

        TString enum2str(FilterType filter) {

                switch(filter) {

                        case FilterType::None:      return "";
                        case FilterType::LowShelf:  return "lshelf";
                        case FilterType::HighShelf: return "hshelf";
                        case FilterType::Notch:     return "notch";
                        case FilterType::Peak:      return "peak";
                        case FilterType::AllPass:   return "allpass";
                        case FilterType::LowPass:   return "lpass";
                        case FilterType::HighPass:  return "hpass";
                        case FilterType::BandPass:  return "bpass";
                        case FilterType::BandStop:  return "bstop";
                        default: enum2str();
                }
        }

        TString enum2str(FilterDesign design) {

                switch(design) {

                        case FilterDesign::None:        return "";
                        case FilterDesign::Butterworth: return "butter";
                        case FilterDesign::Elliptic:    return "ellip";
                        case FilterDesign::ChebyshevI:  return "cheby1";
                        case FilterDesign::ChebyshevII: return "cheby2";
                        case FilterDesign::Bessel:      return "bessel";
                        default: enum2str();
                }
        }

        FilterDesign str2filter(TString str) {

                str = ROOT::IOPlus::Helpers::SnakeToCamel(str);
                if(str == "") return FilterDesign::None;

                if(str == "butter") return FilterDesign::Butterworth;
                if(str == "ellip" ) return FilterDesign::Elliptic;
                if(str == "cheby1") return FilterDesign::ChebyshevI;
                if(str == "cheby2") return FilterDesign::ChebyshevII;
                if(str == "bessel") return FilterDesign::Bessel;

                return FilterDesign::None;
        }

        TString enum2str(Method m) {

                switch(m) {

                        case Method::Direct:     return "direct";
                        case Method::DFT:        return "fft";
                        case Method::Polyphase:  return "polyphase";
                        default: enum2str();
                }
        }

        Method str2method(TString str) {

                str = ROOT::IOPlus::Helpers::SnakeToCamel(str);
                if(str == "fft") return Method::DFT;
                if(str == "polyphase")  return Method::Polyphase;

                str2enum();
                
                return Method::DFT; // default, cannot be reached (on purpose) due to exception in str2enum
        }

        TString enum2str(Trend detrend) {

                switch(detrend) {

                        case Trend::None:             return "";
                        case Trend::Constant:         return "cst";
                        case Trend::Linear:           return "lin";
                        case Trend::Quadratic:        return "quad";
                        case Trend::QuadraticConcave: return "quadv";
                        case Trend::QuadraticConvex:  return "quadx";
                        case Trend::Logarithmic:      return "log";
                        case Trend::Hyperbolic:       return "hyperb";

                        default: enum2str();
                }
        }

        TString enum2str(Average average) {

                switch(average) {

                        case Average::Mean:   return "mean";
                        case Average::Median: return "median";

                        default: enum2str();
                }
        }
        
        TString enum2str(Normalizer op) {

                switch(op) {

                        case Normalizer::None:        return "";
                        case Normalizer::Unitary:     return "unitary";
                        case Normalizer::Scalar:      return "scalar";

                        case Normalizer::Backward:    return "backward";
                        case Normalizer::Orthonormal: return "orthonormal";
                        case Normalizer::Forward:     return "forward";
                        default: enum2str();
                }
        }

        TString enum2str(Direction direction) {

                switch(direction) {

                        case Direction::Forward:  return "fwd";
                        case Direction::Backward: return "bwd";

                        case Direction::Both: [[fallthrough]];
                        case Direction::Orthonormal: [[fallthrough]];
                        case Direction::BackwardForward: [[fallthrough]];
                        case Direction::ForwardBackward: return "";

                        default: enum2str();
                }
        }

        TString enum2str(Estimator estimator) {

                switch(estimator) {

                        case Estimator::Correlation:   return "correlation";
                        case Estimator::BlackmanTukey: return "blackmanTukey";

                        case Estimator::Periodogram        : [[fallthrough]];
                        case Estimator::ModifiedPeriodogram: return "periodogram";

                        case Estimator::Bartlett   : return "bartlett";
                        case Estimator::LombScargle: return "lombScargle";
                        case Estimator::Welch      : return "welch";
                        
                        default: enum2str();
                }
        }

        TString enum2str(SpectrumProperty spectrum) {

                switch(spectrum) {

                        case SpectrumProperty::OneSided : return "oneSided";
                        case SpectrumProperty::TwoSided : return "twoSided";

                        default: enum2str();
                }
        }

        TString enum2str(Transform transform) {

                switch(transform) {

                        case Transform::Standard: return "std";
                        case Transform::Fourier:  return "fourier";
                        case Transform::Laplace:  return "laplace";
                        case Transform::Z:        return "z";
                        case Transform::Hilbert:  return "hilbert";

                        default: enum2str();
                }

                return "";
        }

        WindowType str2window(TString str) {

                str = ROOT::IOPlus::Helpers::SnakeToCamel(str);
                if(str == "") return WindowType::Unit;

                if(str == "zero") return WindowType::ZeroPad;
                if(str == "square") return WindowType::Square;
                if(str == "rectangle") return WindowType::Rectangle;

                if(str == "sawtooth") return WindowType::Sawtooth;
                if(str == "triangle") return WindowType::Triangle;

                if(str == "bartlett") return WindowType::Bartlett;
                if(str == "bartlettHann") return WindowType::BartlettHann;
                if(str == "blackman") return WindowType::Blackman;
                if(str == "blackmanHarris") return WindowType::BlackmanHarris;

                if(str == "planckTaper") return WindowType::PlanckTaper;
                if(str == "planckTaperLeft") return WindowType::PlanckTaperLeft;
                if(str == "planckTaperRight") return WindowType::PlanckTaperRight;

                if(str == "bohman") return WindowType::Bohman;
                if(str == "tukey") return WindowType::Tukey;
                if(str == "cosine") return WindowType::Cosine;
                if(str == "sine") return WindowType::Sine;
                if(str == "flat-top") return WindowType::FlatTop; 
                if(str == "gaus") return WindowType::Gaussian;
                if(str == "hamming") return WindowType::Hamming;
                if(str == "hann") return WindowType::Hann;
                if(str == "kaiser") return WindowType::Kaiser;
                if(str == "lanczos") return WindowType::Lanczos;

                return WindowType::None;
        }

        TString enum2str(WindowType window) {

                switch(window) {

                        case WindowType::None: [[fallthrough]];
                        case WindowType::Unit: return "";

                        case WindowType::Zeros  : [[fallthrough]];
                        case WindowType::ZeroPad: return "zero";

                        case WindowType::Rectangular: [[fallthrough]];
                        case WindowType::Rectangle  : return "rectangle";
                        case WindowType::Square     : return "square";
                        
                        case WindowType::Sawtooth  : return "sawtooth";

                        case WindowType::Triangular: [[fallthrough]];
                        case WindowType::Triangle  : return "triangle";

                        case WindowType::Bartlett      : return "bartlett";
                        case WindowType::BartlettHann  : return "bartlettHann";

                        case WindowType::Blackman        : return "blackman";
                        case WindowType::BlackmanHarris  : return "blackmanHarris";
                        case WindowType::PlanckTaper     : return "planck";
                        case WindowType::PlanckTaperLeft : return "planck-left";
                        case WindowType::PlanckTaperRight: return "planck-right";

                        case WindowType::Bohman        : return "bohman";
                        case WindowType::Tukey         : return "tukey";
                        case WindowType::Cosine        : return "cosine";
                        case WindowType::Sine          : return "sine";
                        case WindowType::FlatTop       : return "flat-top";
                        case WindowType::Gaussian      : return "gaus";
                        case WindowType::Hamming       : return "hamming";
                        case WindowType::Hann          : return "hann";
                        case WindowType::Kaiser        : return "kaiser";
                        case WindowType::Lanczos       : return "lanczos";
                        default: enum2str();
                }
        }

        WindowSymmetry str2window_symmetry(TString str) {

                str = ROOT::IOPlus::Helpers::SnakeToCamel(str);
                if(str == "symmetric") return WindowSymmetry::symmetric;
                if(str == "periodic")  return WindowSymmetry::periodic;

                str2enum();
                
                return WindowSymmetry::symmetric; // default, cannot be reached (on purpose) due to exception in str2enum
        }

        TString enum2str(WindowSymmetry symmetry) {

                switch(symmetry) {

                        case WindowSymmetry::symmetric  : return "symmetric";
                        case WindowSymmetry::periodic     : return "periodic";

                        default: enum2str();
                }
        }

        std::ostream& operator<<(std::ostream& os, const Domain& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const ComplexPart& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const Filter& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const FilterType& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const FilterDesign& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const Method& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const Trend& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const Average& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const Normalizer& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const Direction& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const Estimator& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const SpectrumProperty& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const Transform& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const WindowType& x) {
                os << enum2str(x);
                return os;
        }
        std::ostream& operator<<(std::ostream& os, const WindowSymmetry& x) {
                os << enum2str(x);
                return os;
        }
    }
}
