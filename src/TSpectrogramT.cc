#include "ROOT/Signal/TSpectrogramT.h"

ClassImp(ROOT::Signal::TSpectrogramT<double>)
ClassImp(ROOT::Signal::TSpectrogramT<float>)

namespace ROOT {
    namespace Signal {
                
        template <typename T>
        TSpectrogramT<T>::TSpectrogramT()
        {
            // Default constructor initializes delta time to 1 second, epoch to 0, f0 to 0, and unitY and labelY to an empty string
        }

        template <typename T>
        TSpectrogramT<T>::~TSpectrogramT() {
            // Destructor
        }
    }
}

template class ROOT::Signal::TSpectrogramT<float>;
template class ROOT::Signal::TSpectrogramT<double>;