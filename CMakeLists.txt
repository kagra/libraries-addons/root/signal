
# Name of the library.. used in subdirectories
set(LIBRARY "RSignal")
set(LIBRARY_NAME "ROOT Advanced Signal Processing Library")
set(LIBRARY_BRIEF "Implement DSP and DFT features using KFR library")

cmake_minimum_required(VERSION 3.20)

# Project information
set(PROJECT_TITLE "ROOT Advanced Signal Processing Library")
project(RSignal 
    VERSION 0.1
    DESCRIPTION "Implement DSP and DFT features using KFR library."
    HOMEPAGE_URL "https://git.ligo.org/kagra/libraries-addons/root/signal"
)

set(LIBRARY ${PROJECT_NAME})
set(LIBRARY_TITLE ${PROJECT_TITLE})

# Additional cmake features
add_compile_options(-Wextra -Wno-sign-compare -Wimplicit-fallthrough)

# Optional MacOS features
if(APPLE) 
    set(CMAKE_BUILD_WITH_INSTALL_NAME_DIR ON)
    set(CMAKE_INSTALL_NAME_DIR "@rpath")
    set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
    set(CMAKE_INSTALL_RPATH "@loader_path")
endif()

set(CMAKE_INSTALL_MESSAGE "LAZY")

# Check if git submodule are initialized in case you use custom cmake modules.
file(GLOB CMAKE_MODULE_PATHS "${CMAKE_CURRENT_SOURCE_DIR}/cmake/*")
foreach(MODULE_PATH ${CMAKE_MODULE_PATHS})
    if(IS_DIRECTORY ${MODULE_PATH})
        list(PREPEND CMAKE_MODULE_PATH ${MODULE_PATH})
    endif()
endforeach()

#
# Prepare project architecture
include(ProjectArchitecture)
include(FindPackageStandard)
include(BuildOptions)
include(DisplayMotd)

DISPLAY_MOTD()
MESSAGE_TITLE("${PROJECT_TITLE}")

update_build_options()
check_build_option(BUILD_IMAGE "Docker container won't be built.")
check_build_option(BUILD_DOCUMENTATION "Library documentation won't be built.")
check_build_option(BUILD_TESTS "Tests for this library won't be running.")
check_build_option(BUILD_LIBRARY "Library `${PROJECT_NAME}` will not be built.")
check_build_option(BUILD_EXAMPLES "Examples how-to-use `${PROJECT_NAME}` won't be computed.")
check_build_option(BUILD_TOOLS "Additional binary tools won't be computed.")

#
# Build project
CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Makefile.in ${CMAKE_CURRENT_BINARY_DIR}/cmake/Makefile @ONLY)
CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/docker-compose.yml.in ${CMAKE_CURRENT_SOURCE_DIR}/docker-compose.yml @ONLY)
CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/environment.yml.in ${CMAKE_CURRENT_SOURCE_DIR}/environment.yml @ONLY)
file(CREATE_LINK ${CMAKE_BINARY_DIR_RELATIVE}/cmake/Makefile ${CMAKE_CURRENT_SOURCE_DIR}/Makefile SYMBOLIC)
file(CREATE_LINK ${CMAKE_BINARY_DIR_RELATIVE} ${CMAKE_CURRENT_SOURCE_DIR}/build SYMBOLIC)

#
# Create docker image (used in GitLab CI/CD for running tests)
if(BUILD_IMAGE)
    include(DockerContainer)
endif()

#
# Additional restriction/warning feature
include(DisableInSourceBuild)
include(GitBranchWarning)

#
# Create a library
if(BUILD_LIBRARY)

    DUMP_PROJECT()

    #
    # Load compiler
    include(LoadStandardCompilerC)
    include(LoadStandardCompilerCXX)

    #
    # Library dependencies
    include(LoadROOT)
    find_package(ROOT+ REQUIRED)
    find_package(GSL REQUIRED)
    find_package(KFR REQUIRED)

    #
    # Prepare library Config file for pre-processing
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/include/lib${LIBRARY}.Config.h.in ${CMAKE_CURRENT_SOURCE_DIR}/include/lib${LIBRARY}.Config.h )
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.sh.in   thislib.${LIBRARY}.sh   @ONLY )
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/thislib.csh.in  thislib.${LIBRARY}.csh  @ONLY )
    CONFIGURE_FILE( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/rootlogon.in    .rootlogon   @ONLY )
    
    #
    # Prepare library including ROOT    
    FILE_SOURCES(SOURCES "src")
    add_library(${LIBRARY} SHARED ${SOURCES})
    target_include_directories(${LIBRARY} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")

    if(${CMAKE_MAINPROJECT})
        add_custom_target(lib DEPENDS ${LIBRARY} COMMENT "Compiling library")
    endif()

    target_link_libraries(${LIBRARY} ${ROOT_LIBRARIES})
    target_link_package(${LIBRARY} ROOT+ REQUIRED)
    target_link_package(${LIBRARY} GSL REQUIRED)
    target_link_package(${LIBRARY} KFR REQUIRED)

    #
    # Prepare ROOT Dictionary
    set(DICTIONARY ${LIBRARY}.Dict)
    set(LINKDEF lib${LIBRARY}.LinkDef.h)
    if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/include/${LINKDEF})
        message(FATAL_ERROR "ROOT LinkDef is missing `${LINKDEF}`.")
    endif()

    FILE_HEADERS_RECURSE(HEADERS "include")
    list(REMOVE_ITEM HEADERS ${LINKDEF})
    ROOT_GENERATE_DICTIONARY(${DICTIONARY} ${HEADERS} LINKDEF ${CMAKE_CURRENT_SOURCE_DIR}/include/${LINKDEF} MODULE ${LIBRARY})

    # Installation of the library
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include DESTINATION . PATTERN "*.in" EXCLUDE)
    install(TARGETS ${LIBRARY} LIBRARY DESTINATION lib ARCHIVE DESTINATION lib RUNTIME DESTINATION bin)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARY}.rootmap DESTINATION lib)   # Install the rootmap file
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${LIBRARY}_rdict.pcm DESTINATION lib) # Install the rdict.pcm file

    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/thislib.${LIBRARY}.sh  DESTINATION ${CMAKE_INSTALL_PREFIX} PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/thislib.${LIBRARY}.csh DESTINATION ${CMAKE_INSTALL_PREFIX} PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/.rootlogon  DESTINATION ${CMAKE_INSTALL_PREFIX} PERMISSIONS OWNER_READ OWNER_WRITE GROUP_READ WORLD_READ)

    #
    # Add tests
    if(BUILD_TESTS)
        add_subdirectory("tests")
    endif()

    #
    # Compilation of examples
    if(BUILD_EXAMPLES)
        add_subdirectory("examples")
    endif()

    #
    # Compilation of additonal tools
    if(BUILD_TOOLS)
        add_subdirectory("tools")
    endif()

    #
    # Add benchmark test support
    option(BUILD_BENCHMARK "Build Benchmark" ${BUILD})

    if(NOT DEFINED BUILD_BENCHMARK)
        if(CMAKE_BUILD_TYPE STREQUAL "Release")
            option(BUILD_BENCHMARK "Build Benchmark" OFF)
        else()
            option(BUILD_BENCHMARK "Build Benchmark" ${CMAKE_MAINPROJECT})
        endif()
    endif()

    if(DEFINED BUILD_BENCHMARK AND NOT BUILD_BENCHMARK)
        message(WARNING "Benchmark tests will not be running. (use -DBUILD_BENCHMARK=ON if you want to turn ON)")
    else()
        add_subdirectory("./benchmark")
    endif()

endif()

#
# Prepare assets for documentation
if(BUILD_DOCUMENTATION)
    add_subdirectory("share/doxygen")
endif()

#
# Hook Scripts
if(CMAKE_MAINPROJECT)

    # Make sure Inc are available
    install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/cmake DESTINATION share)

    # Post install message
    find_file(CMAKE_POST_CONFIG PostInstallConfig.cmake PATHS ${CMAKE_MODULE_PATHS})
    if(CMAKE_POST_CONFIG)
        install(SCRIPT "${CMAKE_POST_CONFIG}")
    endif()

    # Post cmake message
    include(PostCMakeConfig)

endif()